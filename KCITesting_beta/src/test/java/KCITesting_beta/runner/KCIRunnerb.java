package KCITesting_beta.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions (
		features = { "src/test/java/KCITesting_beta/activeFeatureFiles/" }, 
		glue = {"stepDefinitions" }, 
		monochrome = true, 
		tags = {}, 
		plugin = { "pretty", "html:target/report","json:target/cucumber.json", "com.cucumber.listener.ExtentCucumberFormatter:target/report/report.html" }
		)

public class KCIRunnerb {

}
