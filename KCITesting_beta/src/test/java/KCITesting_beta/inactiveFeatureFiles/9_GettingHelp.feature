Feature: Getting Help

Scenario: 
	- View Help Page
	- Visible Help Page Elements
	- FAQ Answers
	- Download User Documentation
	- Access the Contact Support Team
	- Close the Help Page
	
	Given User is logged in to Knowable
		
	# View Help Page
	When User clicks the Help Icon in the upper right hand corner
	Then User is taken to the Help page
	
	# Visible Help Page Elements
	
	# FAQ Answers
	When user clicks on each of the topics under the Frequently Asked Questions section
	Then Question under those topics display
	
	When User clicks each of the questions under each topic
	Then The answer to the questions appear
	
	When User clicks on the questions again
	Then The answers are hidden from the user
	
	When User clicks on the topics again
	Then The questions are hidden from the user
	
	# Download User Documentation
	When User clicks the Download button under the Download User Documentation section
	Then User is able to download and view the user guide
	
	When User clicks on the Download User Documentation button in the top right of the page
	Then User is able to download and view the user guide
	
	# Access the Contact Support Form
	When User clicks on the Contact Production Support icon
	Then The Contact Product Support form appears
	
	When User clicks Cancel Request
	Then The Contact Product Support form disappears
	
	# Close the Help Page
	When User clicks on the Back button at the top of the Help page
	Then The page the user previously viewed displays