Feature: Non-SSO Login 

Scenario: 
	- Non-SSO Normal Login
	- Logout 
	- Reset Password Length Check
	- Reset Password Character Check
	- Login with New Credentials 
	- Logout 
	- Login with Incorrect Credentials
	- Reset Password (Sad Path)
	- Login with Old Password 
		
	#	Non-SSO Normal Login 
	Given User opens their email inbox
	When User opens the Knowable website
	When User enters a valid email 
	And User clicks Continue button
	Then User should be taken to password page 
	
	When User enters their default password
	And User clicks Sign In button 
	Then User should be taken to the Dashboard page
	
	#	 Logout	
	When User clicks the top right profile button 
	And User clicks the log out button 
	Then User should be taken to the log in page 
	
	#	Reset Password Length Check
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User clicks Reset Password button 
	Then User should be told to check their email "pleasecheckyouremailtoresetyourpassword"
	
	When User navigates to their email inbox 
	And User clicks password reset link 
	Then User is taken to the password reset page 
	
	When User creates a new password that is less than ten characters "PA$$word2"
	Then A new password length error message should display "requiresatleastcharacters"
	
	When User creates a new password that is at least ten characters "PA$$word21"
	And User correctly confirms the new password "PA$$word21"
	And User clicks Save button 
	Then User should be taken to the log in page 
	
	#	Reset Password Character Check
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User clicks Reset Password button 
	Then User should be told to check their email "pleasecheckyouremailtoresetyourpassword"
	
	When User navigates to their email inbox 
	And User clicks password reset link 
	Then User is taken to the password reset page 
	
	When User creates passwords that do not meet criteria 
	| PA$$WORD21 | pa$$word21 | PA$$wordtwoone | PASSword21 |
	Then A new password character error message should display "requiresatleastspecialcharacteruppercaseletterlowercaseletterandnumber"
	
	When User creates a new password that meets all criteria "PA$$word21"
	And User correctly confirms the new password "PA$$word21"
	And User clicks Save button 
	Then User should be taken to the log in page 
	
	# 	Login with New Credentials
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User enters the new password "PA$$word21"
	And User clicks Sign In button 
	Then User should be taken to the Dashboard page
	
	#	 Logout
	When User clicks the top right profile button 
	And User clicks the log out button 
	Then User should be taken to the log in page 
	
	#	Login with Incorrect Credentials
	When User enters an incorrect email "wrong@email.com"
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User enters the new password "PA$$word21"
	And User clicks Sign In button 
	Then A login error message should display "theusernameorpasswordisincorrect"
	
	When User clicks the Use Another Account link 
	Then User should be taken to the log in page 
	
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User enters an incorrect password "WR0NGpa$$word"
	And User clicks Sign In button 
	Then A login error message should display "theusernameorpasswordisincorrect"
	
	#	9. Reset Password (Sad Path)	
	When User clicks Reset Password button 
	Then User should be told to check their email "pleasecheckyouremailtoresetyourpassword"
	
	When User navigates to their email inbox 
	And User clicks password reset link 
	Then User is taken to the password reset page 
	
	When User creates a new password that meets all criteria "P4$$word123"
	And User incorrectly confirms the new password "P4$$word1234"
	Then A confirm password error message should display "thepasswordsdonotmatch"
	
	When User correctly confirms the new password "P4$$word123"
	And User clicks Save button 
	Then User should be taken to the log in page 
	
	#	10. Login with Old Password
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User enters old password "PA$$word21"
	And User clicks Sign In button 
	Then A login error message should display "theusernameorpasswordisincorrect"