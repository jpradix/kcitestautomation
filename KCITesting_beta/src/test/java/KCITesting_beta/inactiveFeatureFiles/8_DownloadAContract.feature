Feature: Download a Contract

Scenario: 
	- Download via Recently Viewed Contracts
	- Download via Contract List View
	- Download via Contract Detail View
	
	Given User is logged in to Knowable
		
	# Download via Recently Viewed Contracts
	
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page

	When User searches for a specific contract in the list and clicks it
	Then The Contract Preview, Data and Search tabs, and the Related Contracts section load for the contract
	
	When User clicks the Dashboard tab
	Then User should be taken to the Dashboard page
	
	When User clicks the More option to the right of a contract under Recently Viewed Contracts
	Then The download button appears next to the recently viewed contract
	
	When User clicks the Download button under Recently Viewed Contracts
	Then The contract downloads from the Recently Viewed Contracts
	
	# Download via Contract List View
	
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page

	When User clicks the More option to the right of a contract in the list
	Then The download button appears next to the top contract
	
	When User clicks the Download button on the Contract List View
	Then The contract downloads from the Contract List View
	
	# Download via Contract Detail View
	
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page

	When User searches for a specific contract in the list and clicks it
	Then The Contract Preview, Data and Search tabs, and the Related Contracts section load for the contract
	
	When User clicks the Download button on the Contract Viewer
	Then The contract downloads from the Contract Viewer
	