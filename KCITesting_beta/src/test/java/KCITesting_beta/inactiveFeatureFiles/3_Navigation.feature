Feature: Navigation

Scenario: 
	All Users:
			- Navigation Tabs
			- Dashboard Tab < > Contracts Tab
			- Contracts Tab > Logo
			
	User and Administrator Roles ONLY: 
			-Dashboard Tab < > Upload Tab
			- Contracts Tab < > Upload Tab
			- Upload Tab > Logo
	
	Given User is logged in to Knowable
		
		# --------- All Users --------- #
		
	#	Navigation Tabs
	Then User should be taken to the Dashboard page
	
	#	Dashboard Tab < > Contracts Tab
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page
	
	When User clicks the Dashboard tab
	Then User should be taken to the Dashboard page
	
	#	Contracts Tab > Logo
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page
	
	When User clicks the logo in the top left hand corner
	Then User should be taken to the Dashboard page
	
		# --------- User and Administrator Roles ONLY --------- #
	
	#	Dashboard Tab < > Upload Tab
	When User clicks the Upload tab
	Then User should be taken to the Upload page
	
	When User clicks the Dashboard tab 
	Then User should be taken to the Dashboard page

	
	#	Dashboard Tab < > Contracts Tab
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page
	
	When User clicks the Upload tab
	Then User should be taken to the Upload page
	
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page
	
	#	Upload Tab > Logo
	When User clicks the Upload tab
	Then User should be taken to the Upload page
	
	When User clicks the logo in the top left hand corner
	Then User should be taken to the Dashboard page
