Feature: Dashboard 

Scenario: 
	- Visible Dashboard Elements
	- Access Welcome Menu
	- Dashboard Search
	- Recently Viewed Contracts
	- Top Counterparties Chart
	- Document Type Chart 
	
	Given User is logged in to Knowable
		
	#	Visible Dashboard Elements
	When User locates the Total Number of Contracts in the Database 
	And User locates the Total Number of Out of Scope Contracts 
	And User locates the Top Counterparties chart 
	And User locates the Document Type chart 
	And User locates the Document Status chart 
	Then User should have located all page elements 
	
	#	Access Welcome Menu
	When User clicks the top right profile button 
	Then User should see all Welcome menu elements 
	
	#	Dashboard Search
	When User clicks on the Search Panel Search field 
	And User enters "Master" in the Search Panel Search field
	And User locates the Counterparty Name field 
	And User locates the Client Entity field 
	And User clicks the Document Type dropdown 
	And User observes the dropdown options 
	And User locates the Add Filter option 
	And User clicks the Search button  
	Then The Contract page should display with appropriate results 
	
	When User clicks the Reset button
	Then The search criteria should be removed
	
	#	Recently Viewed Contracts
	When User clicks one of the contracts listed on the Contracts page 
	Then User takes note of the contract name 
	
	When User clicks the logo in the top left hand corner 
	Then User should be taken to the Dashboard page 
	
	When User Locates the Recently Viewed contracts in the Search panel 
	Then User should be able to verify that the contract that was just opened is listed at the top of the Recently viewed contracts list 
	
	When User clicks on a recently viewed contract 
	Then User should see the Contract Preview page 
	
	When User clicks the Back button 
	Then User should be taken to the Dashboard page
	
	#	Top Counterparties Chart
	When User clicks on one of the counterparties listed under Top Counterparties and takes note of how many contracts are listed for that selection 
	Then The Contracts tab displays with appropriate results and the Search panel contains the selected Counterparty Name 
	
	When User clicks the Dashboard tab 
	Then User should be taken to the Dashboard page
	
	#	Document Type Chart
	When User clicks on one of the document types listed under Document Type and takes note of how many contracts are listed for that selection 
	Then The Contracts tab displays with appropriate results and the Search panel contains the selected Document Type 
	
	When User clicks the Dashboard tab 
	Then User should be taken to the Dashboard page
	