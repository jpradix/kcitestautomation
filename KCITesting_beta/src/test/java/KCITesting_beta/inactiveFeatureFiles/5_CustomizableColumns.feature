Feature: Customizable Columns

	Scenario: 
	- Access Configure Table Options
	- Available Fields
	- Add a Single Field to Customizable Fields
	- Remove a Single Field from Customizable Fields
	- Add Multiple Fields to Customizable Fields
	- Remove Multiple Fields from Customizable Fields
	- Reorder Customizable Fields
	- Manual Column Customization
	- Default Columns
	
	Given User is logged in to Knowable
	And User clicks the Contracts tab
	#	Access Configure Table Options
	
	When User clicks the More (...) column header
	Then The list of options appears
	
	When User clicks Customize Table
	Then The Customize Table pop-up appears
	
	#	Available Fields
	
	When User searches for a field using search bar above Available Fields list with "Document Type"
	Then Fields relating to search criteria are displayed in the Available Fields list
	
	When User removes the search criteria from the search field
	Then The Available Fields list resets
	
	When User selects a category from the Category dropdown
	Then Fields relating to the selected category are displayed in the Available Fields list
	
	When User chooses Select a Category from the Category dropdown
	Then The category is removed and the list of all fields display in the Available Fields list
	
	#	Add a Single Field to Customizable Fields
	
	When User selects the desired field from the Available Fields list
	Then The desired field is highlighted in the Available Fields list
	
	When User clicks the right arrow to move the field to the Customizable Fields list
	Then The desired field is displayed at the top of the Customizable Fields list
	
	#	Remove a Single Field from Customizable Fields
	
	When User selects another field from the Customizeable Fields list
	Then The desired field is highlighted in the Customizable Fields list
	
	When User clicks the left arrow to move the field to the Available Fields list
	Then The desired field is displayed at the top of the Available Fields list
	
	#	Add Multiple Fields to Customizable Fields
	
	### User must manually test on Mac as the Selenium function to COMMAND + click WebElements does not function on Mac.
	
	#	Remove Multiple Fields from Customizable Fields
	
	### User must manually test on Mac as the Selenium function to COMMAND + click WebElements does not function on Mac.
	
	#	Reorder Customizable Fields
	
	When User selects the first field in the Customizable fields list
	And User clicks the down arrow
	Then The desired field moves down in the list
	
	When User clicks the up arrow
	Then The desired field moves up in the list
	
	When User clicks the bottom arrow
	Then The desired field moves to the bottom of the list
	
	When User clicks the top arrow
	Then The desired field moves to the top of the list
	
	When User rearranges the order of fields in the Customizable Fields list
	And User clicks the Save Preferences button
	Then The pop-up closes and the Contracts tab is displayed with the selected columns in the desired order
	
	#	Manual Column Customization
	
	When User clicks and drags a column header to the right of another column header
	Then The column moves to the desired location
	
	#	Default Columns

	When User clicks the More (...) column header
	Then The list of options appears
	
	When User clicks Customize Table
	Then The Customize Table pop-up appears
	
	When User clicks the Set to Default button below the Customizable Columns list
	Then The default fields display in the Customizable columns list in the default order
	
	When User clicks the Save Preferences button
	Then The default columns display in the default order on the Contracts table