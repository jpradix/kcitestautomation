Feature: Contracts Tab - List View 

Scenario: 
	- Hover Features
	- Page Controls
	- Search Panel - No Results
	- Search Panel - Counterparty Name
	- Search Panel - Client Entity
	- Search Panel - Document Type
	- Search Panel - Add Filter Contract ID
	- Search Panel - Add Filter Document Title
	- Search Panel - Add Filter Document Type (Define Other)
	- Search Panel - Effective Date
	- Search Panel - Add Filter File Name
	- Search Panel - Contract Execution Status
	- Search Panel - Contract Status
	- Search Panel - Parent Control Number
	- Search Panel - Document Issues
	- Search Panel - Out of Scope
	- Search Panel - Upload By
	- Search Panel - Upload Date
	- Sort - One Column Sort Contracts 
	- Sort - Two Column Sort Contracts 

	Given User is logged in to Knowable
		
	# 1. Hover Features
	When User clicks the Contracts tab 
	And User hovers over circle icon 
	Then User sees the Contract Color Key 
	
	When User hovers over flag icon 
	Then User sees the Flag Color Key 
	
	# Cannot automate hovering over contract name to see tooltip as tooltip element cannot be found on the DOM, so no verification can be made. 
	# Must be manually tested. 
	
	# 2. Page Controls
	When User clicks the second page number button 
	Then User is taken to the second page 
	
	When User clicks the next page arrow button 
	Then User is taken to the third page 
	
	When User clicks the last page arrow button 
	Then User is taken to the last page 
	
	When User clicks the previous page arrow button 
	Then User is taken to the previous page 
	
	When User clicks the first page arrow button 
	Then User is taken to the first page 
	
	# 3. Search Panel - No Results
	When User clicks on the Search Panel Search field
	And User enters a word not contained in any contracts in the Search Panel Search field "dseuj3o20s"
	And User clicks the Search button 
	Then No results should display 
	
	# 4. Search Panel - Counterparty Name
	When User clicks the Reset button
	And User enters a name in the Counterparty Name field "Kangaroo LLC" 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Counterparty Name 
	
	# 5. Search Panel - Client Entity
	When User clicks the Reset button
	And User enters a name in the Client Entity field "Client Entity Test Company"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Client Entity 
	
	# 6. Search Panel - Document Type
	When User clicks the Reset button
	And User clicks the Document Type dropdown 
	And User selects a value from the Document Type dropdown 
	And User selects another value from the Document Type dropdown 
	And User clicks the X button next to one of the selected document type values 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Document Type 
	
	# 7. Search Panel - Add Filter Contract ID
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Contract ID from the Filter dropdown 
	And User enters an ID in the Contract ID field "8f983263-65b8-4fc3-baac-67a85efa6f95"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Contract ID 
	
	# 8. Search Panel - Add Filter Document Title
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Document Title from the Filter dropdown 
	And User enters a title in the Document Title field "Mole  Calcium   Echo"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Document Title 
	
	# 9. Search Panel - Add Filter Document Type (Define Other)
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Document Type (Define Other) from the Filter dropdown 
	And User enters a value in the Document Type (Define Other) field "Uranus"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Document Type (Define Other) 
	
	# 10. Search Panel - Effective Date
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Effective Date from the Filter dropdown 
	And User enters a set of dates in the Effective Date fields "03/03/2012"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Effective Date 
	
	# 11. Search Panel - Add Filter File Name
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects File Name from the Filter dropdown 
	And User enters a name in the File Name field "CertificateofIncorporation.pdf"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected File Name 
	
	# 12. Search Panel - Contract Execution Status
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Contract Execution Status from the Filter dropdown 
	And User selects a value from the filter dropdown 
	And User selects another value from the filter dropdown 
	And User clicks the X button next to one of the selected filter values 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Contract Execution Status 
	
	# 13. Search Panel - Contract Status
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Contract Status from the Filter dropdown 
	And User selects a value from the filter dropdown 
	And User selects another value from the filter dropdown 
	And User clicks the X button next to one of the selected filter values 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Contract Status 
	
	# 14. Search Panel - Parent Control Number
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Parent Control Number from the Filter dropdown 
	And User enters a value in the Parent Control Number field "1-EX10.1_A2COLA-1-8-13.pdf-1234"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Parent Control Number 
	
	# 15. Search Panel - Document Issues
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Document Issues from the Filter dropdown 
	And User selects a value from the filter dropdown 
	And User selects another value from the filter dropdown 
	And User clicks the X button next to one of the selected filter values 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Document Issues 
	
	# 16. Search Panel - Out of Scope
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Out of Scope from the Filter dropdown 
	And User selects a value from the filter dropdown 
	And User selects another value from the filter dropdown 
	And User clicks the X button next to one of the selected filter values 
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Out of Scope 
	
	# 17. Search Panel - Upload By
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Upload By from the Filter dropdown 
	And User enters a name in the Upload By field "Jesse Tracy"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Upload By 
	
	# 18. Search Panel - Upload Date
	When User clicks the Reset button
	And User clicks the Add Filter button 
	And User clicks the Select A Filter dropdown 
	And User selects Upload Date from the Filter dropdown 
	And User enters a set of dates in the Upload Date fields "07/19/2019"
	And User clicks the Search button 
	And User enters "INCORPORATION" in the Search Panel Search field
	And User clicks the Search button 
	Then Results should display for the selected Upload Date 
	
	# 19. Sort - One Column Sort Contracts

	When User sets column headers to default
	And User clicks the Reset button
	And User clicks the primary column header
	Then The primary column header is sorted in ascending order
	
	When User clicks the primary column header a second time
	Then The primary column header is sorted in descending order
	
	When User clicks the primary column header a third time
	Then The primary column header sort is not sorted
	
	# 20. Sort - Two Column Sort Contracts
	
	When User clicks the primary and secondary column headers
	Then Primary column sort = Ascending, Secondary column sort = Ascending
	
	When User clicks the secondary column header a second time
	Then Primary column sort = Ascending, Secondary column sort = Descending
	
	When User clicks the secondary column header a third time
	Then Primary column sort = Ascending, Secondary column sort = None
	
	When User clicks the primary column header a second time
	Then Primary column sort = Descending, Secondary column sort = None
	
	When User clicks the primary column header a third time
	Then No sort is applied
	
	When User clicks the primary and secondary column headers
	Then Primary column sort = Ascending, Secondary column sort = Ascending
	
	When User clicks the primary column header a second time
	Then Primary column sort = Descending, Secondary column sort = Ascending
	
	When User clicks the primary column header a third time
	Then Primary column sort = None, Secondary column sort = Ascending
	
	When User clicks the secondary column header a second time
	Then Primary column sort = None, Secondary column sort = Descending
	
	When User clicks the secondary column header a third time
	Then No sort is applied and columns are reset
