Feature: Contracts Tab - Detail View

Scenario: 
	- Select a Contract
	- Visible Contract Viewer Elements
	- Contract Controls
	- Data Tab - Contract Information
	- Search Tab
	- Replace Document - PDF
	- Replace Document - Non-PDF
	- Related Contracts (Contract Family)
	
	Given User is logged in to Knowable
		
	# Select a Contract
	When User clicks the Contracts tab
	Then User should be taken to the Contracts page
	
	When User searches for a specific contract in the list and clicks it
	Then The Contract Preview, Data and Search tabs, and the Related Contracts section load for the contract
	
	# Contract Controls
	When User clicks the right facing arrow once next to the number of total pages (1 of X)
	Then The second page of the contract is visible
	
	When User clicks the right face arrow through the entirety of the contract
	Then The last page of the contract is visible
	
	When User clicks the left facing arrow back through the entirety of the contract
	Then The first page of the contract is visible
	
	When User scrolls through the pages of the contract using the mouse wheel or trackpad
	Then The contract scrolls up and down
	
	When User clicks the page number input field	
	And User enters a page number not more than the number of total pages, and then presses enter or clicks anywhere outside of the field
	Then The chosen page of the contract is visible
	
	When User clicks the Download button on the Contract Viewer
	Then The contract downloads from the Contract Viewer
	
	When User clicks the Back button
	Then User should be taken to the Contracts page
	
	# Data Tab - Contract Information
	
	When User clicks one of the contracts listed on the Contracts page 
	Then The Contract Preview, Data and Search tabs, and the Related Contracts section load for the contract
	
	When User clicks on the Select... dropdown
	Then The list of field categories displays
	
	When User selects a category
	Then The category filter is applied and only fields for that particular category display
	
	When User selects another category
	Then The category filter is applied and only fields for that particular category, and the previously selected category, display
	
	When User clicks on the X beside one of the two category names in the dropdown field
	Then The category filter is removed for that category

	When User clicks a category header once
	Then User can hide and display fields in that category
	
	# Search Tab
	
	When User clicks on the Search Tab
	Then The Search tab is visible
	
	When User types "Resume" into the search field
	Then The search results are visible below the search field; the current result should be highlighted in green while the rest of the results should be highlighted in yellow
	
	When User clicks on the down arrow beside /# Resuts found in document
	Then The user is taken to the next instance of the search results in the document; the current result should be highlighted in green while the rest of the results should be highlighted in yellow
	
	When User clicks the Back button
	Then User should be taken to the Contracts page
	
#	# Replace Document - PDF
#	
#	# Cannot automate upload actions
#	
#	# Replace Document - Non-PDF
#	
#	# Cannot automate upload actions
#	
#	# Related Contracts (Contract Family)
	When User enters "Cow Co"  as the Counterparty Name
	And User clicks the Search button
	And User clicks one of the contracts listed on the Contracts page 
	Then The contract with related contracts loads
	
	When User clicks on a related contract
	Then The related contract and its information loads	
	