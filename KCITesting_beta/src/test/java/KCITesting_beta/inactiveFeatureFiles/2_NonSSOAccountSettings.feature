Feature: Non-SSO Account Settings 

Scenario: 
	- Navigate to Password Tab
	- Update Password
	- Login with New Password
	- Update Personal Information
	- Verify Personal Information
	- Navigate to Password Tab 
	- Incorrect Old Password
	- Incorrect Confirm Password
	- Correct Confirm Password
	
	#	Navigate to Password Tab
	Given User is logged in to Knowable
	When User clicks the top right profile button 
	Then The Welcome Menu should appear  
	
	When User clicks the My Account button 
	Then User should see the My Account pop-up 
	
	When User clicks the Password tab if not selected by default 
	Then User should be able to see password fields 
	
	#	Update Password
	When User enters default password in the Old Password field
	And User enters a new test password into the New Password field "PA$$word21"
	And User enters the correct password into the Confirm Password field "PA$$word21"
	And User clicks the password tab Update button 
	Then User should have successfully changed password and see success message 
	
	When User clicks the password tab Ok button 
	Then User should be able to see password fields 
	
	# 	Login with New Password
	When User clicks the password tab X button in the top right corner of the My Account pop-up 
	Then User should be taken to the Dashboard page
	
	When User clicks the top right profile button 
	And User clicks the log out button 
	Then User should be taken to the log in page 
	
	When User enters a valid email
	And User clicks Continue button 
	Then User should be taken to password page 
	
	When User enters the new test password "PA$$word21" 
	And User clicks Sign In button 
	Then User should be taken to the Dashboard page
	
	#	Update Personal Information
	When User clicks the top right profile button 
	Then The Welcome Menu should appear 
	
	When User clicks the My Account button 
	Then User should see the My Account pop-up 
	
	When User clicks the Profile Tab 
	Then User personal information should display 
	
	When User makes edits to First Name and Last Name fields "Eric" "Stephan"
	And User clicks the account tab Update button 
	Then User should have successfully updated their personal information and see success message 
	
	When User clicks the account tab Ok button 
	Then User personal information should display 
	
	# 	Verify Personal Information
	When User clicks the account tab X button in the top right corner of the My Account pop-up 
	Then User should be taken to the Dashboard page
	
	When User views the Welcome Message next to the Profile button 
	Then User should see updated first name in Welcome Message
	
	#	Navigate to Password Tab
	When User clicks the top right profile button 
	Then The Welcome Menu should appear 
	When User clicks the My Account button 
	Then User should see the My Account pop-up 
	
	When User clicks the Password tab if not selected by default 
	Then User should be able to see password fields 
	#	Incorrect Old Password
	When User enters incorrect password in the Old Password field
	And User enters the default password into the New Password field
	And User enters the correct password into the Confirm Password field
	And User clicks the password tab Update button again
	Then An error message should display and password should not be changed
	
	When User clicks the password tab Ok button again
	Then User should be able to see password fields 
	
	#	Incorrect Confirm Password
	When User enters the test password in the Old Password field "PA$$word21"
	And User enters the default password into the New Password field 
	And User enters the test password into the Confirm Password field "PA$$word21" 
	Then User should not be able to click Update button
	
	#	Correct Confirm Password
	When User enters the default password into the Confirm Password field
	And User clicks the password tab Update button again
	Then User should have successfully changed password and see success message 
	
	When User clicks the password tab Ok button again
	Then User should be able to see password fields 
	
	# 	Reset Account Information
	
	When User clicks the Profile Tab
	Then User personal information should display 
	
	When User resets their profile name
	And User clicks the account tab Update button 
	Then User should have successfully updated their personal information and see success message 
	
	When User clicks the account tab Ok button 
	Then User personal information should display