package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class ContractsTab_ListViewSteps extends DriverFactory {

	int filterDropdownQueueTracker = 0;
	
	@When("^User hovers over circle icon$")
	public void user_hovers_over_circle_icon() throws Throwable {
	    contractsListView.hoverOverCircleIcon();
	}

	@Then("^User sees the Contract Color Key$")
	public void user_sees_the_Contract_Color_Key() throws Throwable {
		contractsListView.confirmControlColorKeyTooltipDisplays();
	}

	@When("^User hovers over flag icon$")
	public void user_hovers_over_flag_icon() throws Throwable {
		contractsListView.hoverOverFlagIcon();
	}

	@Then("^User sees the Flag Color Key$")
	public void user_sees_the_Flag_Color_Key() throws Throwable {
		contractsListView.confirmFlagColorKeyTooltipDisplays();
	}

	@When("^User clicks the second page number button$")
	public void user_clicks_the_second_page_number_button() throws Throwable {
		contractsListView.clickSecondPageButton();
	}

	@Then("^User is taken to the second page$")
	public void user_is_taken_to_the_second_page() throws Throwable {
		contractsListView.confirmSecondPageIsDisplayed();
	}

	@When("^User clicks the next page arrow button$")
	public void user_clicks_the_next_page_arrow_button() throws Throwable {
		contractsListView.clickNextPageButton();
	}

	@Then("^User is taken to the third page$")
	public void user_is_taken_to_the_third_page() throws Throwable {
		contractsListView.confirmThirdPageIsDisplayed();
	}

	@When("^User clicks the last page arrow button$")
	public void user_clicks_the_last_page_arrow_button() throws Throwable {
		contractsListView.clickLastPageButton();
	}

	@Then("^User is taken to the last page$")
	public void user_is_taken_to_the_last_page() throws Throwable {
		contractsListView.confirmLastPageIsDisplayed();
	}

	@When("^User clicks the previous page arrow button$")
	public void user_clicks_the_previous_page_arrow_button() throws Throwable {
		contractsListView.clickPreviousPageButton();
	}

	@Then("^User is taken to the previous page$")
	public void user_is_taken_to_the_previous_page() throws Throwable {
		Thread.sleep(500);
		contractsListView.confirmPreviousPageIsDisplayed();
	}

	@When("^User clicks the first page arrow button$")
	public void user_clicks_the_first_page_arrow_button() throws Throwable {
		contractsListView.clickFirstPageButton();
	}

	@Then("^User is taken to the first page$")
	public void user_is_taken_to_the_first_page() throws Throwable {
		contractsListView.confirmFirstPageIsDisplayed();
	}

	@When("^User enters a word not contained in any contracts in the Search Panel Search field \"([^\"]*)\"$")
	public void user_enters_a_word_not_contained_in_any_contracts_in_the_Search_Panel_Search_field(String searchTerm) throws Throwable {
	    searchPanel.enterSearchTerm(searchTerm);
	}

	@Then("^No results should display$")
	public void no_results_should_display() throws Throwable {
	    contractsListView.confirmNoResultsFound();
	}

	@When("^User enters a name in the Counterparty Name field \"([^\"]*)\"$")
	public void user_enters_a_name_in_the_Counterparty_Name_field(String counterpartyName) throws Throwable {
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_01);
		searchPanel.enterCounterpartyName(counterpartyName);
	}

	@Then("^Results should display for the selected Counterparty Name$")
	public void results_should_display_for_the_selected_Counterparty_Name() throws Throwable {
		contractsListView.confirmCounterpartyNameResults();
	}

	@When("^User enters a name in the Client Entity field \"([^\"]*)\"$")
	public void user_enters_a_name_in_the_Client_Entity_field(String clientEntity) throws Throwable {
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_07);
		searchPanel.enterClientEntity(clientEntity);
	}

	@Then("^Results should display for the selected Client Entity$")
	public void results_should_display_for_the_selected_Client_Entity() throws Throwable {
		contractsListView.confirmClientEntityResults();
	}

	@When("^User selects a value from the Document Type dropdown$")
	public void user_selects_a_value_from_the_Document_Type_dropdown() throws Throwable {
	    documentTypeOptions.hoverAllDocumentTypeOptions();
	    documentTypeOptions.clickSeventhOption();
	}

	@When("^User selects another value from the Document Type dropdown$")
	public void user_selects_another_value_from_the_Document_Type_dropdown() throws Throwable {
	    documentTypeOptions.clickFirstOption();
	    multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_05);
	}

	@When("^User clicks the X button next to one of the selected document type values$")
	public void user_clicks_the_X_button_next_to_one_of_the_selected_document_type_values() throws Throwable {
		searchPanel.clickDocumentTypeXButton();
	}

	@Then("^Results should display for the selected Document Type$")
	public void results_should_display_for_the_selected_Document_Type() throws Throwable {
		contractsListView.confirmDocumentTypeResults();
	}

	@When("^User clicks the Add Filter button$")
	public void user_clicks_the_Add_Filter_button() throws Throwable {
	    searchPanel.clickAddFilterButton();
	}

	@When("^User clicks the Select A Filter dropdown$")
	public void user_clicks_the_Select_A_Filter_dropdown() throws Throwable {
	    searchPanel.clickSelectAFilterButton();
	}

	@When("^User selects Contract ID from the Filter dropdown$")
	public void user_selects_Contract_ID_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectContractIDFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_08);
	}

	@When("^User enters an ID in the Contract ID field \"([^\"]*)\"$")
	public void user_enters_an_ID_in_the_Contract_ID_field(String contractID) throws Throwable {
		searchPanel.enterAddedFilterText(contractID);
	}

	@Then("^Results should display for the selected Contract ID$")
	public void results_should_display_for_the_selected_Contract_ID() throws Throwable {
		contractsListView.confirmContractIDResults();
	}

	@When("^User selects Document Title from the Filter dropdown$")
	public void user_selects_Document_Title_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectDocumentTitleFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_02);
	}

	@When("^User enters a title in the Document Title field \"([^\"]*)\"$")
	public void user_enters_a_title_in_the_Document_Title_field(String docTitle) throws Throwable {
		searchPanel.enterAddedFilterText(docTitle);
	}

	@Then("^Results should display for the selected Document Title$")
	public void results_should_display_for_the_selected_Document_Title() throws Throwable {
		contractsListView.confirmDocumentTitleResults();
	}

	@When("^User selects Document Type \\(Define Other\\) from the Filter dropdown$")
	public void user_selects_Document_Type_Define_Other_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectDocumentTypeDefineOtherFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_06);
	}

	@When("^User enters a value in the Document Type \\(Define Other\\) field \"([^\"]*)\"$")
	public void user_enters_a_value_in_the_Document_Type_Define_Other_field(String docTypeDefOther) throws Throwable {
		searchPanel.enterAddedFilterText(docTypeDefOther);
	}

	@Then("^Results should display for the selected Document Type \\(Define Other\\)$")
	public void results_should_display_for_the_selected_Document_Type_Define_Other() throws Throwable {
		contractsListView.confirmDocumentTypeDefineOtherResults();
	}

	@When("^User selects Effective Date from the Filter dropdown$")
	public void user_selects_Effective_Date_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectEffectiveDateFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_04);
	}

	@When("^User enters a set of dates in the Effective Date fields \"([^\"]*)\"$")
	public void user_enters_a_set_of_dates_in_the_Effective_Date_fields(String date) throws Throwable {
		searchPanel.enterAddedFilterDates(date, date);
	}

	@Then("^Results should display for the selected Effective Date$")
	public void results_should_display_for_the_selected_Effective_Date() throws Throwable {
		contractsListView.confirmEffectiveDateResults();
	}

	@When("^User selects File Name from the Filter dropdown$")
	public void user_selects_File_Name_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectFileNameFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_03);
	}

	@When("^User enters a name in the File Name field \"([^\"]*)\"$")
	public void user_enters_a_name_in_the_File_Name_field(String fileName) throws Throwable {
		searchPanel.enterAddedFilterText(fileName);
	}

	@Then("^Results should display for the selected File Name$")
	public void results_should_display_for_the_selected_File_Name() throws Throwable {
		contractsListView.confirmFileNameResults();
	}

	@When("^User selects Contract Execution Status from the Filter dropdown$")
	public void user_selects_Contract_Execution_Status_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectContractExecutionStatusFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_24);
	}

	@When("^User selects a value from the filter dropdown$")
	public void user_selects_a_value_from_the_filter_dropdown() throws Throwable {
		searchPanel.clickAddedFilterDropwdown();
	    switch (filterDropdownQueueTracker) {
	    case 0:
	    	addedFilterDropdownOptions.hoverAllContractExecutionStatusOptions();
	    	addedFilterDropdownOptions.clickThirdOption();
	    	break;
	    case 1:
	    	addedFilterDropdownOptions.hoverAllContractStatusOptions();
	    	addedFilterDropdownOptions.clickSecondOption();
	    	break;
	    case 2:
	    	addedFilterDropdownOptions.hoverAllDocumentIssuesOptions();
	    	addedFilterDropdownOptions.clickTenthOption();
	    	break;
	    case 3:
	    	addedFilterDropdownOptions.hoverAllOutOfScopeOptions();
	    	addedFilterDropdownOptions.clickSecondOption();
	    	break;
	    default:
	    	break;
	    }
    	filterDropdownQueueTracker++;
	}

	@When("^User selects another value from the filter dropdown$")
	public void user_selects_another_value_from_the_filter_dropdown() throws Throwable {
		addedFilterDropdownOptions.clickFirstOption();
		searchPanel.clickAddedFilterDropwdown();			
	}

	@When("^User clicks the X button next to one of the selected filter values$")
	public void user_clicks_the_X_button_next_to_one_of_the_selected_filter_values() throws Throwable {
	    searchPanel.clickAddedFilterXButton();
	}

	@Then("^Results should display for the selected Contract Execution Status$")
	public void results_should_display_for_the_selected_Contract_Execution_Status() throws Throwable {
		contractsListView.confirmContractExecutionStatusResults();
	}

	@When("^User selects Contract Status from the Filter dropdown$")
	public void user_selects_Contract_Status_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectContractStatusFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_179);
	}

	@Then("^Results should display for the selected Contract Status$")
	public void results_should_display_for_the_selected_Contract_Status() throws Throwable {
		contractsListView.confirmContractStatusResults();
	}

	@When("^User selects Parent Control Number from the Filter dropdown$")
	public void user_selects_Parent_Control_Number_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectParentControlNumberFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_14);
	}

	@When("^User enters a value in the Parent Control Number field \"([^\"]*)\"$")
	public void user_enters_a_value_in_the_Parent_Control_Number_field(String parentControlNumber) throws Throwable {
		searchPanel.enterAddedFilterText(parentControlNumber);
	}

	@Then("^Results should display for the selected Parent Control Number$")
	public void results_should_display_for_the_selected_Parent_Control_Number() throws Throwable {
		contractsListView.confirmParentControlNumberResults();
	}

	@When("^User selects Document Issues from the Filter dropdown$")
	public void user_selects_Document_Issues_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectDocumentIssuesFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_176);
	}

	@Then("^Results should display for the selected Document Issues$")
	public void results_should_display_for_the_selected_Document_Issues() throws Throwable {
		contractsListView.confirmDocumentIssuesResults();
	}

	@When("^User selects Out of Scope from the Filter dropdown$")
	public void user_selects_Out_of_Scope_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectOutOfScopeFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_175);
	}

	@Then("^Results should display for the selected Out of Scope$")
	public void results_should_display_for_the_selected_Out_of_Scope() throws Throwable {
		contractsListView.confirmOutOfScopeResults();
	}

	@When("^User selects Upload By from the Filter dropdown$")
	public void user_selects_Upload_By_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectUploadByFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_177);
	}

	@When("^User enters a name in the Upload By field \"([^\"]*)\"$")
	public void user_enters_a_name_in_the_Upload_By_field(String name) throws Throwable {
		searchPanel.enterAddedFilterText(name);
	}

	@Then("^Results should display for the selected Upload By$")
	public void results_should_display_for_the_selected_Upload_By() throws Throwable {
		contractsListView.confirmUploadByResults();
	}

	@When("^User selects Upload Date from the Filter dropdown$")
	public void user_selects_Upload_Date_from_the_Filter_dropdown() throws Throwable {
	    filterOptions.selectUploadDateFilter();
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setSingleCustomizedFields(availableFieldsListOptions.listOption_178);
	}

	@When("^User enters a set of dates in the Upload Date fields \"([^\"]*)\"$")
	public void user_enters_a_set_of_dates_in_the_Upload_Date_fields(String date) throws Throwable {
		searchPanel.enterAddedFilterDates(date, date);
	}

	@Then("^Results should display for the selected Upload Date$")
	public void results_should_display_for_the_selected_Upload_Date() throws Throwable {
		contractsListView.confirmUploadDateResults();
	}

	@When("^User sets column headers to default$")
	public void user_sets_column_headers_to_default() throws Throwable {
		multiObjectFunctions.emptyCustomizedFields();
		multiObjectFunctions.setTwoCustomizedFields(availableFieldsListOptions.listOption_07, availableFieldsListOptions.listOption_05);
		//multiObjectFunctions.setDefaultColumnHeaders();
	}
	
	@When("^User clicks the primary column header$")
	public void user_clicks_the_primary_column_header() throws Throwable {
	    contractsListView.sortPrimaryColumnHeader();
	}

	@Then("^The primary column header is sorted in ascending order$")
	public void the_primary_column_header_is_sorted_in_ascending_order() throws Throwable {
	    contractsListView.verifySortIsAscendingOrder(0);
	}

	@When("^User clicks the primary column header a second time$")
	public void user_clicks_the_primary_column_header_a_second_time() throws Throwable {
	    contractsListView.sortPrimaryColumnHeader();
	}

	@Then("^The primary column header is sorted in descending order$")
	public void the_primary_column_header_is_sorted_in_descending_order() throws Throwable {
	    contractsListView.verifySortIsDescendingOrder(0);
	}

	@When("^User clicks the primary column header a third time$")
	public void user_clicks_the_primary_column_header_a_third_time() throws Throwable {
	    contractsListView.sortPrimaryColumnHeader();
	}

	@When("^User clicks the primary and secondary column headers$")
	public void user_clicks_the_primary_and_secondary_column_headers() throws Throwable {
	    contractsListView.sortPrimaryColumnHeader();
	    contractsListView.sortSecondaryColumnHeader();
	}

	@When("^User clicks the secondary column header a second time$")
	public void user_clicks_the_secondary_column_header_a_second_time() throws Throwable {
	    contractsListView.sortSecondaryColumnHeader();
	}

	@When("^User clicks the secondary column header a third time$")
	public void user_clicks_the_secondary_column_header_a_third_time() throws Throwable {
	    contractsListView.sortSecondaryColumnHeader();
	}

	@Then("^The primary column header sort is not sorted$")
	public void the_primary_column_header_sort_is_not_sorted() throws Throwable {
		System.out.println("PRIMARY column is no longer sorted.");
	}
	
	@Then("^No sort is applied$")
	public void no_sort_is_applied() throws Throwable {
	    Thread.sleep(500);
		System.out.println("PRIMARY column is no longer sorted.");
		System.out.println("SECONDARY column is no longer sorted.");
	}
	
	@Then("^Primary column sort = Ascending, Secondary column sort = Ascending$")
	public void primary_column_sort_Ascending_Secondary_column_sort_Ascending() throws Throwable {
	    contractsListView.verifySortIsAscendingOrder(0);
	    contractsListView.verifySortIsAscendingOrder(1);
	}
	
	@Then("^Primary column sort = Ascending, Secondary column sort = Descending$")
	public void primary_column_sort_Ascending_Secondary_column_sort_Descending() throws Throwable {
	    contractsListView.verifySortIsAscendingOrder(0);
	    contractsListView.verifySortIsDescendingOrder(1);
	}

	@Then("^Primary column sort = Ascending, Secondary column sort = None$")
	public void primary_column_sort_Ascending_Secondary_column_sort_None() throws Throwable {
	    contractsListView.verifySortIsAscendingOrder(0);
		System.out.println("SECONDARY column is no longer sorted.");
	}

	@Then("^Primary column sort = Descending, Secondary column sort = None$")
	public void primary_column_sort_Descending_Secondary_column_sort_None() throws Throwable {
	    contractsListView.verifySortIsDescendingOrder(0);
		System.out.println("SECONDARY column is no longer sorted.");
	}
	
	@Then("^Primary column sort = Descending, Secondary column sort = Ascending$")
	public void primary_column_sort_Descending_Secondary_column_sort_Ascending() throws Throwable {
	    contractsListView.verifySortIsDescendingOrder(0);
	    contractsListView.verifySortIsAscendingOrder(1);
	}

	@Then("^Primary column sort = None, Secondary column sort = Ascending$")
	public void primary_column_sort_None_Secondary_column_sort_Ascending() throws Throwable {
		System.out.println("PRIMARY column is no longer sorted.");
	    contractsListView.verifySortIsAscendingOrder(1);
	}

	@Then("^Primary column sort = None, Secondary column sort = Descending$")
	public void primary_column_sort_None_Secondary_column_sort_Descending() throws Throwable {
		System.out.println("PRIMARY column is no longer sorted.");
	    contractsListView.verifySortIsDescendingOrder(2);
	}
	
	@Then("^No sort is applied and columns are reset$")
	public void no_sort_is_applied_and_columns_are_reset() throws Throwable {
	    Thread.sleep(500);
		System.out.println("PRIMARY column is no longer sorted.");
		System.out.println("SECONDARY column is no longer sorted.");
		multiObjectFunctions.setDefaultColumnHeaders();
	}



	
}
