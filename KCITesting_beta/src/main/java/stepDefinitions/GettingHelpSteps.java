package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class GettingHelpSteps extends DriverFactory {

	
	@When("^User clicks the Help Icon in the upper right hand corner$")
	public void user_clicks_the_Help_Icon_in_the_upper_right_hand_corner() throws Throwable {
	    pageHeader.clickHelpButton();
	}

	@Then("^User is taken to the Help page$")
	public void user_is_taken_to_the_Help_page() throws Throwable {
	    helpPage.confirmHelpPageIsDisplayed();
	}

	@When("^user clicks on each of the topics under the Frequently Asked Questions section$")
	public void user_clicks_on_each_of_the_topics_under_the_Frequently_Asked_Questions_section() throws Throwable {
		helpPage.clickAllTopics();
	}

	@Then("^Question under those topics display$")
	public void question_under_those_topics_display() throws Throwable {
		helpPage.confirmQuestionsAreDisplayed();
	}

	@When("^User clicks each of the questions under each topic$")
	public void user_clicks_each_of_the_questions_under_each_topic() throws Throwable {
		helpPage.clickAllQuestions();
	}

	@Then("^The answer to the questions appear$")
	public void the_answer_to_the_questions_appear() throws Throwable {
		helpPage.confirmAnswersAreDisplayed();
	}

	@When("^User clicks on the questions again$")
	public void user_clicks_on_the_questions_again() throws Throwable {
		helpPage.clickAllQuestions();
	}

	@Then("^The answers are hidden from the user$")
	public void the_answers_are_hidden_from_the_user() throws Throwable {
		helpPage.confirmAnswersAreNotDisplayed();
	}
	
	@When("^User clicks on the topics again$")
	public void user_clicks_on_the_topics_again() throws Throwable {
		helpPage.clickAllTopics();
	}

	@Then("^The questions are hidden from the user$")
	public void the_questions_are_hidden_from_the_user() throws Throwable {
		helpPage.confirmQuestionsAreNotDisplayed();
	}

	@When("^User clicks the Download button under the Download User Documentation section$")
	public void user_clicks_the_Download_button_under_the_Download_User_Documentation_section() throws Throwable {
		helpPage.clickDownloadUserDocumentationSectionButton();
	}

	@Then("^User is able to download and view the user guide$")
	public void user_is_able_to_download_and_view_the_user_guide() throws Throwable {
		multiObjectFunctions.isFileDownloaded("help.pdf");
	}

	@When("^User clicks on the Download User Documentation button in the top right of the page$")
	public void user_clicks_on_the_Download_User_Documentation_button_in_the_top_right_of_the_page() throws Throwable {
		helpPage.clickTopRightDownloadButton();
	}

	@When("^User clicks on the Contact Production Support icon$")
	public void user_clicks_on_the_Contact_Production_Support_icon() throws Throwable {
		helpPage.clickSupportFormButton();
	}

	@Then("^The Contact Product Support form appears$")
	public void the_Contact_Product_Support_form_appears() throws Throwable {
		helpPage.confirmSupportFormDisplays();
	}

	@When("^User clicks Cancel Request$")
	public void user_clicks_Cancel_Request() throws Throwable {
		helpPage.clickCancelButton();
	}

	@Then("^The Contact Product Support form disappears$")
	public void the_Contact_Product_Support_form_disappears() throws Throwable {
	    helpPage.confirmHelpPageIsDisplayed();
	}

	@When("^User clicks on the Back button at the top of the Help page$")
	public void user_clicks_on_the_Back_button_at_the_top_of_the_Help_page() throws Throwable {
		helpPage.clickBackButton();
	}

	@Then("^The page the user previously viewed displays$")
	public void the_page_the_user_previously_viewed_displays() throws Throwable {
		dashboardPage.confirmUserIsOnTheDashboardPage();
	}
}
