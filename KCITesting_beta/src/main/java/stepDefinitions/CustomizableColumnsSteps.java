package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class CustomizableColumnsSteps extends DriverFactory {

	String fieldOrder;
	
	@When("^User clicks the More \\(\\.\\.\\.\\) column header$")
	public void user_clicks_the_More_column_header() throws Throwable {
	    contractsListView.clickMoreButtonOnContract();
	}

	@Then("^The list of options appears$")
	public void the_list_of_options_appears() throws Throwable {
		contractsListView.confirmCustomizeTableButtonExists();
	}

	@When("^User clicks Customize Table$")
	public void user_clicks_Customize_Table() throws Throwable {
	    contractsListView.clickCustomizeTableButton();
	}

	@Then("^The Customize Table pop-up appears$")
	public void the_Customize_Table_pop_up_appears() throws Throwable {
	    customizeTableView.confirmCustomizeTablePopopIsDisplayed();
	}

	@When("^User searches for a field using search bar above Available Fields list with \"([^\"]*)\"$")
	public void user_searches_for_a_field_using_search_bar_above_Available_Fields_list_with(String string) throws Throwable {
	    customizeTableView.enterTermInSearchAvailableFieldsField(string);
	}

	@Then("^Fields relating to search criteria are displayed in the Available Fields list$")
	public void fields_relating_to_search_criteria_are_displayed_in_the_Available_Fields_list() throws Throwable {
	    availableFieldsListOptions.confirmDocumentTypeDefineOtherOptionExistsInList();
	}

	@When("^User removes the search criteria from the search field$")
	public void user_removes_the_search_criteria_from_the_search_field() throws Throwable {
		customizeTableView.clearSearchAvailableFieldsField();
	}

	@Then("^The Available Fields list resets$")
	public void the_Available_Fields_list_resets() throws Throwable {
	    // Steps to prevent failure until Knowable fixes issue with 'Search Available Fields' field not resetting Available Fields list when cleared
		customizeTableView.clickSelectACategoryDropdown();
		availableFieldsDropdownOptions.clickSelectACategoryOption();
		// ---------------------------------------------------------
		availableFieldsListOptions.confirmDefaultAvailableFieldsList();
	}

	@When("^User selects a category from the Category dropdown$")
	public void user_selects_a_category_from_the_Category_dropdown() throws Throwable {
		customizeTableView.clickSelectACategoryDropdown();
		availableFieldsDropdownOptions.clickContractIdentificationOption();
	}

	@Then("^Fields relating to the selected category are displayed in the Available Fields list$")
	public void fields_relating_to_the_selected_category_are_displayed_in_the_Available_Fields_list() throws Throwable {
		availableFieldsListOptions.confirmAvailableFieldsListAfterDropdownSelection();
	}

	@When("^User chooses Select a Category from the Category dropdown$")
	public void user_chooses_Select_a_Category_from_the_Category_dropdown() throws Throwable {
		customizeTableView.clickSelectACategoryDropdown();
		availableFieldsDropdownOptions.clickSelectACategoryOption();
	}

	@Then("^The category is removed and the list of all fields display in the Available Fields list$")
	public void the_category_is_removed_and_the_list_of_all_fields_display_in_the_Available_Fields_list() throws Throwable {
		availableFieldsListOptions.confirmDefaultAvailableFieldsList();
	}

	@When("^User selects the desired field from the Available Fields list$")
	public void user_selects_the_desired_field_from_the_Available_Fields_list() throws Throwable {
		availableFieldsListOptions.clickFirstAvailableFieldOption();
	}

	@Then("^The desired field is highlighted in the Available Fields list$")
	public void the_desired_field_is_highlighted() throws Throwable {
		availableFieldsListOptions.confirmFirstAvailableFieldOptionIsSelected();
	}

	@When("^User clicks the right arrow to move the field to the Customizable Fields list$")
	public void user_clicks_the_right_arrow_to_move_the_field_to_the_Customizable_Fields_list() throws Throwable {
		customizeTableView.clickTheRightArrowButton();
	}

	@Then("^The desired field is displayed at the top of the Customizable Fields list$")
	public void the_desired_field_is_displayed_at_the_top_of_the_Customizable_Fields_list() throws Throwable {
		customizableFieldsListOptions.confirmFirstCustomizableFieldOptionIsSelected();
	}

	@When("^User selects another field from the Customizeable Fields list$")
	public void user_selects_another_field_from_the_Customizeable_Fields_list() throws Throwable {
		customizableFieldsListOptions.clickSecondCustomizableFieldOption();
	}
	
	@Then("^The desired field is highlighted in the Customizable Fields list$")
	public void the_desired_field_is_highlighted_in_the_Customizable_Fields_list() throws Throwable {
		customizableFieldsListOptions.confirmSecondCustomizableFieldOptionIsSelected();
	}

	@When("^User clicks the left arrow to move the field to the Available Fields list$")
	public void user_clicks_the_left_arrow_to_move_the_field_to_the_Available_Fields_list() throws Throwable {
		customizeTableView.clickTheLeftArrowButton();
	}

	@Then("^The desired field is displayed at the top of the Available Fields list$")
	public void the_desired_field_is_displayed_at_the_top_of_the_Available_Fields_list() throws Throwable {
		availableFieldsListOptions.confirmFirstAvailableFieldOptionIsSelected();
		customizeTableView.clickSetToDefaultButton();
	}

	@When("^User selects the first field in the Customizable fields list$")
	public void user_selects_the_first_field_in_the_Customizable_fields_list() throws Throwable {
		customizableFieldsListOptions.clickFirstCustomizableFieldOption();
		customizableFieldsListOptions.confirmFirstCustomizableFieldOptionIsSelected();
	}

	@When("^User clicks the down arrow$")
	public void user_clicks_the_down_arrow() throws Throwable {
		customizeTableView.clickTheDownArrowButton();
	}

	@Then("^The desired field moves down in the list$")
	public void the_desired_field_moves_down_in_the_list() throws Throwable {
		customizableFieldsListOptions.confirmSecondCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheDownArrowButton();
		customizableFieldsListOptions.confirmThirdCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheDownArrowButton();
		customizableFieldsListOptions.confirmFourthCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheDownArrowButton();
		customizableFieldsListOptions.confirmFifthCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheDownArrowButton();
		customizableFieldsListOptions.confirmLastCustomizableFieldOptionIsSelected();
	}

	@When("^User clicks the up arrow$")
	public void user_clicks_the_up_arrow() throws Throwable {
		customizeTableView.clickTheUpArrowButton();
	}

	@Then("^The desired field moves up in the list$")
	public void the_desired_field_moves_up_in_the_list() throws Throwable {
		customizableFieldsListOptions.confirmFifthCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheUpArrowButton();
		customizableFieldsListOptions.confirmFourthCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheUpArrowButton();
		customizableFieldsListOptions.confirmThirdCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheUpArrowButton();
		customizableFieldsListOptions.confirmSecondCustomizableFieldOptionIsSelected();
		customizeTableView.clickTheUpArrowButton();
		customizableFieldsListOptions.confirmFirstCustomizableFieldOptionIsSelected();
	}

	@When("^User clicks the bottom arrow$")
	public void user_clicks_the_bottom_arrow() throws Throwable {
		customizeTableView.clickTheBottomArrowButton();
	}

	@Then("^The desired field moves to the bottom of the list$")
	public void the_desired_field_moves_to_the_bottom_of_the_list() throws Throwable {
		customizableFieldsListOptions.confirmLastCustomizableFieldOptionIsSelected();
	}

	@When("^User clicks the top arrow$")
	public void user_clicks_the_top_arrow() throws Throwable {
		customizeTableView.clickTheTopArrowButton();
	}

	@Then("^The desired field moves to the top of the list$")
	public void the_desired_field_moves_to_the_top_of_the_list() throws Throwable {
		customizableFieldsListOptions.confirmFirstCustomizableFieldOptionIsSelected();
	}

	@When("^User rearranges the order of fields in the Customizable Fields list$")
	public void user_rearranges_the_order_of_fields_in_the_Customizable_Fields_list() throws Throwable {
		for (int i = 0; i <= 3; i++) {
			customizableFieldsListOptions.clickThirdCustomizableFieldOption();
			customizeTableView.clickTheUpArrowButton();
			customizableFieldsListOptions.clickFourthCustomizableFieldOption();
			customizeTableView.clickTheDownArrowButton();
			customizableFieldsListOptions.clickFirstCustomizableFieldOption();
			customizeTableView.clickTheDownArrowButton();
			customizableFieldsListOptions.clickSixthCustomizableFieldOption();
			customizeTableView.clickTheUpArrowButton();
		}		
		fieldOrder = customizableFieldsListOptions.getFieldOrder();
	}
	
	@When("^User clicks the Save Preferences button$")
	public void user_clicks_the_Save_Preferences_button() throws Throwable {
		customizeTableView.clickSavePreferencesButton();
	}

	@Then("^The pop-up closes and the Contracts tab is displayed with the selected columns in the desired order$")
	public void the_pop_up_closes_and_the_Contracts_tab_is_displayed_with_the_selected_columns_in_the_desired_order() throws Throwable {
		Thread.sleep(500);
		contractsListView.compareColumnHeaderOrderToCustomizableFieldsOrder(fieldOrder);
	}

	@When("^User clicks and drags a column header to the right of another column header$")
	public void user_clicks_and_drags_a_column_header_to_the_right_of_another_column_header() throws Throwable {
	    contractsListView.clickAndDragColumnHeader();
	}

	@Then("^The column moves to the desired location$")
	public void the_column_moves_to_the_desired_location() throws Throwable {
	    contractsListView.compareColumnHeadersAfterClickDragging();
	}

	@When("^User clicks the Set to Default button below the Customizable Columns list$")
	public void user_clicks_the_Set_to_Default_button_below_the_Customizable_Columns_list() throws Throwable {
	    customizeTableView.clickSetToDefaultButton();
	}

	@Then("^The default fields display in the Customizable columns list in the default order$")
	public void the_default_fields_display_in_the_Customizable_columns_list_in_the_default_order() throws Throwable {
		customizableFieldsListOptions.confirmDefaultCustomizableFieldsList();
		fieldOrder = customizableFieldsListOptions.getFieldOrder();
	}

	@Then("^The default columns display in the default order on the Contracts table$")
	public void the_default_columns_display_in_the_default_order_on_the_Contracts_table() throws Throwable {
		contractsListView.compareColumnHeaderOrderToCustomizableFieldsOrder(fieldOrder);
	}

	
}
