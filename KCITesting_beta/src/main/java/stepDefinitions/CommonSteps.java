package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class CommonSteps extends DriverFactory {

	@Given("^User is logged in to Knowable$")
	public void user_is_logged_in_to_Knowable() throws Throwable {
		loginKnowablePage.fullLogin();
	}
	
	@When("^User clicks the logo in the top left hand corner$")
	public void user_clicks_the_logo_in_the_top_left_hand_corner() throws Throwable {
		pageHeader.clickKnowableIcon();
	}
	
	@When("^User clicks the Dashboard tab$")
	public void user_clicks_the_Dashboard_tab() throws Throwable {
		pageHeader.clickDashboardTab();
	}
	
	@Then("^User should be taken to the Dashboard page$")
	public void user_should_be_taken_to_the_Dashboard_page() throws Throwable {
		dashboardPage.confirmUserIsOnTheDashboardPage();
	}
	
	@When("^User clicks the Contracts tab$")
	public void user_clicks_the_Contracts_tab() throws Throwable {
		pageHeader.clickContractsTab();
	}
	
	@Then("^User should be taken to the Contracts page$")
	public void user_should_be_taken_to_the_Contracts_page() throws Throwable {
		contractsListView.confirmUserIsOnTheContractsListViewPage();
	}
	
	@When("^User clicks the Upload tab$")
	public void user_clicks_the_Upload_tab() throws Throwable {
		pageHeader.clickUploadTab();
	}
	
	@Then("^User should be taken to the Upload page$")
	public void user_should_be_taken_to_the_Upload_page() throws Throwable {
		uploadPage.confirmUserIsOnTheUploadPage(); 
	}
	
	@When("^User clicks the top right profile button$")
	public void user_clicks_the_top_right_profile_button() throws Throwable {
		pageHeader.clickProfileButton();
	}
	
	@And("^User clicks the log out button$")
	public void user_clicks_the_log_out_button() throws Throwable {
		pageHeader.clickLogoutButton();
	}
	
	@When("^User clicks on the Search Panel Search field$")
	public void user_clicks_on_the_Search_Panel_Search_field() throws Throwable {
		searchPanel.clickSearchField();
	}
	
	@When("^User clicks the Search button$")
	public void user_clicks_the_Search_button() throws Throwable {
		searchPanel.clickSearchButton();
	}
	
	@When("^User clicks the Reset button$")
	public void user_clicks_the_Reset_button() throws Throwable {
		searchPanel.clickResetButton();
	}
	
	@When("^User clicks the Document Type dropdown$")
	public void user_clicks_the_Document_type_dropdown() throws Throwable {
		searchPanel.clickDocumentTypeDropdown();
	}
	
	@When("^User enters \"([^\"]*)\" in the Search Panel Search field$")
	public void user_enters_in_the_tab_Search_field(String searchTerm) throws Throwable {
	    searchPanel.enterSearchTerm(searchTerm);
	}
	
	@When("^User clicks the Back button$")
	public void user_clicks_the_Back_button() throws Throwable {
		contractsViewerView.clickOnBackButton();
	}
	
	@Then("^The Contract Preview, Data and Search tabs, and the Related Contracts section load for the contract$")
	public void the_Contract_Preview_Data_and_Search_tabs_and_the_Related_Contracts_section_load_for_the_contract()
			throws Throwable {
		multiObjectFunctions.confirmDetailViewElementsExist();
	}
	
	@When("^User searches for a specific contract in the list and clicks it$")
	public void user_searches_for_a_specific_contract_in_the_list_and_clicks_it() throws Throwable {
	    searchPanel.clickAddFilterButton();
	    searchPanel.clickAddedFilterDropwdown();
	    filterOptions.selectFileNameFilter();
	    searchPanel.enterAddedFilterText("resume-samples.pdf");
	    searchPanel.clickSearchButton();
	    contractsListView.clickContractAtTopOfList();
	}
}
