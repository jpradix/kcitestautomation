package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class DownloadAContractSteps extends DriverFactory {

	@When("^User clicks the More option to the right of a contract under Recently Viewed Contracts$")
	public void user_clicks_the_More_option_to_the_right_of_a_contract_under_Recently_Viewed_Contracts()
			throws Throwable {
		System.out.println("STEP: Click the More button on a recently viewed contract");
		searchPanel.clickRecentContractMoreButton();
	}

	@Then("^The download button appears next to the recently viewed contract$")
	public void the_download_button_appears_next_to_the_recently_viewed_contract() throws Throwable {
		searchPanel.confirmRecentlyViewedContractDownloadButtonExists();
	}

	@When("^User clicks the Download button under Recently Viewed Contracts$")
	public void user_clicks_the_Download_button_under_Recently_Viewed_Contracts() throws Throwable {
		System.out.println("STEP: Click the download button on a recently viewed contract");
		searchPanel.clickRecentContractDownloadButton();
	}

	@Then("^The contract downloads from the Recently Viewed Contracts$")
	public void the_contract_downloads_from_the_Recently_Viewed_Contracts() throws Throwable {
		multiObjectFunctions.isFileDownloaded(searchPanel.fileName);
	}

	@When("^User clicks the More option to the right of a contract in the list$")
	public void user_clicks_the_More_option_to_the_right_of_a_contract_in_the_list() throws Throwable {
		System.out.println("STEP: Click the More button next a contract on the list view");
		contractsListView.clickTopContractMoreButton();
	}

	@Then("^The download button appears next to the top contract$")
	public void the_download_button_appears_next_to_the_top_contract() throws Throwable {
		contractsListView.confirmTopContractContractDownloadButtonExists();
	}

	@When("^User clicks the Download button on the Contract List View$")
	public void user_clicks_the_Download_button_on_the_Contract_List_View() throws Throwable {
		System.out.println("STEP: Click the download button on the list view");
		contractsListView.clickTopContractDownloadButton();
	}

	@Then("^The contract downloads from the Contract List View$")
	public void the_contract_downloads_from_the_Contract_List_View() throws Throwable {
		multiObjectFunctions.isFileDownloaded(contractsListView.fileName);
	}

}
