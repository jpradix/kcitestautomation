package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class ContractsTab_DetailViewSteps extends DriverFactory {

	@When("^User clicks the right facing arrow once next to the number of total pages \\((\\d+) of X\\)$")
	public void user_clicks_the_right_facing_arrow_once_next_to_the_number_of_total_pages_of_X(int arg1)
			throws Throwable {
		contractsViewerView.clickRightArrowButton();
	}

	@Then("^The second page of the contract is visible$")
	public void the_second_page_of_the_contract_is_visible() throws Throwable {
		contractsViewerView.confirmContractSecondPageIsVisible();
	}

	@When("^User clicks the right face arrow through the entirety of the contract$")
	public void user_clicks_the_right_face_arrow_through_the_entirety_of_the_contract() throws Throwable {
		contractsViewerView.clickRightArrowButtonThroughContract();
	}

	@Then("^The last page of the contract is visible$")
	public void the_last_page_of_the_contract_is_visible() throws Throwable {
		contractsViewerView.confirmContractLastPageIsVisible();
	}

	@When("^User clicks the left facing arrow back through the entirety of the contract$")
	public void user_clicks_the_left_facing_arrow_back_through_the_entirety_of_the_contract() throws Throwable {
		contractsViewerView.clickLeftArrowButtonThroughContract();
	}

	@Then("^The first page of the contract is visible$")
	public void the_first_page_of_the_contract_is_visible() throws Throwable {
		contractsViewerView.confirmContractFirstPageIsVisible();
	}

	@When("^User scrolls through the pages of the contract using the mouse wheel or trackpad$")
	public void user_scrolls_through_the_pages_of_the_contract_using_the_mouse_wheel_or_trackpad() throws Throwable {
		contractsViewerView.scrollThroughContract();
	}

	@Then("^The contract scrolls up and down$")
	public void the_contract_scrolls_up_and_down() throws Throwable {
		contractsViewerView.confirmScrollSuccess();
	}

	@When("^User clicks the page number input field$")
	public void user_clicks_in_the_page_number_field_next_to_the_left_facing_arrow_of_X() throws Throwable {
		contractsViewerView.clickPageNumberInputField();
	}

	@When("^User enters a page number not more than the number of total pages, and then presses enter or clicks anywhere outside of the field$")
	public void user_enters_a_page_number_not_more_than_the_number_of_total_pages_and_then_presses_enter_or_clicks_anywhere_outside_of_the_field()
			throws Throwable {
		contractsViewerView.enterPageNumbers();
	}

	@Then("^The chosen page of the contract is visible$")
	public void the_chosen_page_of_the_contract_is_visible() throws Throwable {
		System.out.println("If you got to this point without crashing, testing page navigation by inputting page numbers was successful.");
	}

	@When("^User clicks the Download button on the Contract Viewer$")
	public void user_clicks_the_Download_button_on_the_Contract_Viewer() throws Throwable {
		contractsViewerView.clickDownloadButton();
	}

	@Then("^The contract downloads from the Contract Viewer$")
	public void the_contract_downloads_from_the_Contract_Viewer() throws Throwable {
		multiObjectFunctions.isFileDownloaded(contractDataSearchPanel.text_FileName.getText());
	}

	@When("^User clicks on the Select\\.\\.\\. dropdown$")
	public void user_clicks_on_the_Select_dropdown() throws Throwable {
		contractDataSearchPanel.clickSelectDropdown();
	}

	@Then("^The list of field categories displays$")
	public void the_list_of_field_categories_displays() throws Throwable {
		contractDataSearchPanel.hoverAllSelectDropdownOptions();
	}

	@When("^User selects a category$")
	public void user_selects_a_category() throws Throwable {
		contractDataSearchPanel.clickSelectDropdownOption();
	}

	@Then("^The category filter is applied and only fields for that particular category display$")
	public void the_category_filter_is_applied_and_only_fields_for_that_particular_category_display() throws Throwable {
		contractDataSearchPanel.confirmFieldCategories();
	}

	@When("^User selects another category$")
	public void user_selects_another_category() throws Throwable {
		contractDataSearchPanel.clickSelectDropdownOption();
	}

	@Then("^The category filter is applied and only fields for that particular category, and the previously selected category, display$")
	public void the_category_filter_is_applied_and_only_fields_for_that_particular_category_and_the_previously_selected_category_display()
			throws Throwable {
		contractDataSearchPanel.confirmFieldCategories();
	}

	@When("^User clicks on the X beside one of the two category names in the dropdown field$")
	public void user_clicks_on_the_X_beside_one_of_the_two_category_names_in_the_dropdown_field() throws Throwable {
		contractDataSearchPanel.clickCategoryXButton();
	}

	@Then("^The category filter is removed for that category$")
	public void the_category_filter_is_removed_for_that_category() throws Throwable {
		contractDataSearchPanel.confirmFieldCategories();
		contractDataSearchPanel.clickSelectDropdown();
	}

	@When("^User clicks a category header once$")
	public void user_clicks_a_category_header_once() throws Throwable {
		contractDataSearchPanel.clickHeader();
	}

	@Then("^User can hide and display fields in that category$")
	public void user_can_hide_and_display_fields_in_that_category() throws Throwable {
		contractDataSearchPanel.confirmCategoryHeaderCollapsed();
	}

	@When("^User clicks on the Search Tab$")
	public void user_clicks_on_the_Search_Tab() throws Throwable {
		contractDataSearchPanel.clickSearchTab();
	}

	@Then("^The Search tab is visible$")
	public void the_Search_tab_is_visible() throws Throwable {
		contractDataSearchPanel.confirmSearchTabIsVisible();
	}

	@When("^User types \"([^\"]*)\" into the search field$")
	public void user_types_into_the_search_field(String searchTerm) throws Throwable {
		contractDataSearchPanel.enterSearchTermInSearchField(searchTerm);
	}

	@Then("^The search results are visible below the search field; the current result should be highlighted in green while the rest of the results should be highlighted in yellow$")
	public void the_search_results_are_visible_below_the_search_field_the_current_result_should_be_highlighted_in_green_while_the_rest_of_the_results_should_be_highlighted_in_yellow()
			throws Throwable {
		multiObjectFunctions.confirmSearchResults();
	}

	@When("^User clicks on the down arrow beside /# Resuts found in document$")
	public void user_clicks_on_the_down_arrow_beside_Resuts_found_in_document() throws Throwable {
		contractDataSearchPanel.clickDownArrow();
	}

	@Then("^The user is taken to the next instance of the search results in the document; the current result should be highlighted in green while the rest of the results should be highlighted in yellow$")
	public void the_user_is_taken_to_the_next_instance_of_the_search_results_in_the_document_the_current_result_should_be_highlighted_in_green_while_the_rest_of_the_results_should_be_highlighted_in_yellow()
			throws Throwable {
		System.out.println("SUCCESS: If you've gotten to this point without any problems, this test is a success.");
	}
	
	@When("^User enters \"([^\"]*)\"  as the Counterparty Name$")
	public void user_enters_as_the_Counterparty_Name(String name) throws Throwable {
		searchPanel.clickResetButton();
	    searchPanel.enterCounterpartyName(name);
	}

	@Then("^The contract with related contracts loads$")
	public void the_contract_with_related_contracts_loads() throws Throwable {
		contractDataSearchPanel.confirmRelatedContractsExist();
	}

	@When("^User clicks on a related contract$")
	public void user_clicks_on_a_related_contract() throws Throwable {
		contractDataSearchPanel.clickRelatedContract();
	}

	@Then("^The related contract and its information loads$")
	public void the_related_contract_and_its_information_loads() throws Throwable {
		contractDataSearchPanel.confirmRelatedContractHasOpened();
	}

}
