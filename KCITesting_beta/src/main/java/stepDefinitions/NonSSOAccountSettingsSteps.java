package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class NonSSOAccountSettingsSteps extends DriverFactory {

	@Then("^The Welcome Menu should appear$")
	public void the_Welcome_Menu_should_appear() throws Throwable {
		pageHeader.confirmWelcomeMenuAppears();
	}
	
	@When("^User clicks the My Account button$")
	public void user_clicks_the_My_Account_button() throws Throwable {
		pageHeader.clickMyAccountButton();
	}
	
	@Then("^User should see the My Account pop-up$")
	public void user_should_see_the_My_Account_pop_up() throws Throwable {
		myAccountPopup.confirmAccountWindowIsOpen();
	}

	@When("^User clicks the Password tab if not selected by default$")
	public void user_clicks_the_Password_tab_if_not_selected_by_default() throws Throwable {
		myAccountPopup.clickPasswordTab();
	}

	@Then("^User should be able to see password fields$")
	public void user_should_be_able_to_see_password_fields() throws Throwable {
		myAccountPopup.confirmPasswordFieldsAreVisible();
	}

	@When("^User enters default password in the Old Password field$")
	public void user_enters_default_password_in_the_Old_Password_field() throws Throwable {
		myAccountPopup.enterDefaultPasswordInOldPasswordField();
	}

	@When("^User enters a new test password into the New Password field \"([^\"]*)\"$")
	public void user_enters_a_new_test_password_into_the_New_Password_field(String password) throws Throwable {
		myAccountPopup.enterTestPasswordInNewPasswordField(password);
	}

	@When("^User enters the correct password into the Confirm Password field \"([^\"]*)\"$")
	public void user_enters_the_correct_password_into_the_Confirm_Password_field(String password) throws Throwable {
		myAccountPopup.enterTestPasswordInConfirmPasswordField(password);
	}

	@When("^User clicks the password tab Update button$")
	public void user_clicks_the_password_tab_Update_button() throws Throwable {
		myAccountPopup.clickUpdatePasswordButton();
	}

	@Then("^User should have successfully changed password and see success message$")
	public void user_should_have_successfully_changed_password_and_see_success_message() throws Throwable {
		myAccountPopup.confirmPasswordChangeSuccess();
	}

	@When("^User clicks the password tab Ok button$")
	public void user_clicks_the_password_tab_Ok_button() throws Throwable {
		myAccountPopup.clickOkButton();
	}

	@When("^User clicks the password tab X button in the top right corner of the My Account pop-up$")
	public void user_clicks_the_password_tab_X_button_in_the_top_right_corner_of_the_My_Account_pop_up() throws Throwable {
		myAccountPopup.clickXButton();
	}
	
	@When("^User enters the new test password \"([^\"]*)\"$")
	public void user_enters_the_new_password(String password) throws Exception {
		loginKnowablePage.enterNewPassword(password);
	}

	@When("^User clicks the Profile Tab$")
	public void user_clicks_the_Profile_Tab() throws Throwable {
		myAccountPopup.clickProfileTab();
	}

	@Then("^User personal information should display$")
	public void user_personal_information_should_display() throws Throwable {
		myAccountPopup.confirmProfileFieldsAreVisible();
	}

	@When("^User makes edits to First Name and Last Name fields \"([^\"]*)\" \"([^\"]*)\"$")
	public void user_makes_edits_to_First_Name_and_Last_Name_fields(String first, String last) throws Throwable {
		myAccountPopup.enterNewFirstAndLastName(first, last);
	}

	@When("^User clicks the account tab Update button$")
	public void user_clicks_the_account_tab_Update_button() throws Throwable {
		myAccountPopup.clickUpdateProfileButton();
	}

	@Then("^User should have successfully updated their personal information and see success message$")
	public void user_should_have_successfully_updated_their_personal_information_and_see_success_message() throws Throwable {
		myAccountPopup.confirmProfileUpdateSuccess();
	}

	@When("^User clicks the account tab Ok button$")
	public void user_clicks_the_account_tab_Ok_button() throws Throwable {
		myAccountPopup.clickOkButton();
	}

	@When("^User clicks the account tab X button in the top right corner of the My Account pop-up$")
	public void user_clicks_the_account_tab_X_button_in_the_top_right_corner_of_the_My_Account_pop_up() throws Throwable {
		myAccountPopup.clickXButton();
	}

	@When("^User views the Welcome Message next to the Profile button$")
	public void user_views_the_Welcome_Message_next_to_the_Profile_button() throws Throwable {
		pageHeader.viewWelcomeMessage();
	}

	@Then("^User should see updated first name in Welcome Message$")
	public void user_should_see_updated_first_name_in_Welcome_Message() throws Throwable {
		pageHeader.confirmWelcomeName();
	}

	@When("^User enters incorrect password in the Old Password field$")
	public void user_enters_incorrect_password_in_the_Old_Password_field() throws Throwable {
		myAccountPopup.enterDefaultPasswordInOldPasswordField();
	}

	@When("^User enters the default password into the New Password field$")
	public void user_enters_the_default_password_in_the_New_Password_field() throws Throwable {
		myAccountPopup.enterDefaultPasswordInNewPasswordField();
	}
	
	@When("^User enters the correct password into the Confirm Password field$")
	public void user_enters_the_correct_password_into_the_Confirm_Password_field() throws Throwable {
		myAccountPopup.enterDefaultPasswordInConfirmPasswordField();
	}
	
	@When("^User clicks the password tab Update button again$")
	public void user_clicks_the_password_tab_Update_button_again() throws Throwable {
		myAccountPopup.clickUpdatePasswordButton();
	}

	@Then("^An error message should display and password should not be changed$")
	public void an_error_message_should_display_and_password_should_not_be_changed() throws Throwable {
		myAccountPopup.confirmPasswordChangeFailure();
	}
	
	@When("^User enters the test password in the Old Password field \"([^\"]*)\"$")
	public void user_enters_the_test_password_in_the_Old_Password_field(String password) throws Throwable {
		myAccountPopup.enterTestPasswordInOldPasswordField(password);
	}

	@When("^User clicks the password tab Ok button again$")
	public void user_clicks_the_password_tab_Ok_button_again() throws Throwable {
		myAccountPopup.clickOkButton();
	}

	@When("^User enters the test password into the Confirm Password field \"([^\"]*)\"$")
	public void user_enters_the_test_password_into_the_Confirm_Password_field(String password) throws Throwable {
		myAccountPopup.enterTestPasswordInConfirmPasswordField(password);
	}
	
	@When("^User enters the default password into the Confirm Password field$")
	public void user_enters_the_default_password_into_the_Confirm_Password_field() throws Throwable {
		myAccountPopup.enterDefaultPasswordInConfirmPasswordField();
	}
	
	@When("^User should not be able to click Update button$")
	public void user_should_not_be_able_to_click_Update_button() throws Throwable {
		myAccountPopup.confirmPasswordUpdateDisabled();
	}
	
	@When("^User resets their profile name$")
	public void user_resets_their_profile_name() throws Throwable {
		myAccountPopup.enterDefaultName();
	}
}
