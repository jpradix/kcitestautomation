package stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class DashboardSteps extends DriverFactory {

	@When("^User locates the Total Number of Contracts in the Database$")
	public void user_locates_the_Total_Number_of_Contracts_in_the_Database() throws Throwable {
		dashboardPage.waitForTotalContracts();
	}

	@When("^User locates the Total Number of Out of Scope Contracts$")
	public void user_locates_the_Total_Number_of_Out_of_Scope_Contracts() throws Throwable {
		dashboardPage.waitForTotalOutOfScope();
	}

	@When("^User locates the Top Counterparties chart$")
	public void user_locates_the_Top_Counterparties_chart() throws Throwable {
		dashboardPage.waitForTopCounterpartiesList();
	}

	@When("^User locates the Document Type chart$")
	public void user_locates_the_Document_Type_chart() throws Throwable {
		dashboardPage.waitForDocumentTypeList();
	}

	@When("^User locates the Document Status chart$")
	public void user_locates_the_Document_Status_chart() throws Throwable {
		dashboardPage.waitForDocumentStatusList();
	}

	@Then("^User should have located all page elements$")
	public void user_should_have_located_all_page_elements() throws Throwable {
		dashboardPage.confirmDashboardCriticalElementsExist();
	}
	
	@Then("^User should see all Welcome menu elements$")
	public void user_should_see_all_Welcome_menu_elements() throws Throwable {
		pageHeader.hoverAllWelcomeMenuElements();
	}

	@When("^User locates the Counterparty Name field$")
	public void user_locates_the_Counterparty_Name_field() throws Throwable {
		searchPanel.confirmCounterpartyNameFieldExists();
	}

	@When("^User locates the Client Entity field$")
	public void user_locates_the_Client_Entity_field() throws Throwable {
		searchPanel.confirmClientEntityFieldExists();
	}

	@When("^User observes the dropdown options$")
	public void user_observes_the_dropdown_options() throws Throwable {
		documentTypeOptions.hoverAllDocumentTypeOptions(); 
		searchPanel.clickDocumentTypeDropdown();
	}
	
	@When("^User locates the Add Filter option$")
	public void user_locates_the_Add_Filter_option() throws Throwable {
		searchPanel.confirmAddFilterButtonExists();
	}

	@Then("^The Contract page should display with appropriate results$")
	public void the_Contract_page_should_display_with_appropriate_results() throws Throwable {
		contractsListView.confirmUserIsOnTheContractsListViewPage();
	}	

	@Then("^The search criteria should be removed$")
	public void the_search_criteria_should_be_removed() throws Throwable {
		searchPanel.confirmSearchFieldIsEmpty();
	}

	@When("^User clicks one of the contracts listed on the Contracts page$")
	public void user_clicks_one_of_the_contracts_listed_on_the_Contracts_page() throws Throwable {
		contractsListView.clickContractAtTopOfList();
	}

	@Then("^User takes note of the contract name$")
	public void user_takes_note_of_the_contract_name() throws Throwable {
		contractDataSearchPanel.confirmFileNameExists();
	}

	@When("^User Locates the Recently Viewed contracts in the Search panel$")
	public void user_Locates_the_Recently_Viewed_contracts_in_the_Search_panel() throws Throwable {
		searchPanel.confirmRecentlyViewedContractExists();
	}

	@Then("^User should be able to verify that the contract that was just opened is listed at the top of the Recently viewed contracts list$")
	public void user_should_be_able_to_verify_that_the_contract_that_was_just_opened_is_listed_at_the_top_of_the_Recently_viewed_contracts_list() throws Throwable {
		searchPanel.verifyRecentContractNameFileNameIsCorrect(contractDataSearchPanel.fileName);
	}

	@When("^User clicks on a recently viewed contract$")
	public void user_clicks_on_a_recently_viewed_contract() throws Throwable {
		searchPanel.clickRecentContractButton();
	}

	@Then("^User should see the Contract Preview page$")
	public void user_should_see_the_Contract_Preview_page() throws Throwable {
		contractsViewerView.confirmUserIsOnTheContractsViewerPage();
	}

	@When("^User clicks on one of the counterparties listed under Top Counterparties and takes note of how many contracts are listed for that selection$")
	public void user_clicks_on_one_of_the_counterparties_listed_under_Top_Counterparties_and_takes_note_of_how_many_contracts_are_listed_for_that_selection() throws Throwable {
		dashboardPage.clickTopCounterpartyButton();
	}

	@Then("^The Contracts tab displays with appropriate results and the Search panel contains the selected Counterparty Name$")
	public void the_Contracts_tab_displays_with_appropriate_results_and_the_Search_panel_contains_the_selected_Counterparty_Name() throws Throwable {
		contractsListView.verifyNumberOfContractsAndSearchFieldValue(dashboardPage.topCounterpartyAmt);
	}

	@When("^User clicks on one of the document types listed under Document Type and takes note of how many contracts are listed for that selection$")
	public void user_clicks_on_one_of_the_document_types_listed_under_Document_Type_and_takes_note_of_how_many_contracts_are_listed_for_that_selection() throws Throwable {
		dashboardPage.clickDocumentTypeButton();
	}

	@Then("^The Contracts tab displays with appropriate results and the Search panel contains the selected Document Type$")
	public void the_Contracts_tab_displays_with_appropriate_results_and_the_Search_panel_contains_the_selected_Document_Type() throws Throwable {
		contractsListView.verifyNumberOfContractsAndSearchFieldValue(dashboardPage.topDocumentTypeAmt);
	}
}
