package stepDefinitions;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.DriverFactory;

public class NonSSOLoginSteps extends DriverFactory {

	@Given("^User opens their email inbox$")
	public void user_opens_their_email_inbox() throws Throwable {
		((JavascriptExecutor) driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		
		loginEmailPage.getLoginEmail_Page();
		loginEmailPage.enterEmail();
		loginEmailPage.clickContinueButton();
		loginEmailPage.enterPassword();
		loginEmailPage.clickSignInButton();
		
		driver.switchTo().window(tabs.get(1));
	}
	
	@When ("^User opens the Knowable website$")
	public void user_opens_the_Knowable_website() throws Throwable {
		loginKnowablePage.getLoginKnowable_Page();
	}
	
	@When("^User enters a valid email$")
	public void user_enters_a_valid_email() throws Throwable {
		loginKnowablePage.enterEmail();
	}
	
	@And("^User clicks Continue button$")
	public void user_clicks_Continue_button() throws Throwable {
		loginKnowablePage.clickContinueButton();
	}
	
	@Then("^User should be taken to password page$")
	public void user_should_be_taken_to_password_page() throws Throwable {
		loginKnowablePage.confirmPasswordPageIsDisplayed();
	}
	
	@When("^User enters their default password$")
	public void user_enters_a_valid_password() throws Throwable {
		loginKnowablePage.enterPassword();
	}
	
	@And("^User clicks Sign In button$")
	public void user_clicks_Sign_In_button() throws Throwable {
		loginKnowablePage.clickSignInButton();
	}
	
	@Then("^User should be taken to the log in page$")
	public void user_should_be_taken_to_the_log_in_page() throws Throwable {
		loginKnowablePage.confirmLoginEmailPageIsDisplayed();
	}
	
	@When("^User clicks Reset Password button$")
	public void user_clicks_Reset_Password_button() throws Throwable {
		loginKnowablePage.clickResetPasswordButton();
	}
	
	@Then("^User should be told to check their email \"([^\"]*)\"$")
	public void user_should_be_told_to_check_their_email(String message) throws Throwable {
		loginKnowablePage.confirmResetPasswordSubmissionWasSuccessful(message);
	}
	
	@When("^User navigates to their email inbox$")
	public void user_navigates_to_their_email_inbox() throws Throwable {
		driver.close(); 
		driver.switchTo().window(tabs.get(0));
	}
	
	@And("^User clicks password reset link$")
	public void user_clicks_password_reset_link() throws Throwable {
		Thread.sleep(10000);
		emailInboxPage.clickEmailOpenButton();
		emailInboxPage.clickResetPasswordLink();	
		driver.switchTo().window(tabs.get(0));
		emailInboxPage.clickFocusEmailButton();
		emailInboxPage.clickDeleteEmailButton();
	}
	
	@Then("^User is taken to the password reset page$")
	public void user_is_taken_to_the_password_reset_page() throws Throwable {
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}
	
	@When("^User creates a new password that is less than ten characters \"([^\"]*)\"$")
	public void user_creates_a_new_password_that_is_less_than_ten_characters(String password) throws Throwable {
		passwordResetPage.enterNewPassword(password);
	}
	
	@Then("^A new password length error message should display \"([^\"]*)\"$")
	public void a_new_password_error_message_should_display(String error) throws Throwable {
		passwordResetPage.confirmNewPasswordLengthErrorMessageIsDisplayed(error);
	}
	
	@And("^User creates a new password that is at least ten characters \"([^\"]*)\"$")
	public void user_creates_a_new_password_that_is_at_least_ten_characters(String password) throws Throwable {
		passwordResetPage.enterNewPassword(password);
	}
	
	@When("^User correctly confirms the new password \"([^\"]*)\"$")
	public void user_correctly_confirms_the_new_password(String password) throws Throwable {
		passwordResetPage.enterConfirmPassword(password);
	}
	
	@And("^User clicks Save button$")
	public void user_clicks_Save_button() throws Throwable {
		passwordResetPage.clickSaveButton();
	}
	
	@Then("^A login error message should display \"([^\"]*)\"$")
	public void a_login_error_message_should_display(String error) throws Throwable {
		loginKnowablePage.confirmLoginFailed(error);
	}

	@When("^User creates passwords that do not meet criteria$")
	public void user_creates_passwords_that_do_not_meet_criteria(DataTable dataTable) throws Throwable {
		System.out.println("STEP: Enter new passwords that do not meet criteria");
		passwordResetPage.enterBadPasswords(dataTable, 0, 0);
		passwordResetPage.enterBadPasswords(dataTable, 0, 1);
		passwordResetPage.enterBadPasswords(dataTable, 0, 2);
		passwordResetPage.enterBadPasswords(dataTable, 0, 3);
	}
	
	@Then("^A new password character error message should display \"([^\"]*)\"$")
	public void a_new_password_character_error_message_should_display(String error) throws Throwable {
		passwordResetPage.confirmNewPasswordCharacterErrorMessageIsDisplayed(error);
	}

	@When("^User creates a new password that meets all criteria \"([^\"]*)\"$")
	public void user_creates_a_new_password_that_meets_all_criteria(String password) throws Throwable {
		passwordResetPage.enterNewPassword(password);
	}
	
	@Then("^A confirm password error message should display \"([^\"]*)\"$")
	public void a_confirm_password_error_message_should_display(String error) throws Throwable {
		passwordResetPage.confirmConfirmPasswordErrorMessageIsDisplayed(error);
	}

	@When("^User enters the new password \"([^\"]*)\"$")
	public void user_enters_the_new_password(String password) throws Throwable {
		loginKnowablePage.enterNewPassword(password);
	}

	@When("^User enters an incorrect email \"([^\"]*)\"$")
	public void user_enters_an_incorrect_email(String email) throws Throwable {
		loginKnowablePage.enterWrongEmail(email);
	}

	@When("^User clicks the Use Another Account link$")
	public void user_clicks_the_Use_Another_Account_link() throws Throwable {
		loginKnowablePage.clickUseAnotherAccountButton();
	}

	@When("^User enters an incorrect password \"([^\"]*)\"$")
	public void user_enters_an_incorrect_password(String password) throws Throwable {
		loginKnowablePage.enterNewPassword(password);
	}

	@When("^User incorrectly confirms the new password \"([^\"]*)\"$")
	public void user_incorrectly_confirms_the_new_password(String password) throws Throwable {
		passwordResetPage.enterConfirmPassword(password);
	}

	@When("^User enters old password \"([^\"]*)\"$")
	public void user_enters_old_password(String password) throws Throwable {
		loginKnowablePage.enterNewPassword(password);
	}
}
