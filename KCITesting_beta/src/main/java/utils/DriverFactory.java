package utils;

import java.util.ArrayList;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import pageObjects.BasePage;
import pageObjects.MultiObjectFunctions;
import pageObjects.pages.PasswordReset_Page;
import pageObjects.pages.Dashboard_Page;
import pageObjects.pages.EmailInbox_Page;
import pageObjects.pages.Help_Page;
import pageObjects.pages.LoginEmail_Page;
import pageObjects.pages.LoginKnowable_Page;
import pageObjects.pages.Page_Header;
import pageObjects.pages.Upload_Page;
import pageObjects.pages.Contracts.ContractDataSearch_Panel;
import pageObjects.pages.Contracts.ContractsList_View;
import pageObjects.pages.Contracts.ContractsViewer_View;
import pageObjects.popups.MyAccount_Popup;
import pageObjects.popups.SwitchOrganization_Popup;
import pageObjects.popups.CustomizeTable_Popup.CustomizableFieldsList_Options;
import pageObjects.popups.CustomizeTable_Popup.CustomizeTable_View;
import pageObjects.popups.CustomizeTable_Popup.availableFields.AvailableFieldsDropdown_Options;
import pageObjects.popups.CustomizeTable_Popup.availableFields.AvailableFieldsList_Options;
import pageObjects.searchPanel.Search_Panel;
import pageObjects.searchPanel.addFilterOptions.Filter_Options;
import pageObjects.searchPanel.addFilterOptions.AddedFilterDropdown_Options;
import pageObjects.searchPanel.addFilterOptions.DocumentType_Options;

@SuppressWarnings("unused")
public class DriverFactory {

	// INITIALIZE
	
	// ** Driver and Wait ** //
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static SoftAssert soft = new SoftAssert();
	public static ArrayList<String> tabs;

	// ** Page Builders ** //
	public static ContractDataSearch_Panel contractDataSearchPanel;
	public static ContractsList_View contractsListView;
	public static ContractsViewer_View contractsViewerView;	
	public static Dashboard_Page dashboardPage;
	public static EmailInbox_Page emailInboxPage;
	public static Help_Page helpPage;
	public static LoginEmail_Page loginEmailPage;
	public static LoginKnowable_Page loginKnowablePage;
	public static Page_Header pageHeader;
	public static PasswordReset_Page passwordResetPage;
	public static Upload_Page uploadPage;

	// ** Popup Builders ** //
	public static AvailableFieldsDropdown_Options availableFieldsDropdownOptions;
	public static AvailableFieldsList_Options availableFieldsListOptions;
	public static CustomizableFieldsList_Options customizableFieldsListOptions;
	public static CustomizeTable_View customizeTableView;
	public static MyAccount_Popup myAccountPopup;
	public static SwitchOrganization_Popup switchOrganizationPopup;

	// --  Search Panel Builders -- //
	public static AddedFilterDropdown_Options addedFilterDropdownOptions;
	public static DocumentType_Options documentTypeOptions;
	public static Filter_Options filterOptions;
	public static Search_Panel searchPanel;
	
	// ** Common Code Pages ** //
	public static BasePage basePage;
	public static MultiObjectFunctions multiObjectFunctions;
	
	public WebDriver getDriver() {

		// Read Config
		try {
			ReadConfigFile file = new ReadConfigFile();
			String browserName = file.getBrowser();

			switch (browserName) {
			case "firefox":
				if (null == driver) {
					System.setProperty("webdriver.firefox.driver", Constant.GECKO_DRIVER_DIRECTORY);
					DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					capabilities.setCapability("marionette", true);
					driver = new FirefoxDriver();
				}
				break;
			case "chrome":
				if (null == driver) {
					System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_DIRECTORY);
					driver = new ChromeDriver();
//					Dimension d = new Dimension(880, 1050);
//					driver.manage().window().setSize(d);
					driver.manage().window().maximize();
				}
				break;
			}
		} catch (Exception e) {
			System.out.println("Unable to load browser: " + e.getMessage());
		} finally {
			wait = new WebDriverWait(driver, 10);
			
			contractDataSearchPanel = PageFactory.initElements(driver, ContractDataSearch_Panel.class);
			contractsListView = PageFactory.initElements(driver, ContractsList_View.class);
			contractsViewerView = PageFactory.initElements(driver, ContractsViewer_View.class);
			dashboardPage = PageFactory.initElements(driver, Dashboard_Page.class);
			emailInboxPage = PageFactory.initElements(driver, EmailInbox_Page.class);
			helpPage = PageFactory.initElements(driver, Help_Page.class);
			loginEmailPage = PageFactory.initElements(driver, LoginEmail_Page.class);
			loginKnowablePage = PageFactory.initElements(driver, LoginKnowable_Page.class);
			pageHeader = PageFactory.initElements(driver, Page_Header.class);
			passwordResetPage = PageFactory.initElements(driver, PasswordReset_Page.class);
			uploadPage = PageFactory.initElements(driver, Upload_Page.class);
			
			availableFieldsDropdownOptions = PageFactory.initElements(driver, AvailableFieldsDropdown_Options.class);
			availableFieldsListOptions = PageFactory.initElements(driver, AvailableFieldsList_Options.class);
			customizableFieldsListOptions = PageFactory.initElements(driver, CustomizableFieldsList_Options.class);
			customizeTableView = PageFactory.initElements(driver, CustomizeTable_View.class);
			myAccountPopup = PageFactory.initElements(driver, MyAccount_Popup.class);
			switchOrganizationPopup = PageFactory.initElements(driver, SwitchOrganization_Popup.class);
			
			addedFilterDropdownOptions = PageFactory.initElements(driver, AddedFilterDropdown_Options.class);
			documentTypeOptions = PageFactory.initElements(driver, DocumentType_Options.class);
			filterOptions = PageFactory.initElements(driver, Filter_Options.class);
			searchPanel = PageFactory.initElements(driver, Search_Panel.class);	
			
			basePage = PageFactory.initElements(driver, BasePage.class);
			multiObjectFunctions = PageFactory.initElements(driver, MultiObjectFunctions.class);
		}
		return driver;
	}
}
