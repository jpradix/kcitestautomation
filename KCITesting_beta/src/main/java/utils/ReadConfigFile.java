package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadConfigFile {

	protected Properties prop = null;
	protected File src;
	protected FileInputStream fis;

	public ReadConfigFile() {
		try {
			src = new File(Constant.CONFIG_PROPERTIES_DIRECTORY);
			fis = new FileInputStream(src);
			prop = new Properties();
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getBrowser() {
		return prop.getProperty("browser");
	}
	
	public String getEmail() {
		return prop.getProperty("testerEmail");
	}
	
	public String getInboxPW() {
		return prop.getProperty("inboxPassword");
	}
	
	public String getKnowablePW() {
		return prop.getProperty("knowablePassword");
	}
	
	public String getFirstName() {
		return prop.getProperty("testerFirstName");
	}
	
	public String getLastName() {
		return prop.getProperty("testerLastName");
	}
}
