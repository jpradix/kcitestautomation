package utils;

public class Constant {
	static ReadConfigFile file = new ReadConfigFile();
	
	/** Config Properties file **/
	public final static String CONFIG_PROPERTIES_DIRECTORY = "./src/main/java/properties/config.properties";
	
	/** Browser driver files **/
	public final static String GECKO_DRIVER_DIRECTORY = System.getProperty("user.dir") + "/src/main/java/utils/drivers/geckodriver";
	public final static String CHROME_DRIVER_DIRECTORY = System.getProperty("user.dir") + "/src/main/java/utils/drivers/chromedriver";
	
	/** URLs **/
	public final static String URL_EMAIL = "https://outlook.office.com/mail/inbox";
	public final static String URL_KCI = "https://stage.kci.theknowable.com/";
	
	/** Tester info and credentials **/
	public final static String TESTER_FIRSTNAME = file.getFirstName();
	public final static String TESTER_LASTNAME = file.getLastName();
	public final static String TESTER_INBOX_PASSWORD = file.getInboxPW();
	public final static String TESTER_KCI_PASSWORD = file.getKnowablePW();
	public final static String TESTER_EMAIL = file.getEmail();;
	
}
