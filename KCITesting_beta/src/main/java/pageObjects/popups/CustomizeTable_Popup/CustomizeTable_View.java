package pageObjects.popups.CustomizeTable_Popup;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class CustomizeTable_View extends BasePage {

	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[1]") WebElement textField_SearchAvailableFieldsWrapper;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[1]/input") WebElement textField_SearchAvailableFields;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[1]") WebElement textField_SearchVisibleFieldsWrapper;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[1]/input") WebElement textField_SearchVisibleFields;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div") WebElement dropdown_SelectACategory;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[2]/div/button[1]") WebElement button_RightArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[2]/div/button[2]") WebElement button_LeftArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/button[1]") WebElement button_TopArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/button[2]") WebElement button_UpArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/button[3]") WebElement button_DownArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[2]/div[2]/button[4]") WebElement button_BottomArrow;
	public @FindBy(xpath = "/html/body/div[3]/div/div[@role='dialog']//button[.='Save Preferences']") WebElement button_SavePreferences;
	public @FindBy(xpath = "/html/body/div[3]/div/div[@role='dialog']//button[.='Set to Default']") WebElement button_SetToDefault;

	public CustomizeTable_View() throws IOException {
		super();
	}
	
	public CustomizeTable_View confirmCustomizeTablePopopIsDisplayed() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_SearchAvailableFieldsWrapper));
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_SearchVisibleFieldsWrapper));
		Assert.assertTrue(waitUntilWebElementIsVisible(dropdown_SelectACategory));
		Assert.assertTrue(waitUntilWebElementIsVisible(button_SavePreferences));
		Assert.assertTrue(waitUntilWebElementIsVisible(button_SetToDefault));
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View enterTermInSearchAvailableFieldsField(String string) throws Exception {
		sendKeysToWebElement(textField_SearchAvailableFields, string);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clearSearchAvailableFieldsField() throws Exception {
		sendKeysToWebElement(textField_SearchAvailableFields, "");
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickSelectACategoryDropdown() throws Exception {
		waitAndClickElement(dropdown_SelectACategory);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheRightArrowButton() throws Exception {
		waitAndClickElement(button_RightArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheLeftArrowButton() throws Exception {
		waitAndClickElement(button_LeftArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheDownArrowButton() throws Exception {
		waitAndClickElement(button_DownArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheUpArrowButton() throws Exception {
		waitAndClickElement(button_UpArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheBottomArrowButton() throws Exception {
		waitAndClickElement(button_BottomArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickTheTopArrowButton() throws Exception {
		waitAndClickElement(button_TopArrow);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickSetToDefaultButton() throws Exception {
		waitAndClickElement(button_SetToDefault);
		return new CustomizeTable_View();
	}
	
	public CustomizeTable_View clickSavePreferencesButton() throws Exception {
		waitAndClickElement(button_SavePreferences);
		return new CustomizeTable_View();
	}
}