package pageObjects.popups.CustomizeTable_Popup;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePage;

public class CustomizableFieldsList_Options extends BasePage {

	public String fieldOrder;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[1]/div/li/button") WebElement listOption_01;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[2]/div/li/button") WebElement listOption_02;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[3]/div/li/button") WebElement listOption_03;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[4]/div/li/button") WebElement listOption_04;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[5]/div/li/button") WebElement listOption_05;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[6]/div/li/button") WebElement listOption_06;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[7]/div/li/button") WebElement listOption_07;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[8]/div/li/button") WebElement listOption_08;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[9]/div/li/button") WebElement listOption_09;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[10]/div/li/button") WebElement listOption_10;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[11]/div/li/button") WebElement listOption_11;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[12]/div/li/button") WebElement listOption_12;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[13]/div/li/button") WebElement listOption_13;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[14]/div/li/button") WebElement listOption_14;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[3]/div/div/div/div/ul/div[15]/div/li/button") WebElement listOption_15;
	
	public CustomizableFieldsList_Options() throws Exception {
		super();
	}
	
	public CustomizableFieldsList_Options clickFirstCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_01);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickSecondCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_02);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickThirdCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_03);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickFourthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_04);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickFifthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_05);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickSixthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_06);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickSeventhCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_07);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickEighthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_08);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickNinthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_09);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickTenthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_10);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickEleventhCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_11);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickTwelvthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_12);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickThirteenthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_13);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickFourteenthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_14);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options clickFifteenthCustomizableFieldOption() throws Exception {
		waitAndClickElement(listOption_15);
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options confirmFirstCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_01, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options confirmSecondCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_02, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options confirmThirdCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_03, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
		
	public CustomizableFieldsList_Options confirmFourthCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_04, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options confirmFifthCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_05, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options confirmLastCustomizableFieldOptionIsSelected() throws Exception {
		confirmWebElementTagHasSpecificValue(listOption_06, "class", "selected");
		return new CustomizableFieldsList_Options();
	}
	
	public String getFieldOrder() throws Exception {
		tempWait = new WebDriverWait(driver, 0);
		ArrayList<String> fieldColumnOrder = new ArrayList<String>();
		WebElement[] theFields = {listOption_01, listOption_02, listOption_03, listOption_04, listOption_05, 
				listOption_06, listOption_07, listOption_08, listOption_09, listOption_10, 
				listOption_11, listOption_12, listOption_13, listOption_14, listOption_15};
		count: for (int i = 0; i <= 14; i++) {
			try {
				this.tempWait.until(ExpectedConditions.visibilityOf(theFields[i]));
				fieldColumnOrder.add(i, theFields[i].getText());
			} catch (Exception e) {
				break count;
			}
		}
		return fieldColumnOrder.toString().toLowerCase().replaceAll("[^a-z]|\\s", "");
	}
	
	public CustomizableFieldsList_Options confirmDefaultCustomizableFieldsList() throws Exception {
		fieldOrder = getFieldOrder();
		Assert.assertEquals(fieldOrder, "counterpartynamefilenameeffectivedatedocumenttypecliententityuploaddate");
		return new CustomizableFieldsList_Options();
	}
	
	public CustomizableFieldsList_Options emptyCustomizedFields() throws Exception {
		contractsListView.clickMoreButtonOnContract();
		contractsListView.clickCustomizeTableButton();
		customizeTableView.clickSetToDefaultButton();
		for (int i = 0; i < 6; i++) {
			waitAndClickElement(listOption_01);
			customizeTableView.clickTheLeftArrowButton();
		}
		return new CustomizableFieldsList_Options();
	}
	
	
}
