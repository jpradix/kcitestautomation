package pageObjects.popups.CustomizeTable_Popup.availableFields;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageObjects.BasePage;

public class AvailableFieldsList_Options extends BasePage {

	// Change first div index to 4 after KCI fixes the stupid bug that changes
	// element positions when using incognito mode on browser
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[1]/div/li/button") WebElement listOption_01;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[2]/div/li/button") WebElement listOption_02;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[3]/div/li/button") WebElement listOption_03;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[4]/div/li/button") WebElement listOption_04;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[5]/div/li/button") WebElement listOption_05;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[6]/div/li/button") WebElement listOption_06;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[7]/div/li/button") WebElement listOption_07;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[8]/div/li/button") WebElement listOption_08;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[9]/div/li/button") WebElement listOption_09;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[10]/div/li/button") WebElement listOption_10;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[14]/div/li/button") WebElement listOption_14;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[15]/div/li/button") WebElement listOption_15;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[20]/div/li/button") WebElement listOption_20;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[24]/div/li/button") WebElement listOption_24;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[175]/div/li/button") WebElement listOption_175;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[176]/div/li/button") WebElement listOption_176;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[177]/div/li/button") WebElement listOption_177;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[178]/div/li/button") WebElement listOption_178;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/div[1]/div/div/div/div/ul/div[179]/div/li/button") WebElement listOption_179;

	public AvailableFieldsList_Options() throws IOException {
		super();
	}

	public AvailableFieldsList_Options confirmDocumentTypeDefineOtherOptionExistsInList() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_01));
		return new AvailableFieldsList_Options();
	}

	public AvailableFieldsList_Options confirmDefaultAvailableFieldsList() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_01));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_02));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_03));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_04));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_05));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_06));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_07));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_08));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_09));
		return new AvailableFieldsList_Options();
	}

	public AvailableFieldsList_Options confirmAvailableFieldsListAfterDropdownSelection() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_01));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_02));
		Assert.assertTrue(waitUntilWebElementIsVisible(listOption_03));
		return new AvailableFieldsList_Options();
	}

	public AvailableFieldsList_Options clickFirstAvailableFieldOption() throws Exception {
		waitAndClickElement(listOption_01);
		return new AvailableFieldsList_Options();
	}

	public AvailableFieldsList_Options confirmFirstAvailableFieldOptionIsSelected() throws Exception {
		// Temporary code
		// ----------------------------------------------------------------------------
		this.wait.until(ExpectedConditions.visibilityOf(listOption_01));
		try {
			Assert.assertTrue(listOption_01.getAttribute("class").contains("selected"));
			System.out.println("SUCCESS: WebElement \"" + listOption_01.getTagName() + ": " + listOption_01.getText()
					+ "\" is selected using locator: " + "<" + listOption_01.toString() + ">");
		} catch (AssertionError e) {
			System.out.println("FAIL:");
			System.out.println("------------------------------------------");
			System.out.println("Element Tag: " + listOption_01.getTagName());
			System.out.println("Element text: \"" + listOption_01.getText() + "\"");
			System.out.println("Locator: " + listOption_01.toString());
			System.out.println("Reason: Element is not selected");
			System.out.println("Error message: " + e);
			System.out.println("------------------------------------------");
		}
		// -------------------------------------------------------------------------------------------
		/*
		 * Failure trigger disabled until confirmation from developer on intended functionality for this element. 
		 * If tested functionality is confirmed intended: 
		 * 		- Delete temporary code 
		 * 		- Delete all comments except for last comment 
		 * 		- Un-comment line below 
		 * If tested functionality is NOT intended, delete all method code and create new validation rules. */

//		confirmWebElementTagHasSpecificValue(listOption_01, "class", "selected");		
		return new AvailableFieldsList_Options();
	}

}

