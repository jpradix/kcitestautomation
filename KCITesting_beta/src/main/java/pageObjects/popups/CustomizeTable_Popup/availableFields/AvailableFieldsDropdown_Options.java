package pageObjects.popups.CustomizeTable_Popup.availableFields;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class AvailableFieldsDropdown_Options extends BasePage {
	
	
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/div") WebElement option_01;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[2]") WebElement option_02;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[3]") WebElement option_03;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[4]") WebElement option_04;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[5]") WebElement option_05;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[6]") WebElement option_06;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[7]") WebElement option_07;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[8]") WebElement option_08;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[9]") WebElement option_09;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[10]") WebElement option_10;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[11]") WebElement option_11;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[12]") WebElement option_12;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[13]") WebElement option_13;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[14]") WebElement option_14;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[15]") WebElement option_15;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[16]") WebElement option_16;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[17]") WebElement option_17;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[18]") WebElement option_18;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[19]") WebElement option_19;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[20]") WebElement option_20;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[21]") WebElement option_21;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[22]") WebElement option_22;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[23]") WebElement option_23;

	
	public AvailableFieldsDropdown_Options() throws IOException {
		super();
	}
	
	public AvailableFieldsDropdown_Options hoverAllAvailableFieldsOptions() throws Exception {
		actionHover(option_01);
		actionHover(option_02);
		actionHover(option_03);
		actionHover(option_04);
		actionHover(option_05);
		actionHover(option_06);
		actionHover(option_07);
		actionHover(option_08);
		actionHover(option_09);
		actionHover(option_10);
		actionHover(option_11);
		actionHover(option_12);
		actionHover(option_13);
		actionHover(option_14);
		actionHover(option_15);
		actionHover(option_16);
		actionHover(option_17);
		actionHover(option_18);
		actionHover(option_19);
		actionHover(option_20);
		actionHover(option_21);
		actionHover(option_22);
		actionHover(option_23);
		return new AvailableFieldsDropdown_Options();
	}
	
	public AvailableFieldsDropdown_Options clickSelectACategoryOption() throws Exception {
		waitAndClickElement(option_01);
		return new AvailableFieldsDropdown_Options();
	}	
	public AvailableFieldsDropdown_Options clickContractIdentificationOption() throws Exception {
		waitAndClickElement(option_02);
		return new AvailableFieldsDropdown_Options();
	}
	
	
}
