package pageObjects.popups;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SwitchOrganization_Popup {

	public @FindBy(xpath = "//body/div[4]/div/div[@role='dialog']/div[2]/ul//button[.='SDM Base Test']") WebElement button_SDMBase;
	public @FindBy(xpath = "//body/div[4]/div/div[@role='dialog']/div[2]/ul//button[.='SDM Full Test']") WebElement button_SDMFull;
	
}
