package pageObjects.popups;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;
import utils.Constant;

public class MyAccount_Popup extends BasePage {

	// -- Account Tabs -- //
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/nav/nav/ul/li[1]/a") WebElement tab_Password1;
	public @FindBy(xpath = "/html/body/div[4]/div/div/div[2]/nav/nav/ul/li[1]/a") WebElement tab_Password2;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/nav/nav/ul/li[2]/a") WebElement tab_Profile1;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/nav/nav/ul/li[2]/a") WebElement tab_Profile2;
	
	// -- Password Tab -- //
	public @FindBy(xpath = "/html//input[@id='oldPassword']") WebElement textField_OldPassword;
	public @FindBy(xpath = "/html//input[@id='password']") WebElement textField_NewPassword;
	public @FindBy(xpath = "/html//input[@id='passwordConfirm']") WebElement textField_ConfirmPassword;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div/form/div[3]/button[1]") WebElement button_PasswordUpdate;
	public @FindBy(xpath = "/html//div[@class='ReactModalPortal']/div/div[@role='dialog']/div[2]//form//div[.='Passwords do not match.']") WebElement message_PasswordsNoMatch;

	
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div[2]/div/button") WebElement button_Ok;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div[2]/div/div") WebElement message_PasswordChangeSuccess;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[1]/button") WebElement button_X;

	// -- Profile Tab -- //
	public @FindBy(xpath = "/html//input[@id='firstName']") WebElement textField_FirstName;
	public @FindBy(xpath = "/html//input[@id='lastName']") WebElement textField_LastName;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div/form/div[2]/div[3]/div") WebElement text_Email;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div/form/div[3]/button[1]") WebElement button_ProfileUpdate1;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/form/div[3]/button[1]") WebElement button_ProfileUpdate2;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[2]/div[2]/div/button") WebElement button_Ok1;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div[2]/div/button") WebElement button_Ok2;
	public @FindBy(xpath = "/html/body/div[2]/div/div/div[1]/button") WebElement button_X1;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[1]/button") WebElement button_X2;
	
	public MyAccount_Popup() throws Exception {
		super();
	}
	
	public MyAccount_Popup confirmAccountWindowIsOpen() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(tab_Password1));
		Assert.assertTrue(waitUntilWebElementIsVisible(tab_Profile1));
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickPasswordTab() throws Exception {
		waitAndClickElement(tab_Password1);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmPasswordFieldsAreVisible() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_OldPassword));
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_NewPassword));
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_ConfirmPassword));
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterTestPasswordInOldPasswordField(String password) throws Exception {
		sendKeysToWebElement(textField_OldPassword, password);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterDefaultPasswordInOldPasswordField() throws Exception {
		sendKeysToWebElement(textField_OldPassword, Constant.TESTER_KCI_PASSWORD);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterTestPasswordInNewPasswordField(String password) throws Exception {
		sendKeysToWebElement(textField_NewPassword, password);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterDefaultPasswordInNewPasswordField() throws Exception {
		sendKeysToWebElement(textField_NewPassword, Constant.TESTER_KCI_PASSWORD);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterTestPasswordInConfirmPasswordField(String password) throws Exception {
		sendKeysToWebElement(textField_ConfirmPassword, password);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterDefaultPasswordInConfirmPasswordField() throws Exception {
		sendKeysToWebElement(textField_ConfirmPassword, Constant.TESTER_KCI_PASSWORD);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickUpdatePasswordButton() throws Exception {
		waitAndClickElement(button_PasswordUpdate);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmPasswordChangeSuccess() throws Exception {
		if (assertTextMatches("successfullyupdatedpassword", message_PasswordChangeSuccess) == true) {
			System.out.println("--- Password change successful.");
		} else {
			System.out.println("--- Password change failed.");
		}
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickOkButton() throws Exception {
		waitAndClickElement(button_Ok);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickXButton() throws Exception {
		waitAndClickElement(button_X);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickProfileTab() throws Exception {
		waitAndClickElement(tab_Profile1);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmProfileFieldsAreVisible() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_FirstName));
		Assert.assertTrue(waitUntilWebElementIsVisible(textField_LastName));
		Assert.assertTrue(waitUntilWebElementIsVisible(text_Email));
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterNewFirstAndLastName(String first, String last) throws Exception {
		sendKeysToWebElement(textField_FirstName, first);
		sendKeysToWebElement(textField_LastName, last);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup enterDefaultName() throws Exception {
		sendKeysToWebElement(textField_FirstName, Constant.TESTER_FIRSTNAME);
		sendKeysToWebElement(textField_LastName, Constant.TESTER_LASTNAME);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup clickUpdateProfileButton() throws Exception {
		waitAndClickElement(button_ProfileUpdate1);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmProfileUpdateSuccess() throws Exception {
		assertTextMatches("userwasupdated", message_PasswordChangeSuccess);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmPasswordChangeFailure() throws Exception {
		assertTextMatches("failedtoresetpassword", message_PasswordChangeSuccess);
		return new MyAccount_Popup();
	}
	
	public MyAccount_Popup confirmPasswordUpdateDisabled() throws Exception {
		assertTextMatches("passwordsdonotmatch", message_PasswordsNoMatch);
		return new MyAccount_Popup();
	}
	
}



