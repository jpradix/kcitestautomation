package pageObjects;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class MultiObjectFunctions extends BasePage {

	public MultiObjectFunctions() throws IOException {
		super();
	}

	public MultiObjectFunctions emptyCustomizedFields() throws Exception {
		Thread.sleep(500);
		contractsListView.clickMoreButtonOnContract();
		contractsListView.clickCustomizeTableButton();
		customizeTableView.clickSetToDefaultButton();
		for (int i = 0; i < 6; i++) {
			customizableFieldsListOptions.clickFirstCustomizableFieldOption();
			customizeTableView.clickTheLeftArrowButton();
		}
		return new MultiObjectFunctions();
	}
	
	public MultiObjectFunctions setSingleCustomizedFields(WebElement element) throws Exception {
		waitAndClickElement(element);
		customizeTableView.clickTheRightArrowButton();
		customizeTableView.clickSavePreferencesButton();
		return new MultiObjectFunctions();
	}
	
	public MultiObjectFunctions setTwoCustomizedFields(WebElement element1, WebElement element2) throws Exception {
		waitAndClickElement(element1);
		customizeTableView.clickTheRightArrowButton();
		waitAndClickElement(element2);
		customizeTableView.clickTheRightArrowButton();
		customizeTableView.clickSavePreferencesButton();
		return new MultiObjectFunctions();
	}
	
	public MultiObjectFunctions setDefaultColumnHeaders() throws Exception {
		contractsListView.clickMoreButtonOnContract();
		contractsListView.clickCustomizeTableButton();
		customizeTableView.clickSetToDefaultButton();
		customizeTableView.clickSavePreferencesButton();
		return new MultiObjectFunctions();
	}
	
	public MultiObjectFunctions confirmDetailViewElementsExist() throws Exception {
		contractsViewerView.confirmUserIsOnTheContractsViewerPage();
		contractDataSearchPanel.confirmDataAndSearchTabsAndRelatedContractsExist();
		return new MultiObjectFunctions();
	}
	
	public MultiObjectFunctions confirmSearchResults() throws Exception {	
		int viewerResults = contractsViewerView.getViewerResultAmount();
		int searchResults = contractDataSearchPanel.getSearchPanelResultAmount();
		System.out.println("Results expected: " + searchResults  + "\nResults discovered " + viewerResults);
		try {
			Assert.assertEquals(viewerResults, searchResults);
			System.out.println("SUCCESS: Search term results correct.");
		} catch (AssertionError e) {
			System.out.println("FAIL: Search results inaccurate. Error Message:\n " + e);
			soft.assertEquals(viewerResults, searchResults);
		}
		
		return new MultiObjectFunctions();
	}
	
	public boolean isFileDownloaded(String fileName) throws Exception {
		System.out.println(fileName);
		File dir = new File(System.getProperty("user.home") + "/Downloads/");
		File[] dirContents = dir.listFiles();
		int attempts = 1;

		System.out.print("Scanning system to verify download.\nAttempt " + attempts + " of 5");

		for (int i = 0; i < dirContents.length; i++) {
			System.out.print(".");
			if (dirContents[i].getName().equals(fileName)) {
				// File has been found, it can now be deleted:
				System.out.println(" SUCCESS: File was downloaded. Deleting file.");
				dirContents[i].delete();
				return true;
			} else {
				if (i == (dirContents.length - 1) && attempts != 5) {
					attempts++;
					System.out.print("\nAttempt " + attempts + " of 5");
					i = 0;
					dirContents = dir.listFiles();
				}
			}
			Thread.sleep(100);
		}
		System.out.println("\nFAIL: File not found.");
		soft.fail("FAIL: File not downloaded");
		return false;
	}

}
