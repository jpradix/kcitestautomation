package pageObjects;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.DriverFactory;

public class BasePage extends DriverFactory {
	protected WebDriverWait wait;
	protected WebDriverWait tempWait;
	protected JavascriptExecutor jsExecutor;
	protected int attempts = 0;

	public BasePage() throws IOException {
		this.wait = new WebDriverWait(driver, 5);
		jsExecutor = ((JavascriptExecutor) driver);
	}

	/**********************************************************************************
	 ** CLICK METHODS
	 **********************************************************************************/
	public void waitAndClickElement(WebElement element) throws InterruptedException {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
				// System.out.println("SUCCESS: Successfully clicked on the WebElement: " + "<"
				// + element.toString() + ">");
				clicked = true;
			} catch (Exception e) {
				clicked = false;
				try {
					Thread.sleep(500);
					this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
					// System.out.println("SUCCESS: Successfully clicked on the WebElement: " + "<"
					// + element.toString() + ">");
					clicked = true;
				} catch (Exception e1) {
					System.out.println("FAIL:");
					System.out.println("------------------------------------------");
					System.out.println("Element: " + element.toString());
					System.out.println("Reason: Element is not clickable.");
					System.out.println("Error message: " + e);
					System.out.println("------------------------------------------");
					Assert.fail("FAIL: Unable to wait and click on the WebElement, using locator: " + "<"
							+ element.toString() + ">");
				}
			}
			attempts++;
		}
	}

	public void waitAndClickElementsUsingByLocator(By by) throws InterruptedException {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(by)).click();
				// System.out.println("SUCCESS: Successfully clicked on the element using by
				// locator: " + "<" + by.toString() + ">");
				clicked = true;
			} catch (Exception e) {
				System.out.println("FAIL:");
				System.out.println("------------------------------------------");
				System.out.println("Locator: " + by.toString());
				System.out.println("Reason: Element is not clickable by locator.");
				System.out.println("Error message: " + e);
				System.out.println("------------------------------------------");
				Assert.fail("FAIL: Unable to wait and click on the element using the By locator, element: " + "<"
						+ by.toString() + ">");
			}
			attempts++;
		}
	}

	public void clickOnTextFromDropdownList(WebElement list, String textToSearchFor) throws Exception {
		Wait<WebDriver> tempWait = new WebDriverWait(driver, 30);
		try {
			tempWait.until(ExpectedConditions.elementToBeClickable(list)).click();
			list.sendKeys(textToSearchFor);
			list.sendKeys(Keys.ENTER);
			// System.out.println("SUCCESS: Successfully sent the following keys: " +
			// textToSearchFor + ", to the following WebElement: " + "<" + list.toString() +
			// ">");
		} catch (Exception e) {
			System.out.println("FAIL:");
			System.out.println("------------------------------------------");
			System.out.println("List: " + list.toString());
			System.out.println("Undiscovered text: " + textToSearchFor);
			System.out.println("Reason: Text is not visible.");
			System.out.println("Error message: " + e);
			System.out.println("------------------------------------------");
			Assert.fail("Unable to select the required text from the dropdown menu, Exception: " + e.getMessage());
		}
	}

	public void clickOnElementUsingCustomTimeout(WebElement locator, WebDriver driver, int timeout) {
		try {
			final WebDriverWait customWait = new WebDriverWait(driver, timeout);
			customWait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locator)));
			locator.click();
			// System.out.println("SUCCESS: Successfully clicked on the WebElement, using
			// locator: " + "<" + locator + ">"+ ", using a custom Timeout of: " + timeout);
		} catch (Exception e) {
			System.out.println("FAIL: Unable to click on the WebElement, using locator: " + "<" + locator + ">"
					+ ", using a custom Timeout of: " + timeout);
			Assert.fail("Unable to click on the WebElement, Exception: " + e.getMessage());
		}
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** ACTION METHODS
	 **********************************************************************************/

	public void actionHover(WebElement element) throws Exception {
		Actions ob = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			ob.moveToElement(element).perform();
			// System.out.println("SUCCESS: Successfully Action Moved to the WebElement,
			// using locator: " + "<" + element.toString() + ">");
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				ob.moveToElement(element).perform();
				// System.out.println("SUCCESS (Stale Exception): Successfully Action Moved to
				// the WebElement, using locator: " + "<" + element.toString() + ">");
			}
		} catch (Exception e) {
			System.out.println(
					"FAIL: Unable to Action Move to the WebElement, using locator: " + "<" + element.toString() + ">");
			Assert.fail("Unable to Action Move to the WebElement, Exception: " + e.getMessage());
		}
	}

	public void actionClickAndDrag(WebElement from, WebElement to) throws Exception {
		Actions act = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(from)).isEnabled();
			act.dragAndDrop(from, to).build().perform();
			// System.out.println("SUCCESS: Successfully Action Clicked and Dragged the
			// WebElement, using locator: " + "<" + from.toString() + ">");
		} catch (StaleElementReferenceException elementUpdate) {
			WebElement elementToClick = from;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				act.dragAndDrop(from, to).build().perform();
				// System.out.println("SUCCESS (Stale Exception): Successfully Action Clicked
				// and Dragged the WebElement, using locator: " + "<" + from.toString() + ">");
			}
		} catch (Exception e) {
			System.out.println("FAIL: Unable to Action Click and Drag on the WebElement, using locator: " + "<"
					+ from.toString() + ">");
			Assert.fail("Unable to Action Click and Drag on the WebElement, Exception: " + e.getMessage());
		}
	}

	public void actionMoveAndClick(WebElement element) throws Exception {
		Actions ob = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			ob.moveToElement(element).perform();
			ob.moveToElement(element).click().build().perform();
			// System.out.println("SUCCESS: Successfully Action Moved and Clicked on the
			// WebElement, using locator: " + "<" + element.toString() + ">");
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				ob.moveToElement(element).perform();
				ob.moveToElement(elementToClick).click().build().perform();
				// System.out.println("SUCCESS (Stale Exception): Successfully Action Moved and
				// Clicked on the WebElement, using locator: " + "<" + element.toString() +
				// ">");
			}
		} catch (Exception e) {
			System.out.println("FAIL: Unable to Action Move and Click on the WebElement, using locator: " + "<"
					+ element.toString() + ">");
			Assert.fail("Unable to Action Move and Click on the WebElement, Exception: " + e.getMessage());
		}
	}

	public void actionMoveAndClickByLocator(By element) throws Exception {
		Actions ob = new Actions(driver);
		try {
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			if (elementPresent == true) {
				WebElement elementToClick = driver.findElement(element);
				ob.moveToElement(elementToClick).click().build().perform();
				// System.out.println("SUCCESS: Action moved and clicked on the following
				// element, using By locator: " + "<" + element.toString() + ">");
			}
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = driver.findElement(element);
			ob.moveToElement(elementToClick).click().build().perform();
			// System.out.println("SUCCESS(Stale Exception): Action moved and clicked on the
			// following element, using By locator: "+ "<" + element.toString() + ">");
		} catch (Exception e) {
			System.out.println("FAIL: Unable to Action Move and Click on the WebElement using by locator: " + "<"
					+ element.toString() + ">");
			Assert.fail(
					"Unable to Action Move and Click on the WebElement using by locator, Exception: " + e.getMessage());
		}
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** SEND KEYS METHODS /
	 **********************************************************************************/
	public void sendKeysToWebElement(WebElement element, String textToSend) throws Exception {
		try {
			this.waitUntilWebElementIsVisible(element);
			element.clear();
			element.sendKeys(textToSend);
			// System.out.println("SUCCESS: Successfully Sent the following keys: '" +
			// textToSend + "' to element: " + "<"+ element.toString() + ">");
		} catch (Exception e) {
			System.out.println("FAIL: Unable to locate WebElement: " + "<" + element.toString()
					+ "> and send the following keys: " + textToSend);
			Assert.fail("Unable to send keys to WebElement, Exception: " + e.getMessage());
		}
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** ASSERTION TESTS
	 **********************************************************************************/
	public boolean assertTextMatches(String textToMatch, WebElement element) throws Exception {
		waitUntilWebElementIsVisible(element);
		String newText = null;
		String newDate = null;
		try {
			newText = element.getText().toLowerCase().replaceAll("[^a-z]|\\s", "");
			newDate = element.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "");
			if (newText.equals("")) {
				Assert.assertEquals(textToMatch, newDate);
			} else {
				Assert.assertEquals(textToMatch, newText);
			}
			System.out.println("SUCCESS: Expected text found in element: <" + element.toString());
			return true;
		} catch (AssertionError e) {
			if (newText.equals("")) {
				soft.assertEquals(newDate, textToMatch);
			} else {
				soft.assertEquals(newText, textToMatch);
			}
			System.out.println("FAIL:");
			System.out.println("------------------------------------------");
			System.out.println("Element: " + element.toString());
			System.out.println("Expected text: \"" + textToMatch + "\"");
			if (newText.equals("")) {
				System.out.println("Discovered text: \"" + newDate + "\"");
			} else {
				System.out.println("Discovered text: \"" + newText + "\"");
			}
			System.out.println("Error message: " + e);
			System.out.println("------------------------------------------");
			return false;
		}
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** JS METHODS & JS SCROLL
	 **********************************************************************************/
	public void scrollToElementByWebElementLocator(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -400)");
			// System.out.println("SUCCESS: Succesfully scrolled to the WebElement, using
			// locator: " + "<" + element.toString() + ">");
		} catch (Exception e) {
			System.out.println(
					"FAIL: Unable to scroll to the WebElement, using locator: " + "<" + element.toString() + ">");
			Assert.fail("Unable to scroll to the WebElement, Exception: " + e.getMessage());
		}
	}

	public void scrollThroughElementBy(WebElement element, String num) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollTo(0, " + num + ")", element);
			// System.out.println("SUCCESS: Succesfully scrolled through the WebElement,
			// using locator: " + "<" + element.toString() + ">");
		} catch (Exception e) {
			System.out.println(
					"FAIL: Unable to scroll through the WebElement, using locator: " + "<" + element.toString() + ">");
			Assert.fail("Unable to scroll through the WebElement, Exception: " + e.getMessage());
		}
	}

	public void jsPageScroll(int numb1, int numb2) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(" + numb1 + "," + numb2 + ")");
			// System.out.println("SUCCESS: Succesfully scrolled to the correct position!
			// using locators: " + numb1 + ", " + numb2);
		} catch (Exception e) {
			System.out.println(
					"FAIL: Unable to scroll to element using locators: " + "<" + numb1 + "> " + " <" + numb2 + ">");
			Assert.fail("Unable to manually scroll to WebElement, Exception: " + e.getMessage());
		}
	}

	public void waitAndclickElementUsingJS(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			js.executeScript("arguments[0].click();", element);
			// System.out.println("SUCCESS: Successfully JS clicked on the following
			// WebElement: " + "<" + element.toString() + ">");
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement staleElement = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(staleElement)).isEnabled();
			if (elementPresent == true) {
				js.executeScript("arguments[0].click();", elementPresent);
				// System.out.println("SUCCESS(Stale Exception): Successfully JS clicked on the
				// following WebElement: " + "<" + element.toString() + ">");
			}
		} catch (NoSuchElementException e) {
			System.out
					.println("FAIL: Unable to JS click on the following WebElement: " + "<" + element.toString() + ">");
			Assert.fail("Unable to JS click on the WebElement, Exception: " + e.getMessage());
		}
	}

	public void jsClick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** WAIT METHODS
	 * 
	 * @throws InterruptedException
	 **********************************************************************************/
	public boolean waitUntilWebElementIsVisible(WebElement element) throws InterruptedException {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element));
			System.out.println("SUCCESS: WebElement is visible using locator: " + "<" + element.toString() + ">");
			attempts = 0;
			return true;
		} catch (Exception e) {
			soft.fail("WebElement is NOT visible, Exception: " + e.getMessage());
			System.out.println("FAIL:");
			System.out.println("------------------------------------------");
			System.out.println("Element: " + element.toString());
			System.out.println("Reason: Element is not visible.");
			System.out.println("Error message: " + e);
			System.out.println("------------------------------------------");
			return false;
		}
	}

	public boolean confirmWebElementTagHasSpecificValue(WebElement element, String selector, String classType)
			throws Exception {
		this.wait.until(ExpectedConditions.visibilityOf(element));
		try {
			Assert.assertTrue(element.getAttribute(selector).contains(classType));
			System.out.println("SUCCESS: WebElement \"" + element.getTagName() + ": " + element.getText()
					+ "\" is selected using locator: " + "<" + element.toString() + ">");
			return true;
		} catch (AssertionError e) {
			soft.assertTrue(element.getAttribute(selector).contains(classType));
			System.out.println("FAIL:");
			System.out.println("------------------------------------------");
			System.out.println("Element Tag: " + element.getTagName());
			System.out.println("Element text: \"" + element.getText() + "\"");
			System.out.println("Locator: " + element.toString());
			System.out.println("Reason: Element is not selected");
			System.out.println("Error message: " + e);
			System.out.println("------------------------------------------");
			return false;
		}
	}

	public boolean waitUntilWebElementIsVisibleUsingByLocator(By element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			// System.out.println("SUCCESS: Element is visible using By locator: " + "<" +
			// element.toString() + ">");
			return true;
		} catch (Exception e) {
			System.out.println("FAIL: WebElement is NOT visible, using By locator: " + "<" + element.toString() + ">");
			Assert.fail("WebElement is NOT visible, Exception: " + e.getMessage());
			return false;
		}
	}

	public boolean isElementClickable(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element));
			// System.out.println("SUCCESS: WebElement is clickable using locator: " + "<" +
			// element.toString() + ">");
			return true;
		} catch (Exception e) {
			System.out.println("FAIL: WebElement is NOT clickable using locator: " + "<" + element.toString() + ">");
			return false;
		}
	}

	public boolean waitUntilPreLoadElementDissapears(By element) {
		return this.wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** PAGE METHODS
	 **********************************************************************************/
	public BasePage loadUrl(String url) throws Exception {
		driver.get(url);
		return new BasePage();
	}

	public String getCurrentURL() {
		try {
			String url = driver.getCurrentUrl();
			System.out.println("SUCCESS: Found(Got) the following URL: " + url);
			return url;
		} catch (Exception e) {
			System.out.println("FAIL: Unable to locate (Get) the current URL, Exception: " + e.getMessage());
			return e.getMessage();
		}
	}

	public String waitForSpecificPage(String urlToWaitFor) {
		try {
			String url = driver.getCurrentUrl();
			this.wait.until(ExpectedConditions.urlMatches(urlToWaitFor));
			System.out.println("SUCCESS: The current URL was: " + url + ", "
					+ "navigated and waited for the following URL: " + urlToWaitFor);
			return urlToWaitFor;
		} catch (Exception e) {
			System.out.println(
					"FAIL: Exception! waiting for the URL: " + urlToWaitFor + ",  Exception: " + e.getMessage());
			return e.getMessage();
		}
	}

	/**********************************************************************************/
	/**********************************************************************************/

	/**********************************************************************************
	 ** ALERT & POPUPS METHODS
	 **********************************************************************************/
	public void closePopups(By locator) throws InterruptedException {
		try {
			List<WebElement> elements = this.wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
			for (WebElement element : elements) {
				if (element.isDisplayed()) {
					element.click();
					this.wait.until(ExpectedConditions.invisibilityOfAllElements(elements));
					// System.out.println("SUCCESS: The popup has been closed Successfully!");
				}
			}
		} catch (Exception e) {
			System.out.println("FAIL: Exception! - could not close the popup!, Exception: " + e.toString());
			throw (e);
		}
	}

	public boolean checkPopupIsVisible() {
		try {
			@SuppressWarnings("unused")
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			// System.out.println("SUCCESS: A popup has been found!");
			return true;
		} catch (Exception e) {
			System.err.println("FAIL: Error came while waiting for the alert popup to appear. " + e.getMessage());
		}
		return false;
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void closeAlertPopupBox() throws AWTException, InterruptedException {
		try {
			Alert alert = this.wait.until(ExpectedConditions.alertIsPresent());
			alert.accept();
		} catch (UnhandledAlertException f) {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			System.out.println("FAIL: Unable to close the popup");
			Assert.fail("Unable to close the popup, Exception: " + e.getMessage());
		}
	}
	/**********************************************************************************/
	/**********************************************************************************/
}
