package pageObjects.searchPanel;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Search_Panel extends BasePage {
	
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[1]/input") WebElement textField_Search;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[4]/div/div/textarea") WebElement textField_CounterpartyName;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[5]/div/div/textarea") WebElement textField_ClientEntity;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[1]") WebElement dropdown_DocumentTypeSelectFilterValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/button") WebElement button_AddFilter;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[1]") WebElement dropdown_SelectAFilter;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/textarea") WebElement textField_AddedFilter;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/fieldset/div[1]/div/div/div/div/div/div[1]/input") WebElement dateField_AddedFilterStart;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/fieldset/div[2]/div/div/div/div/div/div[1]/input") WebElement dateField_AddedFilterEnd;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div") WebElement dropdown_AddedFilterSelectFilterValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[1]/div[1]/div[2]/div[2]") WebElement button_X1;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[1]/div[1]/div[2]/div[2]") WebElement button_X2;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[3]/button[1]") WebElement button_Search;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[3]/button[2]") WebElement button_Reset;
	
	// -- Only On Dashboard -- //
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[4]/div[2]/div/table/tbody/tr[1]") WebElement button_RecentContract;
	public @FindBy(xpath = "/html//div[@id='__next']/div//section//table/tbody/tr[1]//button/div") WebElement button_RecentContactMore;
	public @FindBy(xpath = "/html/body//button[.='Download']") WebElement button_RecentContractDownload;	
	public String fileName;

	public Search_Panel() throws IOException {
		super();
	}
	
	public Search_Panel clickSearchField() throws Exception {
		Thread.sleep(100);
		waitAndClickElement(textField_Search);
		return new Search_Panel();
	} 
	
	public Search_Panel enterSearchTerm(String searchTerm) throws Exception {
		sendKeysToWebElement(textField_Search, searchTerm);
		return new Search_Panel();
	}
	
	public Search_Panel confirmCounterpartyNameFieldExists() throws Exception {
		waitUntilWebElementIsVisible(textField_CounterpartyName);
		Assert.assertTrue(isElementClickable(textField_CounterpartyName));
		return new Search_Panel();	
	}
	
	public Search_Panel confirmClientEntityFieldExists() throws Exception {
		waitUntilWebElementIsVisible(textField_ClientEntity);
		Assert.assertTrue(isElementClickable(textField_ClientEntity));
		return new Search_Panel();	
	}
	
	public Search_Panel clickDocumentTypeDropdown() throws Exception {
		waitAndClickElement(dropdown_DocumentTypeSelectFilterValue);
		return new Search_Panel();
	}
	
	public Search_Panel confirmAddFilterButtonExists() throws Exception {
		waitUntilWebElementIsVisible(button_AddFilter);
		Assert.assertTrue(isElementClickable(button_AddFilter));
		return new Search_Panel();	
	}

	public Search_Panel clickSearchButton() throws Exception {
		waitAndClickElement(button_Search);
		waitAndClickElement(button_Search);
		Thread.sleep(1000);
		return new Search_Panel();
	}
	
	public Search_Panel clickResetButton() throws Exception {
		waitAndClickElement(button_Reset);
		return new Search_Panel();
	}
	
	public Search_Panel confirmSearchFieldIsEmpty() throws Exception {
		waitUntilWebElementIsVisible(textField_Search);
		Assert.assertEquals("", textField_Search.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new Search_Panel();	
	}
	
	public Search_Panel confirmRecentlyViewedContractExists() throws Exception {
		waitUntilWebElementIsVisible(button_RecentContract);
		Assert.assertTrue(isElementClickable(button_RecentContract));
		return new Search_Panel();	
	}
	
	public Search_Panel verifyRecentContractNameFileNameIsCorrect(String fileName) throws Exception {
		waitUntilWebElementIsVisible(button_RecentContract);
		Assert.assertEquals(fileName.toLowerCase().replaceAll("[^a-z]|\\s", ""), button_RecentContract.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new Search_Panel();	
	}
	
	public Search_Panel clickRecentContractButton() throws Exception {
		waitAndClickElement(button_RecentContract);
		return new Search_Panel();
	}
	
	public Search_Panel enterCounterpartyName(String name) throws Exception {
		waitUntilWebElementIsVisible(textField_CounterpartyName);
		sendKeysToWebElement(textField_CounterpartyName, name);
		return new Search_Panel();
	}
	
	public Search_Panel enterClientEntity(String name) throws Exception {
		waitUntilWebElementIsVisible(textField_ClientEntity);
		sendKeysToWebElement(textField_ClientEntity, name);
		return new Search_Panel();
	}
	
	public Search_Panel clickDocumentTypeXButton() throws Exception {
		waitAndClickElement(button_X1);
		return new Search_Panel();
	}
	
	public Search_Panel clickAddFilterButton() throws Exception {
		waitAndClickElement(button_AddFilter);
		return new Search_Panel();
	}
	
	public Search_Panel clickSelectAFilterButton() throws Exception {
		waitAndClickElement(dropdown_SelectAFilter);
		return new Search_Panel();
	}
	
	public Search_Panel clickAddedFilterDropwdown() throws Exception {
		waitAndClickElement(dropdown_AddedFilterSelectFilterValue);
		return new Search_Panel();
	}
	
	public Search_Panel clickAddedFilterXButton() throws Exception {
		waitAndClickElement(button_X2);
		return new Search_Panel();
	}
	
	public Search_Panel enterAddedFilterText(String text) throws Exception {
		waitUntilWebElementIsVisible(textField_AddedFilter);
		sendKeysToWebElement(textField_AddedFilter, text);
		return new Search_Panel();
	}
	
	public Search_Panel enterAddedFilterDates(String start, String end) throws Exception {
		waitUntilWebElementIsVisible(dateField_AddedFilterStart);
		sendKeysToWebElement(dateField_AddedFilterStart, start);
		sendKeysToWebElement(dateField_AddedFilterEnd, end);
		waitAndClickElement(textField_Search);
		return new Search_Panel();
	}
	
	public Search_Panel clickRecentContractMoreButton() throws Exception {
		waitAndClickElement(button_RecentContactMore);
		return new Search_Panel();
	}
	
	public Search_Panel confirmRecentlyViewedContractDownloadButtonExists() throws Exception {
		waitUntilWebElementIsVisible(button_RecentContractDownload);
		Assert.assertTrue(isElementClickable(button_RecentContractDownload));
		return new Search_Panel();	
	}
	
	public Search_Panel clickRecentContractDownloadButton() throws Exception {
		fileName = button_RecentContract.getText();
		waitAndClickElement(button_RecentContractDownload);
		return new Search_Panel();
	}
	
	
	
	
}
