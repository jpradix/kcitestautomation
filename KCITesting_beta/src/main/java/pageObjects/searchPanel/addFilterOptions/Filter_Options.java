package pageObjects.searchPanel.addFilterOptions;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Filter_Options extends BasePage {

	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[1]") WebElement option_01_DocumentTitle;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[2]") WebElement option_02_FileName;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[3]") WebElement option_03_EffectiveDate;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[4]") WebElement option_04_DocumentTypeDefineOther;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[5]") WebElement option_05_ContractID;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[11]") WebElement option_11_ParentControlNumber;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[21]") WebElement option_21_ContractExecutionStatus;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[172]") WebElement option_172_OutOfScope;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[173]") WebElement option_173_DocumentIssues;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[174]") WebElement option_174_UploadBy;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[175]") WebElement option_175_UploadDate;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div[2]/div/div[176]") WebElement option_176_ContractStatus;
	
	public Filter_Options() throws IOException {
		super();
	}
	
	public Filter_Options selectContractIDFilter() throws Exception {
		waitAndClickElement(option_05_ContractID);
		return new Filter_Options();
	}
	
	public Filter_Options selectDocumentTitleFilter() throws Exception {
		waitAndClickElement(option_01_DocumentTitle);
		return new Filter_Options();
	}
	
	public Filter_Options selectDocumentTypeDefineOtherFilter() throws Exception {
		waitAndClickElement(option_04_DocumentTypeDefineOther);
		return new Filter_Options();
	}
	
	public Filter_Options selectEffectiveDateFilter() throws Exception {
		waitAndClickElement(option_03_EffectiveDate);
		return new Filter_Options();
	}
	
	public Filter_Options selectFileNameFilter() throws Exception {
		waitAndClickElement(option_02_FileName);
		return new Filter_Options();
	}
	
	public Filter_Options selectContractExecutionStatusFilter() throws Exception {
		waitAndClickElement(option_21_ContractExecutionStatus);
		return new Filter_Options();
	}
	
	public Filter_Options selectContractStatusFilter() throws Exception {
		waitAndClickElement(option_176_ContractStatus);
		return new Filter_Options();
	}
	
	public Filter_Options selectParentControlNumberFilter() throws Exception {
		waitAndClickElement(option_11_ParentControlNumber);
		return new Filter_Options();
	}
	
	public Filter_Options selectDocumentIssuesFilter() throws Exception {
		waitAndClickElement(option_173_DocumentIssues);
		return new Filter_Options();
	}
	
	public Filter_Options selectOutOfScopeFilter() throws Exception {
		waitAndClickElement(option_172_OutOfScope);
		return new Filter_Options();
	}
	
	public Filter_Options selectUploadByFilter() throws Exception {
		waitAndClickElement(option_174_UploadBy);
		return new Filter_Options();
	}
	
	public Filter_Options selectUploadDateFilter() throws Exception {
		waitAndClickElement(option_175_UploadDate);
		return new Filter_Options();
	}
	
}
