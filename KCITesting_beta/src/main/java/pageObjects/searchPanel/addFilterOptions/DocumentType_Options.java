package pageObjects.searchPanel.addFilterOptions;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class DocumentType_Options extends BasePage {

	public DocumentType_Options() throws IOException {
		super();
	}
	
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[1]") WebElement option_01;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[2]") WebElement option_02;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[3]") WebElement option_03;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[4]") WebElement option_04;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[5]") WebElement option_05;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[6]") WebElement option_06;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[7]") WebElement option_07;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[6]/div/div/div/div[2]/div/div[8]") WebElement option_08;
	
	public DocumentType_Options hoverAllDocumentTypeOptions() throws Exception {
		actionHover(option_01);
		Assert.assertEquals("addendum", option_01.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_02);
		Assert.assertEquals("amendment", option_02.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_03);
		Assert.assertEquals("changeorder", option_03.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_04);
		Assert.assertEquals("masterbaseagreement", option_04.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_05);
		Assert.assertEquals("nda", option_05.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_06);
		Assert.assertEquals("orderinvoice", option_06.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_07);
		Assert.assertEquals("other", option_07.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		actionHover(option_08);
		Assert.assertEquals("sow", option_08.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickFirstOption() throws Exception {
		waitAndClickElement(option_01);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickSecondOption() throws Exception {
		waitAndClickElement(option_02);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickThirdOption() throws Exception {
		waitAndClickElement(option_03);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickFourthOption() throws Exception {
		waitAndClickElement(option_04);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickFifthOption() throws Exception {
		waitAndClickElement(option_05);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickSixthOption() throws Exception {
		waitAndClickElement(option_06);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickSeventhOption() throws Exception {
		waitAndClickElement(option_07);
		return new DocumentType_Options();
	}
	
	public DocumentType_Options clickEighthOption() throws Exception {
		waitAndClickElement(option_08);
		return new DocumentType_Options();
	}
}
