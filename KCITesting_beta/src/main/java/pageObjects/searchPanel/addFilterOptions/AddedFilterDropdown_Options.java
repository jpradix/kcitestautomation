package pageObjects.searchPanel.addFilterOptions;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class AddedFilterDropdown_Options extends BasePage {

	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[1]") WebElement option_01;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[2]") WebElement option_02;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[3]") WebElement option_03;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[4]") WebElement option_04;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[5]") WebElement option_05;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[6]") WebElement option_06;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[7]") WebElement option_07;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[8]") WebElement option_08;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[9]") WebElement option_09;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[1]/div[7]/div/div/div/div[2]/div/div[10]") WebElement option_10;
	
	public AddedFilterDropdown_Options() throws IOException {
		super();
	}

	public AddedFilterDropdown_Options hoverAllContractExecutionStatusOptions() throws Exception {
		actionHover(option_01);
		assertTextMatches("executedbyclientonly", option_01);
		actionHover(option_02);
		assertTextMatches("executedbycounterpartyonly", option_02);
		actionHover(option_03);
		assertTextMatches("fullyexecuted", option_03);
		actionHover(option_04);
		assertTextMatches("notexecuted", option_04);
		actionHover(option_05);
		assertTextMatches("notsignablenosignatureexpected", option_05);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options hoverAllOutOfScopeOptions() throws Exception {
		actionHover(option_01);
		assertTextMatches("no", option_01);
		actionHover(option_02);
		assertTextMatches("yes", option_02);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options hoverAllDocumentIssuesOptions() throws Exception {
		actionHover(option_01);
		assertTextMatches("exactduplicate", option_01);
		actionHover(option_02);
		assertTextMatches("foreignlanguage", option_02);
		actionHover(option_03);
		assertTextMatches("illegible", option_03);
		actionHover(option_04);
		assertTextMatches("missingmastermissingparent", option_04);
		actionHover(option_05);
		assertTextMatches("missingpages", option_05);
		actionHover(option_06);
		assertTextMatches("multipledocuments", option_06);
		actionHover(option_07);
		assertTextMatches("nearduplicate", option_07);
		actionHover(option_08);
		assertTextMatches("nocliententities", option_08);
		actionHover(option_09);
		assertTextMatches("onlycliententities", option_09);
		actionHover(option_10);
		assertTextMatches("techissues", option_10);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options hoverAllContractStatusOptions() throws Exception {
		actionHover(option_01);
		assertTextMatches("pendingreview", option_01);
		actionHover(option_02);
		assertTextMatches("reviewcomplete", option_02);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickFirstOption() throws Exception {
		waitAndClickElement(option_01);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickSecondOption() throws Exception {
		waitAndClickElement(option_02);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickThirdOption() throws Exception {
		waitAndClickElement(option_03);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickFourthOption() throws Exception {
		waitAndClickElement(option_04);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickFifthOption() throws Exception {
		waitAndClickElement(option_05);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickSixthOption() throws Exception {
		waitAndClickElement(option_06);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickSeventhOption() throws Exception {
		waitAndClickElement(option_07);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickEighthOption() throws Exception {
		waitAndClickElement(option_08);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickNinthOption() throws Exception {
		waitAndClickElement(option_09);
		return new AddedFilterDropdown_Options();
	}
	
	public AddedFilterDropdown_Options clickTenthOption() throws Exception {
		waitAndClickElement(option_10);
		return new AddedFilterDropdown_Options();
	}
}
