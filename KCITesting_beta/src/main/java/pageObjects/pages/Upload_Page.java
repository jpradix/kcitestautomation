package pageObjects.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Upload_Page extends BasePage {

	public @FindBy(xpath = "//div[@id='work-area']/div/section[1]//span[.='In Progress']") WebElement text_InProgress;

	public Upload_Page() throws Exception {
		super();
	}
	
	public Upload_Page confirmUserIsOnTheUploadPage() throws Exception {
		Assert.assertTrue(isElementClickable(text_InProgress));
		return new Upload_Page();	
	}
}
