package pageObjects.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;
import utils.Constant;

public class LoginKnowable_Page extends BasePage {

	List<List<String>> data;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form//input[@name='email']") WebElement textField_Email;
	public @FindBy(xpath = "/html//div[@id='__next']//form//button[.='Continue']") WebElement button_Continue;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form//input[@name='password']") WebElement textField_Password;
	public @FindBy(xpath = "/html//div[@id='__next']//form//button[.='Sign In']") WebElement button_SignIn;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form//span[.='The username or password is incorrect']") WebElement text_LoginFailed;
	public @FindBy(xpath = "/html//div[@id='__next']//form//button[.='Reset Password']") WebElement button_ResetPassword;
	public @FindBy(xpath = "/html//div[@id='__next']//form//button[.='Use Another Account']") WebElement button_UseAnotherAccount;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form//p[.='Please check your email to reset your password.']") WebElement text_ResetPassword;
	
	public LoginKnowable_Page() throws Exception {
		super();
	}
	
	public LoginKnowable_Page fullLogin() throws Exception {
		getDriver().get(Constant.URL_KCI);
		sendKeysToWebElement(textField_Email, Constant.TESTER_EMAIL);
		waitAndClickElement(button_Continue);
		sendKeysToWebElement(textField_Password, Constant.TESTER_KCI_PASSWORD);
		waitAndClickElement(button_SignIn);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page getLoginKnowable_Page() throws Exception {
		getDriver().get(Constant.URL_KCI);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterEmail() throws Exception {
		Thread.sleep(1000);
		sendKeysToWebElement(textField_Email, Constant.TESTER_EMAIL);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterWrongEmail(String email) throws Exception {
		Thread.sleep(1000);
		sendKeysToWebElement(textField_Email, email);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickContinueButton() throws Exception {
		waitAndClickElement(button_Continue);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page confirmPasswordPageIsDisplayed() throws Exception {
		Assert.assertTrue(isElementClickable(textField_Password));
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterPassword() throws Exception {
		sendKeysToWebElement(textField_Password, Constant.TESTER_KCI_PASSWORD);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterNewPassword(String newPassword) throws Exception {
		sendKeysToWebElement(textField_Password, newPassword);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickSignInButton() throws Exception {
		waitAndClickElement(button_SignIn);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickUseAnotherAccountButton() throws Exception {
		waitAndClickElement(button_UseAnotherAccount);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page confirmResetPasswordSubmissionWasSuccessful(String message) throws Exception {
		waitUntilWebElementIsVisible(text_ResetPassword);
		Assert.assertEquals(message, text_ResetPassword.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page confirmLoginFailed(String message) throws Exception {
		waitUntilWebElementIsVisible(text_LoginFailed);
		Assert.assertEquals(message, text_LoginFailed.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page confirmLoginEmailPageIsDisplayed() throws Exception {
		Assert.assertTrue(isElementClickable(textField_Email));
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickResetPasswordButton() throws Exception {
		waitAndClickElement(button_ResetPassword);
		return new LoginKnowable_Page();
	}
}
