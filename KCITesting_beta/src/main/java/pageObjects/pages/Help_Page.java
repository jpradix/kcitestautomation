package pageObjects.pages;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Help_Page extends BasePage {

	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/h2") WebElement text_FAQ;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/header") WebElement button_Topic1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[2]/header") WebElement button_Topic2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/header") WebElement button_Topic3;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[4]/header") WebElement button_Topic4;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[1]/header") WebElement button_Topic1Question1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[2]/header") WebElement button_Topic1Question2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[3]/header") WebElement button_Topic1Question3;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[2]/div/div/section[1]/header") WebElement button_Topic2Question1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[2]/div/div/section[2]/header") WebElement button_Topic2Question2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[1]/header") WebElement button_Topic3Question1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[2]/header") WebElement button_Topic3Question2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[3]/header") WebElement button_Topic3Question3;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[4]/div/div/section[1]/header") WebElement button_Topic4Question1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[1]/div") WebElement button_Topic1Answer1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[2]/div") WebElement button_Topic1Answer2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[1]/div/div/section[3]/div") WebElement button_Topic1Answer3;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[2]/div/div/section[1]/div") WebElement button_Topic2Answer1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[2]/div/div/section[2]/div") WebElement button_Topic2Answer2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[1]/div") WebElement button_Topic3Answer1;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[2]/div") WebElement button_Topic3Answer2;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[3]/div/div/section[3]/div") WebElement button_Topic3Answer3;
	public @FindBy(xpath = "/html/body/div[1]/div/div/section/div[1]/div/section[4]/div/div/section[1]/div") WebElement button_Topic4Answer1;
	public @FindBy(xpath = "//div[@id='__next']/div/div/section/div[@title='Download User Documentation']//a[@href='/api/help']") WebElement link_Download;
	public @FindBy(xpath = "/html//div[@id='__next']/div/div/div/a[@href='/api/help']/button[.='Download User Documentation']") WebElement button_Download;
	public @FindBy(xpath = "//div[@id='__next']/div/div/section/div[@title='Still need Help?']/div/div/span[.='Contact Product Support']") WebElement icon_Support;
	public @FindBy(xpath = "/html/body/div[3]/div/div") WebElement form_Support;
	public @FindBy(xpath = "//div[@id='requestType']/div[1]/div[1]") WebElement dropdown_Categories;
	public @FindBy(xpath = "//div[@id='requestType']/div[2]/div/div[1]/div") WebElement option_UserProfileAdministration;
	public @FindBy(xpath = "//div[@id='requestType']/div[2]/div/div[2]/div") WebElement option_Dashboard;
	public @FindBy(xpath = "//div[@id='requestType']/div[2]/div/div[3]/div") WebElement option_Contracts;
	public @FindBy(xpath = "//div[@id='requestType']/div[2]/div/div[4]/div") WebElement option_Upload;
	public @FindBy(xpath = "//div[@id='requestType']/div[2]/div/div[5]/div") WebElement option_Other;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/form/ul/li/ul/li[2]/div/input") WebElement textField_Subject;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[1]/form/ul/li/ul/li[3]/textarea") WebElement textArea_Description;
	public @FindBy(xpath = "/html/body/div[3]/div/div/div[2]/div/div[2]/button[1]") WebElement button_Cancel;
	public @FindBy(xpath = "/html/body/div[1]/div/div/div/button") WebElement button_Back;

	public Help_Page() throws IOException {
		super();
	}

	public Help_Page confirmHelpPageIsDisplayed() throws Exception {
		waitUntilWebElementIsVisible(text_FAQ);
		try {
			Assert.assertTrue(isElementClickable(text_FAQ));
			System.out.println("SUCCESS: Help Page is displayed properly, using locator: " + text_FAQ.toString());
		} catch (AssertionError e) {
			soft.assertTrue(isElementClickable(text_FAQ));
			System.out.println("FAIL: Help Page is NOT displayed properly. Error Message:\n " + e);
		}
		return new Help_Page();
	}

	public Help_Page clickAllTopics() throws Exception {
		waitAndClickElement(button_Topic1);
		waitAndClickElement(button_Topic2);
		waitAndClickElement(button_Topic3);
		waitAndClickElement(button_Topic4);
		return new Help_Page();
	}

	public Help_Page confirmQuestionsAreDisplayed() throws Exception {
		try {
			Assert.assertTrue(isElementClickable(button_Topic1Question1));
			System.out.println(
					"SUCCESS: FIRST question of FIRST topic displayed properly, using locator: " + text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic1Question2));
			System.out.println("SUCCESS: SECOND question of FIRST topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic1Question3));
			System.out.println(
					"SUCCESS: THIRD question of FIRST topic displayed properly, using locator: " + text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic2Question1));
			System.out.println("SUCCESS: FIRST question of SECOND topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic2Question2));
			System.out.println("SUCCESS: SECOND question of SECOND topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Question1));
			System.out.println(
					"SUCCESS: FIRST question of THIRD topic displayed properly, using locator: " + text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Question2));
			System.out.println("SUCCESS: SECOND question of THIRD topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Question3));
			System.out.println(
					"SUCCESS: THIRD question of THIRD topic displayed properly, using locator: " + text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic4Question1));
			System.out.println("SUCCESS: FIRST question of FOURTH topic displayed properly, using locator: "
					+ text_FAQ.toString());
		} catch (AssertionError e) {
			soft.assertTrue(isElementClickable(button_Topic1Question1));
			System.out.println("FAIL: FIRST question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic1Question2));
			System.out.println("FAIL: SECOND question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic1Question3));
			System.out.println("FAIL: THIRD question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic2Question1));
			System.out.println("FAIL: FIRST question of SECOND topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic2Question2));
			System.out
					.println("FAIL: SECOND question of SECOND topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Question1));
			System.out.println("FAIL: FIRST question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Question2));
			System.out.println("FAIL: SECOND question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Question3));
			System.out.println("FAIL: THIRD question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic4Question1));
			System.out.println("FAIL: FIRST question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
		}
		return new Help_Page();
	}

	public Help_Page clickAllQuestions() throws Exception {
		waitAndClickElement(button_Topic1Question1);
		waitAndClickElement(button_Topic1Question2);
		waitAndClickElement(button_Topic1Question3);
		waitAndClickElement(button_Topic2Question1);
		waitAndClickElement(button_Topic2Question2);
		waitAndClickElement(button_Topic3Question1);
		waitAndClickElement(button_Topic3Question2);
		waitAndClickElement(button_Topic3Question3);
		waitAndClickElement(button_Topic4Question1);
		return new Help_Page();
	}

	public Help_Page confirmAnswersAreDisplayed() throws Exception {
		waitUntilWebElementIsVisible(button_Topic1Question1);
		try {
			Assert.assertTrue(isElementClickable(button_Topic1Answer1));
			System.out.println("SUCCESS: Answer to FIRST question of FIRST topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic1Answer2));
			System.out.println("SUCCESS: Answer to SECOND question of FIRST topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic1Answer3));
			System.out.println("SUCCESS: Answer to THIRD question of FIRST topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic2Answer1));
			System.out.println("SUCCESS: Answer to FIRST question of SECOND topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic2Answer2));
			System.out.println("SUCCESS: Answer to SECOND question of SECOND topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Answer1));
			System.out.println("SUCCESS: Answer to FIRST question of THIRD topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Answer2));
			System.out.println("SUCCESS: Answer to SECOND question of THIRD topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic3Answer3));
			System.out.println("SUCCESS: Answer to THIRD question of THIRD topic displayed properly, using locator: "
					+ text_FAQ.toString());
			Assert.assertTrue(isElementClickable(button_Topic4Answer1));
			System.out.println("SUCCESS: Answer to FIRST question of FOURTH topic displayed properly, using locator: "
					+ text_FAQ.toString());
		} catch (AssertionError e) {
			soft.assertTrue(isElementClickable(button_Topic1Answer1));
			System.out.println(
					"FAIL: Answer to FIRST question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic1Answer2));
			System.out.println(
					"FAIL: Answer to SECOND question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic1Answer3));
			System.out.println(
					"FAIL: Answer to THIRD question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic2Answer1));
			System.out.println(
					"FAIL: Answer to FIRST question of SECOND topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic2Answer2));
			System.out.println(
					"FAIL: Answer to SECOND question of SECOND topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Answer1));
			System.out.println(
					"FAIL: Answer to FIRST question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Answer2));
			System.out.println(
					"FAIL: Answer to SECOND question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic3Answer3));
			System.out.println(
					"FAIL: Answer to THIRD question of THIRD topic is NOT displayed properly, Error Message:\n " + e);
			soft.assertTrue(isElementClickable(button_Topic4Answer1));
			System.out.println(
					"FAIL: Answer to FIRST question of FIRST topic is NOT displayed properly, Error Message:\n " + e);
		}
		return new Help_Page();
	}

	public Help_Page confirmAnswersAreNotDisplayed() throws Exception {
		confirmWebElementTagHasSpecificValue(button_Topic1Question1, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic1Question2, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic1Question3, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic2Question1, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic2Question2, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic3Question1, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic3Question2, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic3Question3, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic4Question1, "aria-expanded", "false");
		return new Help_Page();
	}

	public Help_Page confirmQuestionsAreNotDisplayed() throws Exception {
		confirmWebElementTagHasSpecificValue(button_Topic1, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic2, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic3, "aria-expanded", "false");
		confirmWebElementTagHasSpecificValue(button_Topic4, "aria-expanded", "false");
		return new Help_Page();
	}
	
	public Help_Page clickDownloadUserDocumentationSectionButton() throws Exception {
		waitAndClickElement(link_Download);
		return new Help_Page();
	}
	
	public Help_Page clickTopRightDownloadButton() throws Exception {
		waitAndClickElement(button_Download);
		return new Help_Page();
	}
	
	public Help_Page clickSupportFormButton() throws Exception {
		waitAndClickElement(icon_Support);
		return new Help_Page();
	}
	
	public Help_Page confirmSupportFormDisplays() throws Exception {
		try {
			Assert.assertTrue(waitUntilWebElementIsVisible(form_Support));
			System.out.println("SUCCESS: Support form is displayed properly, using locator: " + form_Support.toString());
		} catch (AssertionError e) {
			soft.assertTrue(waitUntilWebElementIsVisible(form_Support));
			System.out.println("FAIL: Support form is NOT displayed properly. Error Message:\n " + e);
		}
		
		waitAndClickElement(dropdown_Categories);
		actionHover(option_UserProfileAdministration);
		actionHover(option_Dashboard);
		actionHover(option_Contracts);
		actionHover(option_Upload);
		actionHover(option_Other);
		
		sendKeysToWebElement(textField_Subject, "Easter Egg");
		sendKeysToWebElement(textArea_Description, "             . - ."
				+ "\n          .'        '."
				+ "\n         /            \\"
				+ "\n        :              :"
				+ "\n        :              :"
				+ "\n         '.           .' "
				+ "\n            ' - - - '");	
		Thread.sleep(1000);
		return new Help_Page();
	}
	
	public Help_Page clickCancelButton() throws Exception {
		waitAndClickElement(button_Cancel);
		return new Help_Page();
	}
	
	public Help_Page clickBackButton() throws Exception {
		waitAndClickElement(button_Back);
		return new Help_Page();
	}
	
}
