package pageObjects.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import cucumber.api.DataTable;
import pageObjects.BasePage;

public class PasswordReset_Page extends BasePage {

	List<List<String>> data;
	public @FindBy(xpath = "/html//input[@id='password']") WebElement textField_NewPassword;
	public @FindBy(xpath = "/html//input[@id='passwordConfirm']") WebElement textField_ConfirmPassword;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form/div/div[2]/div[1]/div") WebElement text_NewPasswordLengthError;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form/div/div[2]/div[1]/div") WebElement text_NewPasswordCharacterError;
	public @FindBy(xpath = "/html//div[@id='__next']/div//form/div/div[2]/div[2]/div") WebElement text_ConfirmPasswordError;
	public @FindBy(xpath = "/html//div[@id='__next']//form//button[@type='submit']") WebElement button_ResetPassword;
	

	public PasswordReset_Page() throws Exception {
		super();
	}
	
	public PasswordReset_Page enterNewPassword(String password) throws Exception {
		sendKeysToWebElement(textField_NewPassword, password);
		return new PasswordReset_Page(); 
	}
	
	public PasswordReset_Page enterBadPasswords(DataTable dataTable, int row, int col) throws Exception {
		data = dataTable.raw();
		sendKeysToWebElement(textField_NewPassword, data.get(row).get(col));
		confirmNewPasswordCharacterErrorMessageIsDisplayed("requiresatleastspecialcharacteruppercaseletterlowercaseletterandnumber");
		return new PasswordReset_Page();
	}
	
	public PasswordReset_Page confirmNewPasswordLengthErrorMessageIsDisplayed(String error) throws Exception {
		waitUntilWebElementIsVisible(text_NewPasswordLengthError);
		Assert.assertEquals(error, text_NewPasswordLengthError.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new PasswordReset_Page();
	}
	
	public PasswordReset_Page confirmNewPasswordCharacterErrorMessageIsDisplayed(String error) throws Exception {
		waitUntilWebElementIsVisible(text_NewPasswordCharacterError);
		Assert.assertEquals(error, text_NewPasswordCharacterError.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new PasswordReset_Page();
	}
	
	public PasswordReset_Page confirmConfirmPasswordErrorMessageIsDisplayed(String error) throws Exception {
		waitUntilWebElementIsVisible(text_ConfirmPasswordError);
		Assert.assertEquals(error, text_ConfirmPasswordError.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new PasswordReset_Page();
	}
	
	public PasswordReset_Page enterConfirmPassword(String password) throws Exception {
		sendKeysToWebElement(textField_ConfirmPassword, password);
		return new PasswordReset_Page();
	}
	
	public PasswordReset_Page clickSaveButton() throws Exception {
		waitAndClickElement(button_ResetPassword);
		return new PasswordReset_Page();
	}
	
}
