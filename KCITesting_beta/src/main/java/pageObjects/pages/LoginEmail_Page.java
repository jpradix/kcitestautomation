package pageObjects.pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;
import utils.Constant;

public class LoginEmail_Page extends BasePage {

	List<List<String>> data;
	public @FindBy(xpath = "/html/body/div/form[1]/div/div/div[2]/div[3]/div/div/div/div[2]/div[2]/div/input[1]") WebElement textField_Email;
	public @FindBy(xpath = "/html/body/div/form[1]/div/div/div[2]/div[3]/div/div/div/div[3]/div[2]/div/div/div/input") WebElement button_Continue;
	public @FindBy(xpath = "/html/body/div/form[1]/div/div/div[1]/div[3]/div/div[2]/div/div[2]/div/div[2]/input") WebElement textField_Password;
	public @FindBy(xpath = "/html/body/div/form[1]/div/div/div[1]/div[3]/div/div[2]/div/div[3]/div[2]/div/div/div/input") WebElement button_SignIn;
	
	public LoginEmail_Page() throws IOException {
		super();
	}
	
	public LoginKnowable_Page getLoginEmail_Page() throws Exception {
		getDriver().get(Constant.URL_EMAIL);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterEmail() throws Exception {
		sendKeysToWebElement(textField_Email, Constant.TESTER_EMAIL);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickContinueButton() throws Exception {
		waitAndClickElement(button_Continue);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page enterPassword() throws Exception {
		sendKeysToWebElement(textField_Password, Constant.TESTER_INBOX_PASSWORD);
		return new LoginKnowable_Page();
	}
	
	public LoginKnowable_Page clickSignInButton() throws Exception {
		waitAndClickElement(button_SignIn);
		return new LoginKnowable_Page();
	}
	
}
