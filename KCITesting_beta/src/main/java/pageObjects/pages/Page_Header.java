package pageObjects.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Page_Header extends BasePage {

	public @FindBy(xpath = "//div[@id='__next']/header") WebElement outerDiv_Header;
	public @FindBy(xpath = "//*[@id='__next']/header/div[1]/a") WebElement button_KnowableIcon;
	public @FindBy(xpath = "//div[@id='__next']//nav/a[@href='/dashboard']") WebElement button_Dashboard;
	public @FindBy(xpath = "//div[@id='__next']//nav/a[@href='/contracts']") WebElement button_Contracts;
	public @FindBy(xpath = "//div[@id='__next']//nav/a[@href='/upload']") WebElement button_Upload;
	public @FindBy(xpath = "//*[@id=\"__next\"]/header/div[2]/button/div") WebElement button_Profile;
	public @FindBy(xpath = "/html/body/div[1]/header/div[2]/span") WebElement button_Help;
	public @FindBy(xpath = "/html/body/div[1]/header/div[2]/button/div/span") WebElement text_Welcome;
	public @FindBy(xpath = "/html//div[@id='__next']//ul//button[.='Insights']") WebElement button_Insights;
	public @FindBy(xpath = "/html//div[@id='__next']//ul//button[.='My Account']") WebElement button_MyAccount;
	public @FindBy(xpath = "/html//div[@id='__next']//ul//button[.='Admin Settings']") WebElement button_AdminSettings;
	public @FindBy(xpath = "/html//div[@id='__next']//ul//button[.='Switch Organizations']") WebElement button_SwitchOrg;
	public @FindBy(xpath = "/html//div[@id='__next']//ul//button[.='Log Out']") WebElement button_Logout;
		
	public Page_Header() throws Exception {
		super(); 
	}
	
	public Page_Header clickKnowableIcon() throws Exception {
		waitAndClickElement(button_KnowableIcon);
		return new Page_Header();
	}
	
	public Page_Header clickDashboardTab() throws Exception {
		waitAndClickElement(button_Dashboard);
		return new Page_Header();
	}
	
	public Page_Header clickContractsTab() throws Exception {
		waitAndClickElement(button_Contracts);
		return new Page_Header();
	}
	
	public Page_Header clickUploadTab() throws Exception {
		waitAndClickElement(button_Upload);
		return new Page_Header();
	}
	
	public Page_Header clickHelpButton() throws Exception {
		waitAndClickElement(button_Help);
		return new Page_Header();
	}
	
	public Page_Header clickProfileButton() throws Exception {
		waitAndClickElement(button_Profile);
		return new Page_Header();
	}
	
	public Page_Header clickLogoutButton() throws Exception {
		waitAndClickElement(button_Logout);
		return new Page_Header();
	}
	
	public Page_Header confirmWelcomeMenuAppears() throws Exception {
		Assert.assertTrue(isElementClickable(button_Logout));
		return new Page_Header();
	}
	
	public Page_Header clickMyAccountButton() throws Exception {
		waitAndClickElement(button_MyAccount);
		return new Page_Header();
	}
	
	public Page_Header viewWelcomeMessage() throws Exception {
		waitUntilWebElementIsVisible(text_Welcome);
		return new Page_Header();
	}
	
	public Page_Header confirmWelcomeName() throws Exception {
		Assert.assertEquals("welcomeeric", text_Welcome.getText().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new Page_Header();
	}
	
	public Page_Header hoverAllWelcomeMenuElements() throws Exception {
		actionHover(button_Insights);
		actionHover(button_MyAccount);
		actionHover(button_AdminSettings);
		actionHover(button_SwitchOrg);
		actionHover(button_Logout);
		waitAndClickElement(outerDiv_Header);
		return new Page_Header();
	}
}
