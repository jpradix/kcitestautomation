package pageObjects.pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class Dashboard_Page extends BasePage {

	public String topCounterpartyAmt;
	public String topDocumentTypeAmt;
	public @FindBy(xpath = "/html/body/div/div/div[1]/div[2]/div[1]/div/button[1]") WebElement button_TopCounterparty;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div[2]/div[1]/div/button[1]/h4") WebElement text_TopCounterpartyValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div[2]/div[2]/div/button[1]/p") WebElement button_DocumentType;
	public @FindBy(xpath = "//*[@id='work-area']/div[2]/div[2]/div/button[1]/div/span") WebElement text_DocumentTypeValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div[1]/div[1]/div[1]/h1") WebElement text_TotalContractsNumber;
	public @FindBy(xpath = "//div[@id='work-area']/div[1]/div[1]//h5[.='Total number of Out of Scope Contracts in Database:']") WebElement text_TotalOutOfScopeContracts;
	public @FindBy(xpath = "//*[@id='work-area']/div[1]/div[1]/div[1]") WebElement div_TotalContracts;
	public @FindBy(xpath = "//*[@id='work-area']/div[1]/div[1]/div[2]") WebElement div_TotalOutOfScopeContracts;
	public @FindBy(xpath = "//*[@id='work-area']/div[2]/div[1]/div") WebElement list_TopCounterparty;
	public @FindBy(xpath = "//*[@id='work-area']/div[2]/div[2]/div") WebElement list_TopDocumentType;
	public @FindBy(xpath = "//*[@id='work-area']/div[1]/div[2]/div[2]") WebElement list_DocumentStatus;
	
	public Dashboard_Page() throws Exception {
		super(); 
	}
	
	public Dashboard_Page confirmUserIsOnTheDashboardPage() throws Exception {
		Assert.assertTrue(isElementClickable(text_TopCounterpartyValue));
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page waitForTotalContracts() throws Exception {
		waitUntilWebElementIsVisible(div_TotalContracts);
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page waitForTotalOutOfScope() throws Exception {
		waitUntilWebElementIsVisible(div_TotalContracts);
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page waitForTopCounterpartiesList() throws Exception {
		waitUntilWebElementIsVisible(list_TopCounterparty);
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page waitForDocumentTypeList() throws Exception {
		waitUntilWebElementIsVisible(list_TopDocumentType);
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page waitForDocumentStatusList() throws Exception {
		waitUntilWebElementIsVisible(list_DocumentStatus);
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page confirmDashboardCriticalElementsExist() throws Exception {
		Assert.assertTrue(isElementClickable(div_TotalContracts));
		Assert.assertTrue(isElementClickable(text_TotalOutOfScopeContracts));
		Assert.assertTrue(isElementClickable(list_TopCounterparty));
		Assert.assertTrue(isElementClickable(list_TopDocumentType));
		Assert.assertTrue(isElementClickable(list_DocumentStatus));
		return new Dashboard_Page();	
	}
	
	public Dashboard_Page clickTopCounterpartyButton() throws Exception {
		topCounterpartyAmt = text_TopCounterpartyValue.getText();
		waitAndClickElement(button_TopCounterparty);
		return new Dashboard_Page();
	}
	
	public Dashboard_Page clickDocumentTypeButton() throws Exception {
		topDocumentTypeAmt = text_DocumentTypeValue.getText();
		waitAndClickElement(button_DocumentType);
		return new Dashboard_Page();
	}
	
}
