package pageObjects.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import pageObjects.BasePage;

public class EmailInbox_Page extends BasePage {

	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div/div/div/div/div/div[2]") WebElement button_OpenEmail;
	public @FindBy(xpath = "//*[@id='LPlnk976314']") WebElement link_ResetPassword;
	public @FindBy(partialLinkText = "https://stage.kci.theknowable.com/reset") WebElement text_ResetPassword;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div/div[1]/div[2]/div/div/div/div/div/div[2]/div/div/div/div[2]/div[1]/div[2]/button[1]") WebElement button_Delete;
	public @FindBy(xpath = "/html//div[@id='app']/div/div[@class='_3KAPMPOz8KsW24ooeUflK2']/div[@class='_2jR8Yc0t2ByBbcz_HIGqZ4']//div[@class='_1jw6v9zFEgnOiXShpU1qqM']/div/div[@role='region']//div[@class='_3EKWmVhftsURDkAdylmz_G threeColConversationViewSenderImageOn']/div/div[1]/div[@role='tablist']/button[1]/span//span[.=' Focused']") WebElement button_Focused;

	public EmailInbox_Page() throws Exception {
		super();
	}
	
	public EmailInbox_Page clickEmailOpenButton() throws Exception {
		waitAndClickElement(button_OpenEmail);
		return new EmailInbox_Page();
	}
	
	public EmailInbox_Page clickResetPasswordLink() throws Exception {
		waitAndClickElement(text_ResetPassword);
		return new EmailInbox_Page();
	}
	
	public EmailInbox_Page clickDeleteEmailButton() throws Exception {
		actionHover(button_OpenEmail);
		actionMoveAndClick(button_Delete);
		return new EmailInbox_Page();
	}
	
	public EmailInbox_Page clickFocusEmailButton() throws Exception {
		waitAndClickElement(button_Focused);
		return new EmailInbox_Page();
	}
}
