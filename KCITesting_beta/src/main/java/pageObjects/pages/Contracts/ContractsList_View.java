package pageObjects.pages.Contracts;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePage;

public class ContractsList_View extends BasePage {

	public String actualCountStr;
	public int totalListViewPagesInt;
	public int pageIndex;
	public String[] chars;
	public String actualCountStrNew;
	public ArrayList<String> fieldColumnOrder;
	public String columnOrderBefore;
	public String columnOrderAfter;
	public int attempts = 0;

	// Arrow buttons
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[1]/a") WebElement button_Page1;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[2]/a") WebElement button_Page2;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[3]/a") WebElement button_Page3;
	public @FindBy(xpath = "//div[@id='work-area']/div/div//a[@title='Go to the next page']/span") WebElement buttonArrow_PageNext;
	public @FindBy(xpath = "//div[@id='work-area']/div/div//a[@title='Go to the previous page']/span") WebElement button_PagePrevious;
	public @FindBy(xpath = "//div[@id='work-area']/div/div//a[@title='Go to the last page']/span") WebElement buttonArrow_PageLast;
	public @FindBy(xpath = "//div[@id='work-area']/div/div//a[@title='Go to the first page']/span") WebElement buttonArrow_PageFirst;

	// "More" buttons
	public @FindBy(css = ".ellipses.heDnbR.styles__Ellipses-sc-1j2bww7-1") WebElement columnButton_More;
	public @FindBy(xpath = "/html/body/div[4]//button[.='Customize Table']") WebElement columnButton_More_CustomizeTable;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div//table[@class='k-grid-table']/tbody/tr[1]/td[9]//button/div") WebElement button_TopContract_More;
	public @FindBy(xpath = "/html/body//button[.='Download']") WebElement button_TopContract_More_Download;

	// Column headers
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[1]") WebElement icon_Circle;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[1]/div/table/thead/tr/th[1]/div/div") WebElement tooltip_Circle;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[1]/div/table/thead/tr/th[2]") WebElement icon_Flag;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[1]/div/table/thead/tr/th[2]/div/div") WebElement tooltip_Flag;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[3]//a[@href='#']") WebElement columnHeader_03;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[4]//a[@href='#']") WebElement columnHeader_04;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[5]//a[@href='#']") WebElement columnHeader_05;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[6]//a[@href='#']") WebElement columnHeader_06;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[7]//a[@href='#']") WebElement columnHeader_07;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[8]//a[@href='#']") WebElement columnHeader_08;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[9]//a[@href='#']") WebElement columnHeader_09;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[10]//a[@href='#']") WebElement columnHeader_10;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[11]//a[@href='#']") WebElement columnHeader_11;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[12]//a[@href='#']") WebElement columnHeader_12;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[13]//a[@href='#']") WebElement columnHeader_13;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[14]//a[@href='#']") WebElement columnHeader_14;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[15]//a[@href='#']") WebElement columnHeader_15;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[16]//a[@href='#']") WebElement columnHeader_16;
	public @FindBy(xpath = "//div[@id='work-area']/div/div/div/div[@class='k-grid-header']//table//tr/th[17]//a[@href='#']") WebElement columnHeader_17;

	// First column entry values
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[1]/td[3]") WebElement text_FirstEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[2]/td[3]") WebElement text_SecondEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[3]/td[3]") WebElement text_ThirdEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[4]/td[3]") WebElement text_FourthEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[5]/td[3]") WebElement text_FifthEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[6]/td[3]") WebElement text_SixthEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[7]/td[3]") WebElement text_SeventhEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[8]/td[3]") WebElement text_EighthEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[9]/td[3]") WebElement text_NinthEntryFirstColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[10]/td[3]") WebElement text_TenthEntryFirstColumnValue;

	// Second column entry values
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[1]/td[4]") WebElement text_FirstEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[2]/td[4]") WebElement text_SecondEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[3]/td[4]") WebElement text_ThirdEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[4]/td[4]") WebElement text_FourthEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[5]/td[4]") WebElement text_FifthEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[6]/td[4]") WebElement text_SixthEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[7]/td[4]") WebElement text_SeventhEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[8]/td[4]") WebElement text_EighthEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[9]/td[4]") WebElement text_NinthEntrySecondColumnValue;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[10]/td[4]") WebElement text_TenthEntrySecondColumnValue;

	// Miscellaneous elements
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr/td/div/span[1]") WebElement message_NoResultsFound;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[1]") WebElement button_TopContract;
	public @FindBy(xpath = "/html//div[@id='pdf-wrapper']//a") WebElement button_Back;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[3]/div") WebElement text_ListPageCount;
	public @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/table/tbody/tr[1]/td[4]") WebElement text_FirstEntryFourthColumnValue;
	public String fileName;

	public ContractsList_View() throws Exception {
		super();
	}

	public ContractsList_View confirmUserIsOnTheContractsListViewPage() throws Exception {
		Assert.assertTrue(isElementClickable(icon_Circle));
		return new ContractsList_View();
	}

	public ContractsList_View clickContractAtTopOfList() throws Exception {
		waitAndClickElement(button_TopContract);
		return new ContractsList_View();
	}

	public ContractsList_View verifyNumberOfContractsAndSearchFieldValue(String contractAmt) throws Exception {
		Thread.sleep(500);
		actualCountStr = getNumberOfContracts();
		Assert.assertEquals(contractAmt, actualCountStr);
		return new ContractsList_View();
	}

	public String getNumberOfContracts() {
		String text = text_ListPageCount.getText();
		String[] contractCount = text.split(" ");
		String actualCountStr = contractCount[4];
		return actualCountStr;
	}

	public ContractsList_View clickMoreButtonOnContract() throws Exception {
		waitAndClickElement(columnButton_More);
		return new ContractsList_View();
	}

	public ContractsList_View confirmCustomizeTableButtonExists() throws Exception {
		Assert.assertTrue(isElementClickable(columnButton_More_CustomizeTable));
		return new ContractsList_View();
	}

	public ContractsList_View clickCustomizeTableButton() throws Exception {
		waitAndClickElement(columnButton_More_CustomizeTable);
		return new ContractsList_View();
	}

	public ContractsList_View confirmHeadersHaveChanged() throws Exception {
		waitAndClickElement(columnButton_More_CustomizeTable);
		return new ContractsList_View();
	}

	public ContractsList_View compareColumnHeaderOrderToCustomizableFieldsOrder(String fieldOrder) throws Exception {
		Thread.sleep(800);
		tempWait = new WebDriverWait(driver, 0);
		fieldColumnOrder = new ArrayList<String>();
		WebElement[] theColumns = { columnHeader_03, columnHeader_04, columnHeader_05, columnHeader_06, columnHeader_07,
				columnHeader_08, columnHeader_09, columnHeader_10, columnHeader_11, columnHeader_12, columnHeader_13,
				columnHeader_14, columnHeader_15, columnHeader_16, columnHeader_17 };
		count: for (int i = 0; i <= 14; i++) {
			try {
				this.tempWait.until(ExpectedConditions.visibilityOf(theColumns[i]));
				fieldColumnOrder.add(i, theColumns[i].getText());
			} catch (Exception e) {
				break count;
			}
		}
		soft.assertEquals(fieldOrder, fieldColumnOrder.toString().toLowerCase().replaceAll("[^a-z]|\\s", ""));
		return new ContractsList_View();
	}

	public ContractsList_View clickAndDragColumnHeader() throws Exception {
		columnOrderBefore = fieldColumnOrder.toString().toLowerCase().replaceAll("[^a-z]|\\s", "");
		actionClickAndDrag(columnHeader_03, columnHeader_05);
		return new ContractsList_View();
	}

	public ContractsList_View compareColumnHeadersAfterClickDragging() throws Exception {
		tempWait = new WebDriverWait(driver, 0);
		fieldColumnOrder = new ArrayList<String>();
		WebElement[] theColumns = { columnHeader_03, columnHeader_04, columnHeader_05, columnHeader_06, columnHeader_07,
				columnHeader_08, columnHeader_09, columnHeader_10, columnHeader_11, columnHeader_12, columnHeader_13,
				columnHeader_14, columnHeader_15, columnHeader_16, columnHeader_17 };
		count: for (int i = 0; i <= 14; i++) {
			try {
				this.tempWait.until(ExpectedConditions.visibilityOf(theColumns[i]));
				fieldColumnOrder.add(i, theColumns[i].getText());
			} catch (Exception e) {
				break count;
			}
		}
		columnOrderAfter = fieldColumnOrder.toString().toLowerCase().replaceAll("[^a-z]|\\s", "");
		soft.assertEquals(columnOrderAfter, "counterpartynamefilenameeffectivedatecliententityuploaddatedocumenttype");
		return new ContractsList_View();
	}

	public ContractsList_View hoverOverCircleIcon() throws Exception {
		actionHover(icon_Circle);
		return new ContractsList_View();
	}

	public ContractsList_View confirmControlColorKeyTooltipDisplays() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(tooltip_Circle));
		return new ContractsList_View();
	}

	public ContractsList_View hoverOverFlagIcon() throws Exception {
		actionHover(icon_Flag);
		return new ContractsList_View();
	}

	public ContractsList_View confirmFlagColorKeyTooltipDisplays() throws Exception {
		Assert.assertTrue(waitUntilWebElementIsVisible(tooltip_Flag));
		return new ContractsList_View();
	}

	public ContractsList_View clickSecondPageButton() throws Exception {
		waitAndClickElement(button_Page2);
		return new ContractsList_View();
	}

	public ContractsList_View confirmSecondPageIsDisplayed() throws Exception {
		Thread.sleep(750);
		confirmWebElementTagHasSpecificValue(button_Page2, "class", "k-state-selected");
		return new ContractsList_View();
	}

	public ContractsList_View clickNextPageButton() throws Exception {
		waitAndClickElement(buttonArrow_PageNext);
		return new ContractsList_View();
	}

	public ContractsList_View confirmThirdPageIsDisplayed() throws Exception {
		Thread.sleep(750);
		confirmWebElementTagHasSpecificValue(button_Page3, "class", "k-state-selected");
		return new ContractsList_View();
	}

	public ContractsList_View clickLastPageButton() throws Exception {
		waitAndClickElement(buttonArrow_PageLast);
		return new ContractsList_View();
	}

	public ContractsList_View confirmLastPageIsDisplayed() throws Exception {
		Thread.sleep(750);
		actualCountStr = getNumberOfContracts();
		double actualCountInt = Double.parseDouble(actualCountStr);
		double totalListViewPagesDouble = Math.ceil(actualCountInt / 20);
		totalListViewPagesInt = (int) totalListViewPagesDouble;
		if (totalListViewPagesInt > 10) {
			pageIndex = totalListViewPagesInt + 1;
		} else {
			pageIndex = totalListViewPagesInt;
		}
		actualCountStr = Integer.toString(pageIndex);
		chars = actualCountStr.split("");
		actualCountStrNew = chars[1];
		try {
			WebElement button_LastPageNumber = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[" + actualCountStrNew + "]/a"));
			confirmWebElementTagHasSpecificValue(button_LastPageNumber, "class", "k-state-selected");
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement button_LastPageNumberUpdated = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[" + actualCountStrNew + "]/a"));
			confirmWebElementTagHasSpecificValue(button_LastPageNumberUpdated, "class", "k-state-selected");
		}
		return new ContractsList_View();
	}

	public ContractsList_View clickPreviousPageButton() throws Exception {
		waitAndClickElement(button_PagePrevious);
		return new ContractsList_View();
	}

	public ContractsList_View confirmPreviousPageIsDisplayed() throws Exception {
		Thread.sleep(750);
		pageIndex--;
		actualCountStr = Integer.toString(pageIndex);
		chars = actualCountStr.split("");
		actualCountStrNew = chars[1];
		try {
			WebElement button_PreviousPageNumber = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[" + actualCountStrNew + "]/a"));
			confirmWebElementTagHasSpecificValue(button_PreviousPageNumber, "class", "k-state-selected");
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement button_PreviousPageNumberUpdated = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div/div/div[3]/ul/li[" + actualCountStrNew + "]/a"));
			confirmWebElementTagHasSpecificValue(button_PreviousPageNumberUpdated, "class", "k-state-selected");
		}
		return new ContractsList_View();
	}

	public ContractsList_View clickFirstPageButton() throws Exception {
		waitAndClickElement(buttonArrow_PageFirst);
		return new ContractsList_View();
	}

	public ContractsList_View confirmFirstPageIsDisplayed() throws Exception {
		Thread.sleep(750);
		confirmWebElementTagHasSpecificValue(button_Page1, "class", "k-state-selected");
		return new ContractsList_View();
	}

	public ContractsList_View confirmNoResultsFound() throws Exception {	
		if (assertTextMatches("noresultsfound", message_NoResultsFound) == true) {
			System.out.println("--- List view accurately displays lack of results.");
		} else {
			System.out.println("--- List view displaying results where it should not.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmCounterpartyNameResults() throws Exception {
		if (assertTextMatches("kangaroollc", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Counterparty Name results are displayed.");
		} else {
			System.out.println("--- Expected Counterparty Name results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmClientEntityResults() throws Exception {
		if (assertTextMatches("cliententitytestcompany", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Client Entity results are displayed.");
		} else {
			System.out.println("--- Expected Client Entity results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmDocumentTypeResults() throws Exception {
		if (assertTextMatches("other", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Document Type results are displayed.");
		} else {
			System.out.println("--- Expected Document Type results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmContractIDResults() throws Exception {
		if (assertTextMatches("fbfcbaacaefaf", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Contract ID results are displayed.");
		} else {
			System.out.println("--- Expected Contract ID results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmDocumentTitleResults() throws Exception {
		if (assertTextMatches("molecalciumecho", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Document Title results are displayed.");
		} else {
			System.out.println("--- Expected Document Title results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmDocumentTypeDefineOtherResults() throws Exception {
		if (assertTextMatches("uranus", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Document Type (Define Other) results are displayed.");
		} else {
			System.out.println("--- Expected Document Type (Define Other) results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmEffectiveDateResults() throws Exception {
		if (assertTextMatches("332012", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Effective Date results are displayed.");
		} else {
			System.out.println("--- Expected Effective Date results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmFileNameResults() throws Exception {
		if (assertTextMatches("certificateofincorporationpdf", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected File Name results are displayed.");
		} else {
			System.out.println("--- Expected File Name results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmContractExecutionStatusResults() throws Exception {
		if (assertTextMatches("fullyexecuted", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Contract Execution Status results are displayed.");
		} else {
			System.out.println("--- Expected Contract Execution Status results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmContractStatusResults() throws Exception {
		if (assertTextMatches("reviewcomplete", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Contract Status results are displayed.");
		} else {
			System.out.println("--- Expected Contract Status results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmParentControlNumberResults() throws Exception {
		if (assertTextMatches("exacolapdf", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Parent Control Number results are displayed.");
		} else {
			System.out.println("--- Expected Parent Control Number results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmDocumentIssuesResults() throws Exception {
		if (assertTextMatches("illegibletechissues", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Document Issues results are displayed.");
		} else {
			System.out.println("--- Expected Document Issues results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmOutOfScopeResults() throws Exception {
		if (assertTextMatches("yes", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Out of Scope results are displayed.");
		} else {
			System.out.println("--- Expected Out of Scope results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmUploadByResults() throws Exception {
		if (assertTextMatches("jessetracy", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Upload By results are displayed.");
		} else {
			System.out.println("--- Expected Upload By results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View confirmUploadDateResults() throws Exception {
		if (assertTextMatches("7192019", text_FirstEntryFirstColumnValue) == true) {
			System.out.println("--- Expected Upload Date results are displayed.");
		} else {
			System.out.println("--- Expected Upload Date results are not displayed.");
		}
		return new ContractsList_View();
	}

	public ContractsList_View sortPrimaryColumnHeader() throws Exception {
		waitAndClickElement(columnHeader_03);
		Thread.sleep(1000);
		return new ContractsList_View();
	}

	public ContractsList_View sortSecondaryColumnHeader() throws Exception {
		waitAndClickElement(columnHeader_04);
		Thread.sleep(1000);
		return new ContractsList_View();
	}

	public ContractsList_View verifySortIsAscendingOrder(int secondarySortIndicator) throws Exception {
		String previous = "";
		int index = 0;
		try {
			String[] entryFirstColumnValues = {
					text_FirstEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SecondEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_ThirdEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FourthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FifthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SixthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SeventhEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_EighthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_NinthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_TenthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "") };
			String[] entrySecondColumnValues = {
					text_FirstEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SecondEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_ThirdEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FourthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FifthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SixthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SeventhEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_EighthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_NinthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_TenthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "") };
			if (secondarySortIndicator == 0) {
				verify: for (String current : entryFirstColumnValues) {
					if (current.compareTo(previous) >= 0) {
						previous = current;
						if (current == entryFirstColumnValues[4]) {
							System.out.println("PRIMARY column is correctly sorted in ASCENDING order. Current order: "
									+ Arrays.toString(entryFirstColumnValues));
						}
					} else {
						System.out.println("PRIMARY column is NOT in ASCENDING order. Current order: "
								+ Arrays.toString(entryFirstColumnValues) + " --- Order broken at: [" + previous + ", "
								+ current + "]");
						soft.assertTrue(false,
								"PRIMARY column is NOT in ASCENDING order. Current order: "
										+ Arrays.toString(entryFirstColumnValues) + " --- Order broken at: [" + previous
										+ ", " + current + "]");
						break verify;
					}
				}
			} else {
				for (String current : entryFirstColumnValues) {
					if (current.equals(previous)) {
						String previousEntry = entrySecondColumnValues[index - 1];
						String currentEntry = entrySecondColumnValues[index];
						if (currentEntry.compareTo(previousEntry) > 0) {
							System.out.println("SECONDARY column is correctly sorted in ASCENDING order. \""
									+ previousEntry + "\" appears before \"" + currentEntry + "\"");
						} else if (currentEntry.compareTo(previousEntry) < 0) {
							System.out.println("SECONDARY column is NOT in ASCENDING order. Order broken at: ["
									+ previous + ", " + current + "]");
							soft.assertTrue(false, "SECONDARY column is NOT in ASCENDING order. Order broken at: ["
									+ previous + ", " + current + "]");
						} else {
							System.out.println("SECONDARY column is correctly sorted in ASCENDING order. \""
									+ previousEntry + "\" matches \"" + currentEntry + "\"");
						}
					}
					previous = current;
					index++;
				}
				entryFirstColumnValues = null;
				entrySecondColumnValues = null;
				attempts = 0;
			}
		} catch (StaleElementReferenceException e) {
			attempts++;
			if (attempts <= 1) {
				Thread.sleep(500);
				verifySortIsAscendingOrder(secondarySortIndicator);
			}
		}
		return new ContractsList_View();
	}

	public ContractsList_View verifySortIsDescendingOrder(int secondarySortIndicator) throws Exception {
		String previous = "";
		int index = 0;
		try {
			String[] entryFirstColumnValues = {
					text_FirstEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SecondEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_ThirdEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FourthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FifthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SixthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SeventhEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_EighthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_NinthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_TenthEntryFirstColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "") };
			String[] entrySecondColumnValues = {
					text_FirstEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SecondEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_ThirdEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FourthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_FifthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SixthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_SeventhEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_EighthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_NinthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", ""),
					text_TenthEntrySecondColumnValue.getText().toLowerCase().replaceAll("[^a-z0-9]|\\s", "") };
			if (secondarySortIndicator == 0) {
				verify: for (String current : entryFirstColumnValues) {
					if (current == entryFirstColumnValues[0]) {
						previous = current;
						continue;
					}
					if (current.compareTo(previous) <= 0) {
						previous = current;
						if (current == entryFirstColumnValues[4]) {
							System.out.println("PRIMARY column is correctly sorted in DESCENDING order. Current order: "
									+ Arrays.toString(entryFirstColumnValues));
						}
					} else {
						System.out.println("PRIMARY column is NOT in DESCENDING order. Current order: "
								+ Arrays.toString(entryFirstColumnValues) + " --- Order broken at: [" + previous + ", "
								+ current + "]");
						soft.assertTrue(false,
								"PRIMARY column is NOT in DESCENDING order. Current order: "
										+ Arrays.toString(entryFirstColumnValues) + " --- Order broken at: [" + previous
										+ ", " + current + "]");
						break verify;
					}
				}
			} else if (secondarySortIndicator == 1) {
				for (String current : entryFirstColumnValues) {
					if (current == entryFirstColumnValues[0]) {
						previous = current;
						index++;
						continue;
					}
					if (current.equals(previous)) {
						String previousEntry = entrySecondColumnValues[index - 1];
						String currentEntry = entrySecondColumnValues[index];
						if (currentEntry.compareTo(previousEntry) < 0) {
							System.out.println("SECONDARY column is correctly sorted in DESCENDING order. \""
									+ previousEntry + "\" appears after \"" + currentEntry + "\"");
						} else if (currentEntry.compareTo(previousEntry) > 0) {
							System.out.println("SECONDARY column is NOT in DESCENDING order. Order broken at: ["
									+ previous + ", " + current + "]");
							soft.assertTrue(false, "SECONDARY column is NOT in DESCENDING order. Order broken at: ["
									+ previous + ", " + current + "]");
						} else {
							System.out.println("SECONDARY column is correctly sorted in DESCENDING order. \""
									+ previousEntry + "\" matches \"" + currentEntry + "\"");
						}
					}
					previous = current;
					index++;
				}
				entryFirstColumnValues = null;
				entrySecondColumnValues = null;
				attempts = 0;
			} else {
				verify: for (String current : entrySecondColumnValues) {
					if (current == entrySecondColumnValues[0]) {
						previous = current;
						continue;
					}
					if (current.compareTo(previous) <= 0) {
						previous = current;
						if (current == entrySecondColumnValues[4]) {
							System.out
									.println("SECONDARY column is correctly sorted in DESCENDING order. Current order: "
											+ Arrays.toString(entrySecondColumnValues));
						}
					} else {
						System.out.println("SECONDARY column is NOT in DESCENDING order. Current order: "
								+ Arrays.toString(entrySecondColumnValues) + " --- Order broken at: [" + previous + ", "
								+ current + "]");
						soft.assertTrue(false,
								"SECONDARY column is NOT in DESCENDING order. Current order: "
										+ Arrays.toString(entrySecondColumnValues) + " --- Order broken at: ["
										+ previous + ", " + current + "]");
						break verify;
					}
				}
			}
		} catch (StaleElementReferenceException e) {
			attempts++;
			if (attempts <= 1) {
				Thread.sleep(500);
				verifySortIsDescendingOrder(secondarySortIndicator);
			}
		}
		return new ContractsList_View();
	}

	public ContractsList_View clickTopContractMoreButton() throws Exception {
		waitAndClickElement(button_TopContract_More);
		return new ContractsList_View();
	}

	public ContractsList_View confirmTopContractContractDownloadButtonExists() throws Exception {
		waitUntilWebElementIsVisible(button_TopContract_More_Download);
		Assert.assertTrue(isElementClickable(button_TopContract_More_Download));
		return new ContractsList_View();
	}

	public ContractsList_View clickTopContractDownloadButton() throws Exception {
		fileName = text_FirstEntryFourthColumnValue.getText();
		waitAndClickElement(button_TopContract_More_Download);
		return new ContractsList_View();
	}
}