package pageObjects.pages.Contracts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import pageObjects.BasePage;

public class ContractsViewer_View extends BasePage {

	private String[] totalPages;
	private String currentPage;
	private String testMessage;
	private String disabled = null;
	private String scrollDownConfirmation;
	private String scrollUpConfirmation;
	@SuppressWarnings("unused")
	private String fileName;
	private String nextPage;
	private String previousPage;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div") WebElement div_ContractPreview;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[1]") WebElement page_01;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[2]") WebElement page_02;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[3]") WebElement page_03;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[4]") WebElement page_04;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[5]") WebElement page_05;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[6]") WebElement page_06;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[7]") WebElement page_07;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[8]") WebElement page_08;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[9]") WebElement page_09;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[10]") WebElement page_10;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[11]") WebElement page_11;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[12]") WebElement page_12;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[2]/div/div[13]") WebElement page_13;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]/span[2]") WebElement button_ArrowRight;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]/span[1]") WebElement button_ArrowLeft;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]") WebElement text_HeaderTotalPages;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]/input") WebElement textField_HeaderCurrentPage;
	private @FindBy(xpath = "/html/body/div[1]/div/div[1]/div/div[1]/div/div[1]") WebElement text_FooterTotalPages;
	private @FindBy(xpath = "/html//div[@id='pdf-wrapper']//button[.='Download']") WebElement button_Download;
	private @FindBy(xpath = "/html//div[@id='pdf-wrapper']/div[1]/div/a") WebElement button_Back;
	
	public ContractsViewer_View() throws IOException {
		super();
	}

	public ContractsViewer_View confirmUserIsOnTheContractsViewerPage() throws Exception {
		Assert.assertTrue(isElementClickable(div_ContractPreview));
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickOnBackButton() throws Exception {
		waitAndClickElement(button_Back);
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickRightArrowButton() throws Exception {
		waitAndClickElement(button_ArrowRight);
		return new ContractsViewer_View();
	}

	public ContractsViewer_View confirmContractSecondPageIsVisible() throws Exception {
		Thread.sleep(500);
		soft.assertEquals("2", textField_HeaderCurrentPage.getAttribute("value"));
		if ((textField_HeaderCurrentPage.getAttribute("value").equals("2"))) {
			System.out.println("SUCCESS: Second page of contract is visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
		} else {
			System.out.println("FAIL: Second page of contract is NOT visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
		}
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickRightArrowButtonThroughContract() throws Exception {
		currentPage = textField_HeaderCurrentPage.getAttribute("value");
		testMessage = "Waiting for page # ";
		// Loop will run as long as arrow button is not disabled...
		while (disabled == null) {
			// ... or until loop kill switch is thrown once page 10 is reached
			if (!currentPage.equals("10")) {
				nextPage = Integer.toString(Integer.parseInt(currentPage) + 1);
				waitAndClickElement(button_ArrowRight);
				disabled = button_ArrowRight.getAttribute("disabled");
				System.out.print(testMessage + nextPage);
				// Give the DOM time to update the current page value before reiterating loop
				do {
					currentPage = textField_HeaderCurrentPage.getAttribute("value");
					System.out.print(".");
				} while (!currentPage.equals(nextPage));
				System.out.println(" SUCCESS");
				// Execute loop kill switch
			} else {
				disabled = "disabled";
			}
		}
		disabled = null;
		return new ContractsViewer_View();
	}

	public ContractsViewer_View confirmContractLastPageIsVisible() throws Exception {
		Thread.sleep(500);
		currentPage = textField_HeaderCurrentPage.getAttribute("value");
		totalPages = text_HeaderTotalPages.getText().split(" ");
		try {
			assertThat(currentPage, isOneOf(totalPages[1], "10"));
			if (currentPage.equals("10")) {
				scrollDownConfirmation = "SUCCESS (Scroll): Tenth page was visible using locator: " + "<"
						+ textField_HeaderCurrentPage.toString() + ">";
			} else {
				scrollDownConfirmation = "SUCCESS (Scroll): Last page was visible using locator: " + "<"
						+ textField_HeaderCurrentPage.toString() + ">";
			}
			System.out.println("SUCCESS: Last or tenth page of contract is visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
		} catch (AssertionError e) {
			scrollDownConfirmation = "FAIL (Scroll): Last or tenth page was NOT visible.";
			System.out.println("FAIL: Last or tenth page of contract is NOT visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
			soft.fail("FAIL: Last page of contract is NOT visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">" + "\nExpected \"" + totalPages[1]
					+ "\" or \"10\", but received \"" + currentPage + "\"");
		}
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickLeftArrowButtonThroughContract() throws Exception {
		currentPage = textField_HeaderCurrentPage.getAttribute("value");
		testMessage = "Waiting for page # ";
		while (disabled == null) {
			if (!currentPage.equals("1")) {
				previousPage = Integer.toString(Integer.parseInt(currentPage) - 1);
				waitAndClickElement(button_ArrowLeft);
				disabled = button_ArrowLeft.getAttribute("disabled");
				System.out.print(testMessage + previousPage);
				do {
					currentPage = textField_HeaderCurrentPage.getAttribute("value");
					System.out.print(".");
				} while (!currentPage.equals(previousPage) && !previousPage.equals("0"));
				System.out.println(" SUCCESS");
			} else {
				disabled = "disabled";
			}	
		}
		disabled = null;
		return new ContractsViewer_View();
	}

	public ContractsViewer_View confirmContractFirstPageIsVisible() throws Exception {
		Thread.sleep(500);
		Assert.assertEquals("1", textField_HeaderCurrentPage.getAttribute("value"));
		if ((textField_HeaderCurrentPage.getAttribute("value").equals("1"))) {
			scrollUpConfirmation = "SUCCESS (Scroll): First page was visible.";
			System.out.println("SUCCESS: First page of contract is visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
		} else {
			scrollUpConfirmation = "FAIL (Scroll): First page was NOT visible.";
			System.out.println("FAIL: First page of contract is NOT visible using locator: " + "<"
					+ textField_HeaderCurrentPage.toString() + ">");
		}
		return new ContractsViewer_View();
	}

	public ContractsViewer_View scrollThroughContract() throws Exception {
		int scrolls = 0;
		int i;
		for (i = 0; i <= 10650; i = i += 1065) {
			String currentPage = textField_HeaderCurrentPage.getAttribute("value");
			scrolls++;
			if (!currentPage.equals("10") && scrolls <= 20) {
				scrollThroughElementBy(div_ContractPreview, Integer.toString(i));
			}
		}
		scrolls = 0;
		confirmContractLastPageIsVisible();
		for (i = 10650; i >= 0; i -= 1065) {
			scrollThroughElementBy(div_ContractPreview, Integer.toString(i));
		}
		confirmContractFirstPageIsVisible();
		return new ContractsViewer_View();
	}

	public ContractsViewer_View confirmScrollSuccess() throws Exception {
		if (scrollDownConfirmation.contains("SUCCESS") && scrollUpConfirmation.contains("SUCCESS")) {
			System.out.println("SUCCESS: All pages successfully reached with scrolling.");
		} else {
			System.out.println(scrollDownConfirmation);
			System.out.println(scrollUpConfirmation);
		}
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickPageNumberInputField() throws Exception {
		waitAndClickElement(textField_HeaderCurrentPage);
		return new ContractsViewer_View();
	}

	public ContractsViewer_View enterPageNumbers() throws Exception {
		Thread.sleep(1000);
		List<String> selectedPages = new ArrayList<String>();
		totalPages = text_HeaderTotalPages.getText().split(" ");
		List<String> pages = new ArrayList<String>();
		int max = Integer.parseInt(totalPages[1]);
		for (int i = 0; i < max; i++) {
			pages.add(i, Integer.toString(i + 1));
		}
		for (int i = 1; i <= 10; i++) {
			if (pages.size() < 1) {
				break;
			}
			Random generator = new Random();
			int randomIndex = generator.nextInt(pages.size());
			selectedPages.add((i - 1), pages.get(randomIndex));
			sendKeysToWebElement(textField_HeaderCurrentPage, pages.get(randomIndex));
			pages.remove(randomIndex);
			waitAndClickElement(div_ContractPreview);
		}
		return new ContractsViewer_View();
	}


	public ContractsViewer_View clickDownloadButton() throws Exception {
		fileName = contractDataSearchPanel.text_FileName.getText();
		waitAndClickElement(button_Download);
		return new ContractsViewer_View();
	}

	public ContractsViewer_View clickBackButton() throws Exception {
		waitAndClickElement(button_Back);
		return new ContractsViewer_View();
	}

	public int getViewerResultAmount() {
		int count = 0;
		WebElement element;
		StringBuilder bodyText = new StringBuilder("");
		for (int i = 1; i <= 13; i++) {
			element = driver.findElement(
					By.xpath("/html/body/div[1]/div/div[1]/div/div[2]/div/div[" + Integer.toString(i) + "]"));
			scrollToElementByWebElementLocator(element);
			bodyText.append(element.getText().toLowerCase());
		}
		String fullText = bodyText.toString();
		while (fullText.contains("resume")) {
			count++;
			fullText = fullText.substring(fullText.indexOf("resume") + "resume".length());
		}
		return count;
	}

}
