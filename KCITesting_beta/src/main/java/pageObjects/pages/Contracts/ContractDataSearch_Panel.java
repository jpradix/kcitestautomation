package pageObjects.pages.Contracts;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageObjects.BasePage;

public class ContractDataSearch_Panel extends BasePage {

	public String status;
	public @FindBy(xpath = "//div[@id='__next']//section/nav//ul//a[.='Data']") WebElement tab_Data;
	public @FindBy(xpath = "//div[@id='__next']//section/nav//ul//a[.='Search']") WebElement tab_Search;

	// Data Tab
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[1]/div[1]") WebElement dropdown_Select;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[1]/div") WebElement option_01;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[2]/div") WebElement option_02;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[3]/div") WebElement option_03;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[4]/div") WebElement option_04;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[5]/div") WebElement option_05;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[6]/div") WebElement option_06;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[7]/div") WebElement option_07;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[8]/div") WebElement option_08;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[9]/div") WebElement option_09;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[10]/div") WebElement option_10;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[11]/div") WebElement option_11;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[12]/div") WebElement option_12;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[13]/div") WebElement option_13;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[14]/div") WebElement option_14;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[15]/div") WebElement option_15;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[16]/div") WebElement option_16;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[17]/div") WebElement option_17;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[18]/div") WebElement option_18;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[19]/div") WebElement option_19;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[20]/div") WebElement option_20;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[21]/div") WebElement option_21;
	public @FindBy(xpath = "/html//div[@id='__next']//section//form/div[1]/div/div/div[2]/div/div[22]/div") WebElement option_22;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/h4") WebElement header_01;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[3]/h4") WebElement header_02;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[3]/div") WebElement text_FileName;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[1]/h5") WebElement subheader_01;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[2]/h5") WebElement subheader_02;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[3]/h5") WebElement subheader_03;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[4]/h5") WebElement subheader_04;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[5]/h5") WebElement subheader_05;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[6]/h5") WebElement subheader_06;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[7]/h5") WebElement subheader_07;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/ul/li[8]/h5") WebElement subheader_08;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[3]/ul/li/h5") WebElement nextSubheader_01;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[1]/div/div/div[1]/div[1]/div[2]/div[2]") WebElement button_CategoryX;

	// Search Tab
	public @FindBy(xpath = "//div[@id='__next']//section//form/div/input") WebElement textField_SearchDocument;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div/form/div[2]/div/button[2]") WebElement button_ArrowDown;
	public @FindBy(xpath = "/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]") WebElement text_SearchResults;

	// Related Contracts
	public @FindBy(xpath = "/html/body/div[1]/div/nav/div/div/ol/li/ol/li[1]/div/button[2]") WebElement button_RelatedContract;
	public @FindBy(xpath = "/html/body/div[1]/div/nav") WebElement div_RelatedContracts;
	public String fileName;

	public ContractDataSearch_Panel() throws IOException {
		super();
	}

	public ContractDataSearch_Panel confirmFileNameExists() throws Exception {
		Assert.assertTrue(isElementClickable(text_FileName));
		fileName = text_FileName.getText();
		return new ContractDataSearch_Panel();
	}

	public ContractDataSearch_Panel confirmDataAndSearchTabsAndRelatedContractsExist() throws Exception {
		waitUntilWebElementIsVisible(tab_Data);
		waitUntilWebElementIsVisible(tab_Search);
		waitUntilWebElementIsVisible(div_RelatedContracts);
		return new ContractDataSearch_Panel();
	}

	public ContractDataSearch_Panel clickSelectDropdown() throws Exception {
		waitAndClickElement(dropdown_Select);
		return new ContractDataSearch_Panel();
	}

	public ContractDataSearch_Panel hoverAllSelectDropdownOptions() throws Exception {
		actionHover(option_01);
		actionHover(option_02);
		actionHover(option_03);
		actionHover(option_04);
		actionHover(option_05);
		actionHover(option_06);
		actionHover(option_07);
		actionHover(option_08);
		actionHover(option_09);
		actionHover(option_10);
		actionHover(option_11);
		actionHover(option_12);
		actionHover(option_13);
		actionHover(option_14);
		actionHover(option_15);
		actionHover(option_16);
		actionHover(option_17);
		actionHover(option_18);
		actionHover(option_19);
		actionHover(option_20);
		actionHover(option_21);
		actionHover(option_22);
		return new ContractDataSearch_Panel();
	}

	public int checks = 0;
	public ContractDataSearch_Panel clickSelectDropdownOption() throws Exception {
		if (checks == 0) {
			waitAndClickElement(option_01);
		} else {
			waitAndClickElement(option_21);
		}
		return new ContractDataSearch_Panel();
	}

	public ContractDataSearch_Panel confirmFieldCategories() throws Exception {
		checks++;
		String exception = null;
		try {
			assertTextMatches("counterpartyname", subheader_01);
			assertTextMatches("documenttitle", subheader_02);
			assertTextMatches("filename", subheader_03);
			assertTextMatches("effectivedate", subheader_04);
			assertTextMatches("documenttype", subheader_05);
			assertTextMatches("documenttypedefineother", subheader_06);
			assertTextMatches("cliententity", subheader_07);
			assertTextMatches("contractid", subheader_08);
			if (checks == 2) {
				assertTextMatches("contractstatus", nextSubheader_01);
			}
		} catch (AssertionError e) {
			exception = "";		
			System.out.println("SUCCESS: All subheaders are displayed properly.");
		} finally {
			if (exception == null) {
				System.out.println("SUCCESS: All subheaders are displayed properly.");
			}	
		}
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel confirmCategoryHeaderCollapsed() throws Exception {
		confirmWebElementTagHasSpecificValue(header_01, "class", "collapsed");
		return new ContractDataSearch_Panel();
	}
	public ContractDataSearch_Panel clickCategoryXButton() throws Exception {	
		waitAndClickElement(button_CategoryX);
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel clickHeader() throws Exception {	
		waitAndClickElement(header_01);
		return new ContractDataSearch_Panel();
	}

	public ContractDataSearch_Panel clickSearchTab() throws Exception {	
		waitAndClickElement(tab_Search);
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel confirmSearchTabIsVisible() throws Exception {	
		waitUntilWebElementIsVisible(textField_SearchDocument);	
		try {
			Assert.assertTrue(isElementClickable(textField_SearchDocument));
			System.out.println("SUCCESS: Search tab displayed properly.");
		} catch (AssertionError e) {
			System.out.println("FAIL: Search tab not displayed properly. Error Message:\n " + e);
			soft.assertTrue(isElementClickable(textField_SearchDocument));
		}
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel enterSearchTermInSearchField(String searchTerm) throws Exception {	
		sendKeysToWebElement(textField_SearchDocument, searchTerm);
		return new ContractDataSearch_Panel();
	}
	
	int intResults = 0;
	public int getSearchPanelResultAmount() {
		String[] strResults = text_SearchResults.getText().split(" ");
		intResults = Integer.parseInt(strResults[0]);
		return intResults;
	}
	
	public ContractDataSearch_Panel clickDownArrow() throws Exception {	
		for (int i = 0; i < intResults; i++) {
			waitAndClickElement(button_ArrowDown);
		}
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel confirmRelatedContractsExist() throws Exception {
		waitUntilWebElementIsVisible(div_RelatedContracts);
		return new ContractDataSearch_Panel();
	}
	
	String oldFileName;
	public ContractDataSearch_Panel clickRelatedContract() throws Exception {	
		oldFileName = text_FileName.getText();
		waitAndClickElement(button_RelatedContract);
		return new ContractDataSearch_Panel();
	}
	
	public ContractDataSearch_Panel confirmRelatedContractHasOpened() throws Exception {
		checks = 0;
		System.out.print("Waiting for new page");
		do {
			if (checks >= 100) {
				break;
			}
			this.wait.until(ExpectedConditions.visibilityOf(text_FileName));
			System.out.print(".");
			checks++;
		} while (text_FileName.getText().equals(oldFileName));
		System.out.println("\nChecks = " + checks);
		System.out.println(oldFileName);
		System.out.println(text_FileName.getText());

		if (assertTextMatches("docreplacepdf", text_FileName) == true) {
			System.out.println("--- Related document displayed properly. Updated File Name is visible.");
		} else {
			System.out.println("--- Related document NOT displaying properly. Updated File Name is NOT visible.");
		}
		return new ContractDataSearch_Panel();
	}
}
