# IDE Setup
---
## Java and Maven Installation for Windows
### Java:

1. Download Java SE Development Kit from this URL: https://www.oracle.com/technetwork/java/javase/downloads/jdk13-downloads-5672538.html
2. Run the installer
3. Set up system variables (Part 1):
    - Open Advanced System Settings and click the Advanced tab
    - Click the Environment Variables button
    - Click the New button under the System Variables section to create a new System Variable
        - Variable name: `JAVA_HOME`
        - Variable value: *[JAVA INSTALLATION DIRECTORY PATH]*
            - Example: `C:\Program Files\Java\jdk1.8.0_131`
- Navigate to your Java installation folder. Copy and paste the directory path to the Variable value input box
- Click OK

4. Set up system variables (Part 2):
    - In the System Variables section, scroll to find the “Path” variable and double click
    - Click New and paste the same directory used to set the above variable value, and type `\bin` at the end. 
        - Example: `C:\Program Files\Java\jdk1.8.0_131\bin`
    - Click OK to close the “Edit environment variable” window
    - Click OK to close the “Environment Variables” window

5. Confirm installation was successful
    - Open command line terminal
    - Run the command: ```java -version```
	
*You should see the version information for Java listed in the console confirming the installation.*

### Maven:

1. Download Apache Maven binary zip archive from the following link: https://maven.apache.org/download.cgi
2. Extract the folder to the Desktop
3. Navigate to the Program Files directory and create a new folder called “Dev_Programs”
4. Move the extracted Apache Maven folder to the Dev_Programs folder
5. Open Environment Variables and create a new System Variable
    - Variable name: `M2_HOME`
    - Variable value: *[Location of the extracted Apache Maven directory]*
        - Example: `C:\Program Files\Dev_Programs\apache-maven-3.6.2`

6. Create another new System Variable
    - Variable name: `M2`
    - Variable value: *[Location of the extracted Apache Maven directory, followed by `\bin`]*
        - Example: `C:\Program Files\Dev_Programs\apache-maven-3.6.3\bin`

7. Double click the Path variable and click New
8. Type `%M2%`
9. Click OK on all open windows and open command line console
10. Run the command ```mvn -v```

*You should see the version information for Apache Maven listed in the console confirming the installation.*

---
## Java and Maven Installation for Mac
*If you already have created a bash profile, skip steps 6-10. Otherwise, you will need to create a bash profile (File) under your user directory. A typical install of OS X won’t create a .bash_profile for you. When you want to run functions from your command line, this is a must-have.*

1.	Download and run the installer for Java SE Development Kit from this URL: https://www.oracle.com/technetwork/java/javase/downloads/jdk13-downloads-5672538.html
2.	Download Apache Maven binary zip archive from the following link: https://maven.apache.org/download.cgi
3.	Extract the Apache Maven folder to the Desktop
4.	Navigate to your user directory and create a new folder called “Dev_Programs”
5.	Move the extracted Apache Maven folder to the Dev_Programs folder
6.	Open the Terminal
7.	Type ```cd ~/``` to go to your home folder
8.	Type ```touch .bash_profile``` to create your new file.
9.	Edit `.bash_profile` with your favorite editor (or you can just type ```open -e .bash_profile``` to open it in TextEdit.
10.	Type ```. .bash_profile``` to reload `.bash_profile` and update any functions you add.
11.	Add the following to the file (alter the locations for Apache Maven and Java JDK in accordance to your machine, as well as the version type you have downloaded):
    * `export M2_HOME=/usr/local/apache-maven-3.6.2`
    * `export M2=$M2_HOME/bin`
    * `export PATH=$PATH:$M2`
    * `export JAVA_HOME=/Library/Java/JavaVirtalMachines/jdk1.8.0_171.jdk/Contents/Home`
    * `source ~/.git-bash-for-mac.sh`

12.	Also add the following:
    * `export NVM_DIR=”$HOME/.nvm`
    * `[ -s “$NVM_DIR/nvm.sh” ] && \. “$NVM_DIR/nvm.sh”` #This loads nvm
    * `[ -s “$NVM_DIR/bash_completion” ] && \. “$NVM_DIR/bash-completion` #This loads nvm bash_completion

13.	Open the Terminal and perform
    * `Java -version` 
    * `mvn -v`

*You should see that the relevant software is installed.*

---
## Java and Maven Configuration Within Eclipse
### Eclipse Installation and Project Setup

1.	Download the Eclipse install file from:  https://www.eclipse.org/downloads/
2.	Run the installer and select “Eclipse IDE for Java Developers”
3.	Launch Eclipse. Eclipse launcher will open, allowing you to select a workspace.
4.	Before selecting a Workspace, create a new folder on the Desktop called “KCITestAutomation”
5.	Select the directory created in step 4 as the workspace, then press OK. You may now clone the project from the git repository into the workspace.
6.	Click File, then click Import.
7.	Select General, then click “Existing Projects into Workspace. Click Next.
8.	Make sure “Select root directory” is selected, then click Browse.
9.	Navigate to the `KCITesting_beta` folder and click Open.
10.	Make sure the checkmark next to the KCITesting_beta project is checked, then click Finish. 

*The project should now be able to be viewed and edited. However, further steps need to be taken for the automation to run properly.*

---
## Configuring Java and Maven

### Maven:

1.	Open the Preferences settings for Eclipse.
2.	Expand the Maven tab, click Installations, and then click the Add button,
3.	Click the Directory button and navigate to the Apache Maven installation folder that was extracted to the Dev_Programs folder. 
4.	Open the Apache Maven folder and then click Finish.
5.	Make sure the apache-maven installation is checked, then Apply and Close.

### Java:

1.	While still in the Preferences window, expand the Java tab and click Installed JREs.
2.	If a JRE is not already installed: 
    * click Add
    * Select “Standard VM” and click Next.
    * Click the Directory button. Navigate to your Java installation and open the Home folder. Example path: `/Library/Java/JavaVirtualMachines/jdk-13.jdk/Contents/Home`
    * After the appropriate directory is selected, JRE name should autofill.
    * In Default VM arguments, add the following line:`-Dmaven.multiModuleProjectDirectory=M2_HOME`
3. If a JRE is installed: 
    * Select the currently installed JRE and click Edit.
    * Ensure the JRE home correctly links to your Java home installation. If not, click the Directory button and navigate to and open the correct folder.
    * If the Default VM arguments box is empty, add the line provided in Step 6 above.
4.	Click Finish, then click Apply and Close.
5.	Right click the `KCITesting_beta` folder in the Package Explorer and click Properties.
6.	Select Java Build Path and click the Libraries tab.
7.	Select the JRE System Library and click the Edit button.
8.	Select “Alternate JRE” if it is not already, then ensure the correct Installed JRE is selected and click Finish.
9.	Click the Add Variable button, select the `M2_REPO` variable, click OK, then click Apply and Close.
10.	Right click the `pom.xml` file in the Package Explorer, mouse over Run As, and select “Maven install”. 

*If everything is configured properly, the console will display a Build Success message after fully installing Maven.*

---
## Required Eclipse Plugins

Certain plugins are required in order for Cucumber to function within Eclipse. Access the Eclipse Marketplace by going to the Help dropdown and clicking Eclipse Marketplace. Search for and install the following plugins:
	
* Buildship Gradle Integration
* Cucumber Eclipse Plugin
* Natural 0.7.6

I also recommend installing with Darkest Dark Theme with DevStyle plugin for a more visually appealing UI, but this is not required. 
**Restart Eclipse after installing these plugins.**

---
#### There are many more steps involved in preparing the environment to run Cucumber / Selenium automation. 
#### These steps have already been handled within the pulled project code. 
#### If errors occur after reviewing the instructions, contact the developer for further assistance.

---
## Setting up personal information and login credentials used during testing

* Navigate to and open the config file: `KCITesting_beta/src/main/java/properties/config.properties`
* Change the following values to match your own requirements and personal information:
    * `browser`: Type either “chrome” or “firefox”, value IS case-sensitive.
    * `testerEmail`: The email used to access your email inbox and KCI. Values should be the same. If they are different, contact the developer for assistance.
    * `inboxPassword`: The password used to access your email inbox.
    * `knowablePassword`: Your default password used to access KCI.
    * `testerFirstName`: The default first name used to test the Account Settings within KCI.
    * `testerLastName`: The default last name used to test the Account Settings within KCI.

All tests are inactive by default. To select specific tests to run, move the feature file(s) you wish to run to the `activeFeatureFiles` package, leaving tests you wish to ignore in the `inactiveFeatureFiles` package.
